#!/bin/bash

DEPLOY_ROOT=/home/sanket/SVN/Kiran/ConnectionBot
COMMON_LIB=$DEPLOY_ROOT/common/lib-inhouse
THIRDPARTY_LIB=$DEPLOY_ROOT/thirdparty/lib
BOT_ROOT=$DEPLOY_ROOT/bot
BOT_LIB=$BOT_ROOT/lib-inhouse
BOT_CONF=$BOT_ROOT/conf
BOT_LOGS=$BOT_ROOT/logs

CLASSPATH=$BOT_LIB/bot.jar:$COMMON_LIB/tech-frameworks.jar
CLASSPATH=$CLASSPATH:$COMMON_LIB/core-service_common.jar
CLASSPATH=$CLASSPATH:$THIRDPARTY_LIB/slf4j-api-1.6.2.jar:$THIRDPARTY_LIB/slf4j-log4j12-1.6.2.jar
CLASSPATH=$CLASSPATH:$THIRDPARTY_LIB/log4j-1.2.16.jar
CLASSPATH=$CLASSPATH:$THIRDPARTY_LIB/commons-dbcp-1.4.jar:$THIRDPARTY_LIB/commons-pool-1.5.6.jar
CLASSPATH=$CLASSPATH:$THIRDPARTY_LIB/apache-log4j-extras-1.1.jar
CLASSPATH=$CLASSPATH:$THIRDPARTY_LIB/commons-io-1.2.jar
CLASSPATH=$CLASSPATH:$COMMON_LIB/tech-hibernate-pkr.jar:$THIRDPARTY_LIB/spring.jar:$THIRDPARTY_LIB/commons-logging.jar:$THIRDPARTY_LIB/netty-3.6.6.Final.jar

export CLASSPATH

$JAVA_HOME/bin/java -Xms512m -Xmx512m -verbose:gc -XX:+PrintGC -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -Djava.net.preferIPv4Stack=true com.games24x7.init.BotMain $BOT_CONF/bot.props >> $BOT_LOGS/bot.log 2>&1 &
