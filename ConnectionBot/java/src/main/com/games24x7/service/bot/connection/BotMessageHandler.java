package com.games24x7.service.bot.connection;

import com.games24x7.service.UserSession;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;

public interface BotMessageHandler <T extends Message>
{
	public void handleMessage( UserSession session, T message, PlayerInfo plrInfo );
}
