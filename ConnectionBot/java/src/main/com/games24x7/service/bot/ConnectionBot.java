package com.games24x7.service.bot;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.message.serializer.BotSerializer;
import com.games24x7.framework.configuration.Configuration;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.BotMsgDigester;

public interface ConnectionBot
{
	public Channel createCSClientChannel(String teIp, int port);
	public BotSerializer getBotSerializer();
	public BotMessageHandler getMsgHandler( int classId );
	public Configuration getConfiguration();
	public StartConnectionBots getBots();
	public ScheduledExecutorService getScheduledThreadPool();
	public BotMsgDigester getBotMsgDigester();
	public void handleDisconnection(long playerId, long tableId, ScheduledFuture heartBeatTask);
}
