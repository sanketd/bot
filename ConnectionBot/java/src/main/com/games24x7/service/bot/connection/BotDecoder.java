package com.games24x7.service.bot.connection;

import java.lang.reflect.Method;
import java.util.zip.Inflater;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.service.message.Frame;

public class BotDecoder extends org.jboss.netty.handler.codec.frame.FrameDecoder
{

	private static char EOF_CHAR = ( char ) 0x00;
	private static String EOM_REGEXP = "\r\n" + EOF_CHAR;
	String buff = null;

	private Logger logger = LoggerFactory.getLogger( BotDecoder.class );

	public BotDecoder()
	{
	}

	@Override
	protected Object decode( ChannelHandlerContext ctx, Channel channel, ChannelBuffer buffer ) throws Exception
	{
		logger.debug( "Inside decoder" + buffer.toString());
		if( buffer.readableBytes() < 17 )
		{
			return null;
		}
		buffer.markReaderIndex();
		byte firstByte = buffer.readByte();
		boolean isJumboFrame = ( firstByte >>> 7 ) == 1;
		boolean isCompressedPayload = ( ( firstByte >>> 6 ) ) == 1;
		int messageClassID = buffer.readInt();
		long receiverID = buffer.readLong();
		int length;
		if( isJumboFrame )
		{
			length = buffer.readInt();
		}
		else
		{
			length = buffer.readShort();
		}
		byte[] payload;
		if( isCompressedPayload )
		{
			int compressedPayloadLength = isJumboFrame ? buffer.readInt() : buffer.readShort();
			if( buffer.readableBytes() < compressedPayloadLength )
			{
				buffer.resetReaderIndex();
				return null;
			}
			byte[] compressedPayload = new byte[compressedPayloadLength];
			buffer.readBytes( compressedPayload );
			payload = new byte[length];
			Inflater inflater = new Inflater();
			inflater.setInput( compressedPayload );
			int inflatedBytes = inflater.inflate( payload );
			if( inflatedBytes != payload.length )
			{
				System.err.println( "Inflated byte array size does not match with the count on Frame header for - " + ctx.getChannel().getRemoteAddress() );
			}
			inflater.end();
		}
		else
		{
			if( buffer.readableBytes() < length )
			{
				buffer.resetReaderIndex();
				return null;
			}
			payload = new byte[length];
			buffer.readBytes( payload );
		}
		// destroyDirectChannelBuffer( buffer );
		return new Frame( messageClassID, receiverID, isCompressedPayload, payload );

	}

	public static void destroyDirectChannelBuffer( ChannelBuffer toBeDestroyed )
	{
		try
		{

			// Preconditions.checkArgument(toBeDestroyed.isDirect(),
			// "toBeDestroyed isn't direct!");
			if( !toBeDestroyed.isDirect() )
			{
				System.out.println( "ChannelBuffer is not Direct so cant destroy" );
			}

			Method cleanerMethod = toBeDestroyed.getClass().getMethod( "cleaner" );
			cleanerMethod.setAccessible( true );
			Object cleaner = cleanerMethod.invoke( toBeDestroyed );
			Method cleanMethod = cleaner.getClass().getMethod( "clean" );
			cleanMethod.setAccessible( true );
			cleanMethod.invoke( cleaner );
		}
		catch( Exception ex )
		{
			System.out.println( "Failed to destroyDirectChannelBuffer due to Exception" );
		}

	}

}
