package com.games24x7.service.bot.connection;

import java.util.zip.Deflater;

import org.jboss.netty.buffer.ChannelBuffer;
import org.jboss.netty.buffer.ChannelBuffers;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.handler.codec.oneone.OneToOneEncoder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.service.message.Frame;

public class BotEncoder extends OneToOneEncoder
{

	private Logger logger = LoggerFactory.getLogger( BotEncoder.class );

	public BotEncoder()
	{
	}

	@Override
	protected Object encode( ChannelHandlerContext ctx, Channel channel, Object msg ) throws Exception
	{
		if( !( msg instanceof Frame ) )
		{
			return msg;
		}
		Frame frame = ( Frame ) msg;

		// Form the first byte which is a bitmap indicative of various
		// states
		logger.debug( " frame.isJumbo() " + frame.isJumbo() + "frame length " + frame.getPayload().length + "frame.isCompressed() " + frame.isCompressed() );
		byte firstByte = ( byte ) ( frame.isJumbo() ? ( 1 << 7 ) : 0 );
		firstByte = ( byte ) ( firstByte | ( byte ) ( frame.isCompressed() ? ( 1 << 6 ) : 0 ) );
		ChannelBuffer cb = ChannelBuffers.dynamicBuffer();
		cb.writeByte( firstByte );

		// Write the routing fields of message class ID & receiver ID
		cb.writeInt( frame.getMessageClassID() );
		cb.writeLong( frame.getReceiverID() );

		// Write the length of the frame
		if( frame.isJumbo() )
		{
			cb.writeInt( frame.getPayload().length );
		}
		else
		{
			cb.writeShort( frame.getPayload().length );
		}

		// Compress the payload if needed and write the payload and
		// compressed
		// length of the payload too
		if( frame.isCompressed() )
		{
			Deflater deflater = new Deflater();
			byte[] compressedPayload = new byte[2 * frame.getPayload().length];
			deflater.setInput( frame.getPayload() );
			deflater.finish();
			int compressedLength = deflater.deflate( compressedPayload );
			if( frame.isJumbo() )
			{
				cb.writeInt( compressedLength );
			}
			else
			{
				cb.writeShort( compressedLength );
			}
			cb.writeBytes( compressedPayload, 0, compressedLength );
			deflater.end();
			logger.debug( " frame.isJumbo() " + frame.isJumbo() + "compressedLength length " + compressedLength + "frame.isCompressed() " + frame.isCompressed() );
		}
		else
		{
			cb.writeBytes( frame.getPayload() );
		}
		return cb;
	}

}
