package com.games24x7.service.bot.connection;

import java.util.concurrent.TimeUnit;

import org.jboss.netty.channel.ChannelPipeline;
import org.jboss.netty.channel.ChannelPipelineFactory;
import org.jboss.netty.channel.Channels;
import org.jboss.netty.handler.timeout.IdleStateHandler;
import org.jboss.netty.util.HashedWheelTimer;
import org.jboss.netty.util.Timer;

import com.games24x7.framework.configuration.Configuration;
import com.games24x7.init.BotMain;

public class BotPipelineFactory implements ChannelPipelineFactory
{

	private Configuration mainconfig = BotMain.getMainConfig();
	private static Timer timer = new HashedWheelTimer( 1000, TimeUnit.MILLISECONDS );

	@Override
	public ChannelPipeline getPipeline() throws Exception
	{

		ChannelPipeline pipeline = Channels.pipeline();
		pipeline.addLast( "idleStateHandler",
				new IdleStateHandler( timer, 
						mainconfig.getIntValue( "DISCONNECTION_DETECTION_TIME_IN_SECONDS", 10 ), 
						mainconfig.getIntValue( "DISCONNECTION_DETECTION_TIME_IN_SECONDS", 10 ),
						0 ) );
		pipeline.addLast( "botencoder", new BotEncoder() );
		pipeline.addLast( "botdecoder", new BotDecoder() );
		pipeline.addLast( "handler", new BotHandler() );
		return pipeline;
	}

}