package com.games24x7.service.bot.connection;

import org.jboss.netty.channel.ChannelHandlerContext;
import org.jboss.netty.channel.ChannelStateEvent;
import org.jboss.netty.channel.ExceptionEvent;
import org.jboss.netty.channel.MessageEvent;
import org.jboss.netty.handler.timeout.IdleState;
import org.jboss.netty.handler.timeout.IdleStateAwareChannelHandler;
import org.jboss.netty.handler.timeout.IdleStateEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.service.message.Frame;
import com.games24x7.service.message.Message;
import com.games24x7.service.message.serializer.Serializer;
import com.games24x7.service.message.serializer.io.taggedtext.json.JSONMessageInputStream;
import com.games24x7.util.ChannelIDChannelInfoMap;
import com.games24x7.util.ChannelIdVsPlayerInfo;
import com.games24x7.util.ChannelInfo;
import com.games24x7.util.PlayerInfo;

public class BotHandler extends IdleStateAwareChannelHandler
{

	private Logger logger = LoggerFactory.getLogger( BotHandler.class );
	//private ConnectionService connectionService;
	@Override
	public void channelOpen( ChannelHandlerContext ctx, ChannelStateEvent e ) throws Exception
	{

		if( logger.isDebugEnabled() )
		{
			logger.debug( "GE Channel Open" );
		}
	}

	@Override
	public void channelIdle( ChannelHandlerContext ctx, IdleStateEvent e )
	{
			try
			{
					if( e.getState() == IdleState.READER_IDLE )
					{
							logger.debug( "READER_IDLE " + IdleState.READER_IDLE );
							e.getChannel().close();
					}
					else if( e.getState() == IdleState.WRITER_IDLE )
					{
							logger.debug( "WRITER_IDLE " + IdleState.READER_IDLE );
					}
			}
			catch( Exception ex )
			{
					logger.error( "Exception :", ex );
			}
	}

	@Override
	public void channelConnected( ChannelHandlerContext ctx, ChannelStateEvent e ) throws Exception
	{
		try
		{
			System.out.println( "connected = " + ctx.getChannel().getRemoteAddress() );
			ChannelInfo channelInfo = ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( ctx.getChannel() );
			// System.out.println( channelInfo.getSessionKey() + " "
			// + new UserSession( channelInfo.getChannelID(),
			// channelInfo.getSessionKey() ) );
			ctx.setAttachment( channelInfo.getChannelID() );
			Long channelID = ( Long ) ctx.getAttachment();
//			ChannelIdVsReceiverQueue.put( channelID, new ReceivedMsgQueue());
		}
		catch( Exception ex )
		{
			ex.printStackTrace();
		}

		if( logger.isInfoEnabled() )
		{
			logger.info( "netty Channel Connected" );
		}
	}

	@Override
	public void channelDisconnected( ChannelHandlerContext ctx, ChannelStateEvent e ) throws Exception
	{

		System.out.println( "disconnected - " + ctx.getChannel().getRemoteAddress() );
		Long channelID = ( Long ) ctx.getAttachment();
//		ChannelInfo channelInfo= ChannelIDChannelInfoMap.getChannelInfo( channelID );
//		ChannelIdVsReceiverQueue.remove( channelID );
		if( logger.isDebugEnabled() )
		{
			logger.debug( "In netty onDisconnect" );
		}
		System.out.println( channelID );
		PlayerInfo playerInfo = ChannelIdVsPlayerInfo.get( channelID );
		System.out.println( playerInfo.getPlayerId() );
		BotImpl.getInstance().handleDisconnection(playerInfo.getPlayerId(), playerInfo.getTableId(), playerInfo.getHeartBeatTask());
	}

	@Override
	public void messageReceived( ChannelHandlerContext ctx, MessageEvent e ) throws Exception
	{
		Frame f = ( Frame ) e.getMessage();
		Long channelId = ( Long ) ctx.getAttachment();
		logger.debug( "Message from CS" + new String( f.getPayload() ) );
		byte[] payload = f.getPayload();
		Serializer serializer = BotImpl.getInstance().getBotSerializer();
		int messageClassID = f.getMessageClassID();
		Message message = serializer.deserialize( new JSONMessageInputStream( serializer, payload ), messageClassID );
		if( messageClassID == 1048661 )
			System.out.println( "++++++++++++" + message.toString() );

		logger.debug( "Incoming Message" + new String( f.getPayload() ) + "ChannelId " );
		//System.out.println( "Incoming Message" + new String( f.getPayload() ) + "ChannelId " );
		BotImpl.getInstance().getBotMsgDigester().enqueue( message, channelId );
//		ReceivedMsgQueue receivedMsgQueue = ChannelIdVsReceiverQueue.get( channelId );
//		boolean isSucc = receivedMsgQueue.put( message );
		//System.out.println("Is msg added to queue "+isSucc);
//		System.out.println(receivedMsgQueue.toString());
	}

	@Override
	public void exceptionCaught( ChannelHandlerContext ctx, ExceptionEvent e ) throws Exception
	{
		try
		{
			// e.getCause().printStackTrace();
		}
		catch( Exception ex )
		{
			// TODO: handle exception
		}

	}

	@Override
	public void channelBound( ChannelHandlerContext ctx, ChannelStateEvent e ) throws Exception
	{
		if( logger.isDebugEnabled() )
		{
			logger.debug( "GE Channel Bound " );
		}
	}

	@Override
	public void channelUnbound( ChannelHandlerContext ctx, ChannelStateEvent e ) throws Exception
	{
		if( logger.isDebugEnabled() )
		{
			logger.debug( "GE Channel Unbound " );
		}
	}
}
