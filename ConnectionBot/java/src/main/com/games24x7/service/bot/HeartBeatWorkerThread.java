package com.games24x7.service.bot;

import java.sql.Timestamp;
import java.util.Date;
import java.util.TimerTask;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.message.HeartBeat;
import com.games24x7.service.message.Message;
import com.games24x7.util.SendMsgOnChannel;

public class HeartBeatWorkerThread implements Runnable 
{

	Channel channel=null;
	long receiverId;
	long playerId;
	public HeartBeatWorkerThread(Channel ch, long receiverId, long playerId)
	{
		this.channel=ch;
		this.receiverId=receiverId;
		this.playerId=playerId;
	}
	@Override
	public void run()
	{
			Message heartBeat = new HeartBeat(receiverId, playerId);
			//System.out.println("Fake Time="+new Date(System.currentTimeMillis())+", sending heartbeat "+heartBeat.toString()+" "+receiverId + " playerId " + playerId);
			//System.out.println(playerId);
			if(channel!=null)
			{				
				SendMsgOnChannel.send( heartBeat, channel );
			}
			else
			{
				System.out.println("Channel closed ");
			}
		
	}
	
	
}
