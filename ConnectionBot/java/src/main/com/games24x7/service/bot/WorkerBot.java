package com.games24x7.service.bot;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.HandShake;
import com.games24x7.bot.message.MttTableMapResp;
import com.games24x7.service.UserSession;
import com.games24x7.service.message.Message;
import com.games24x7.util.ChannelIDChannelInfoMap;
import com.games24x7.util.ChannelIdVsPlayerInfo;
import com.games24x7.util.ChannelInfo;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class WorkerBot implements Runnable
{

	// private static Logger logger = LoggerFactory.getLogger(
	// WorkerBot.class );
	long playerId = -1;
	long receiverId;
	Message msg = null;
	int serviceId;
	Channel channel = null;
	ChannelInfo channelInfo = null;

	public void setChannelInfo( ChannelInfo channelInfo, MttTableMapResp message )
	{
		this.channelInfo = channelInfo;
		System.out.println( message.getMttplayerid() );
		System.out.println( message.getMtttableid() );
	}

	//private ReceivedMsgQueue receivedMsgQueue = null;
	private boolean handshakeReceived = false;
	private UserSession us = null;
	private PlayerInfo plrInfo = null;
	private boolean mttTableMapFlag;
	private long mttTournamentId = 0;
	private boolean isFMG = false;
	private int channel_id = 1;
	private double FMGAmount = 0;
	String myIp = null;
	private int noOfPlayer = 2;
	private int settlementType;
	private int perTablePlayers;
	private String csIp;
	private int csPort;
	
	public WorkerBot( long receiverId, long playerId, String teIp, int port, int perTablePlayers, int settlementType, boolean mttTableMapFlag, long mttTournamentId, int channel_id, boolean isFMG,
			int FMGAmount )
	{
		System.out.println( "inside constructor " + receiverId );
		
		this.receiverId = receiverId;
		this.playerId = playerId;
		this.mttTableMapFlag = mttTableMapFlag;
		this.mttTournamentId = mttTournamentId;
		this.noOfPlayer = perTablePlayers;
		this.isFMG = isFMG;
		this.myIp = BotImpl.getInstance().getConfiguration().getStringValue( "MY_IP" );
		this.channel_id = channel_id;
		this.FMGAmount = FMGAmount;
		this.settlementType=settlementType;
		this.perTablePlayers=perTablePlayers;
		this.csIp=teIp;
		this.csPort=port;
	}

	@Override
	public void run()
	{
		this.channel = BotImpl.getInstance().createCSClientChannel( csIp, csPort );
		this.channelInfo = ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( channel );
		this.us = new UserSession( Long.toString( channelInfo.getChannelID() ), channelInfo.getSessionKey() );
		plrInfo = new PlayerInfo( playerId, channel );
		plrInfo.setSeatId( ( int ) playerId % perTablePlayers );
		plrInfo.setUserSession( us );
		plrInfo.setSettlementType( settlementType );
		ChannelIdVsPlayerInfo.put( channelInfo.getChannelID(), plrInfo );

		msg = new HandShake( receiverId, playerId, myIp );
		System.out.println( "sending handshake " + msg.toString() + " " + channel.isConnected() + " " + msg.getPlayerID() + " " + msg.getReceiverID() );
		us.setAccountID( Thread.currentThread().getId(), playerId );
		us.setAccountName( Thread.currentThread().getName() );
		//System.out.println( "Hello ---->>> " + channel );
		boolean sendSuccess = SendMsgOnChannel.send( msg, channel );
		if( sendSuccess )
		{
			System.out.println( "Handshake Sent" );
		}
	}

}
