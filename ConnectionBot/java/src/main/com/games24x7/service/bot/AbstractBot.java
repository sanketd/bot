package com.games24x7.service.bot;

import com.games24x7.framework.configuration.Configuration;

public abstract class AbstractBot implements Bot
{
	    protected Configuration mainConfig;
	    public AbstractBot(Configuration mainConfig) 
	    {
	        this.mainConfig = mainConfig;
	    }
	public Configuration getConfiguration(){
		return mainConfig;
	}

}
