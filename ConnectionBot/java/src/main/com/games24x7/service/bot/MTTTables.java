/*package com.games24x7.service.bot;

import java.util.Timer;

import org.jboss.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.BotMessageConstants;
import com.games24x7.bot.message.HandShake;
import com.games24x7.bot.message.MttTableMap;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.ChannelIDChannelInfoMap;
import com.games24x7.util.ChannelIdVsReceiverQueue;
import com.games24x7.util.ChannelInfo;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.ReceivedMsgQueue;
import com.games24x7.util.SendMsgOnChannel;

public class MTTTables
{

	*//*******
	 * 
	 * 
	 * Using dummy user to get MTT Table mapping
	 * 
	 * 
	 *//*

	private static Logger logger = LoggerFactory.getLogger( WorkerBot.class );
	long playerId = 1;
	static int id = 0;
	long receiverId = 1;
	long tournamentId;
	Message msg = null;
	int serviceId;
	Channel channel = null;
	ChannelInfo channelInfo = null;
	private Timer timer;
	//private ReceivedMsgQueue receivedMsgQueue = null;
	private boolean handshakeReceived = false;
	private UserSession us = null;

	public MTTTables( long tournamentId, String teIp, int port )
	{
		System.out.println( "inside constructor " + receiverId + " " + tournamentId );
		this.channel = BotImpl.getInstance().createCSClientChannel( teIp, port );
		this.channelInfo = ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( channel );
		this.us = new UserSession( String.valueOf(channelInfo.getChannelID()), channelInfo.getSessionKey() );
		this.tournamentId = tournamentId;
		System.out.println( channelInfo.toString() + " " + us.toString() + " " + channel.isBound() + " " + channel.isOpen() + " " + channel.isReadable() + " " + channel.isWritable() + " "
				+ channel.isConnected() );
	}

	public void getTables()
	{
		boolean flag = true;
		try
		{
			Thread.sleep( 1000 );
		}
		catch( InterruptedException e1 )
		{
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		msg = new HandShake( receiverId, playerId, "192.168.48.192" );
		// ( receiverId, playerId, serviceId,
		// Thread.currentThread().getId(), false );
		System.out.println( "sending handshake " + msg.toString() + " " + channel.isConnected() + " " + msg.getPlayerID() + " " + msg.getReceiverID() );
		us.setAccountID( receiverId, playerId );
		us.setAccountName( Thread.currentThread().getName() );
		boolean sendSuccess = SendMsgOnChannel.send( msg, channel );
		if( sendSuccess )
		{
			try
			{
				Thread.sleep( 1000 );
				System.out.println( "Geting receiver queue for channel " + channelInfo.getChannelID() );
				receivedMsgQueue = ChannelIdVsReceiverQueue.get( channelInfo.getChannelID() );
				System.out.println( "Queue " + receivedMsgQueue.toString() + " and its size " );// +
														// receivedMsgQueue.size()
														// );
				if( receivedMsgQueue != null )
				{
					while( flag )
					{

						// System.out.println("polling queue "+receivedMsgQueue);
						Message message = receivedMsgQueue.get();
						if( message != null )
						{
							logger.debug( "Message " + message.getClass().toString() );
							if( message.getClassID() == BotMessageConstants.HANDSHAKERESPONSE )
							{
								handshakeReceived = true;
								logger.debug( "Handshakeresponse " + message.toString() );
								System.out.println( "HandShake Response" + message.toString() );
								MttTableMap mttTableMap = new MttTableMap( receiverId, playerId, tournamentId );
								System.out.println( "Sending MTT table msg " + mttTableMap.toString() );
								SendMsgOnChannel.send( mttTableMap, channel );
							}
							else if(message.getClassID() == BotMessageConstants.MTTTABLEMAPRESP)
							{
								BotMessageHandler messageHandler = BotImpl.getInstance().getMsgHandler(message.getClassID());
								try
								{
									messageHandler.handleMessage( us, message, new PlayerInfo( playerId, channel ) );
								}
								catch(NullPointerException e)
								{
									int count = message.toString().indexOf("Child");
									String temp = message.toString().substring( count );
									System.out.println("Handler Not added for Received Msg "+temp);
									logger.debug( "Handler Not added for Received Msg " +temp);
								}

							}
						}
					}
				}
				else
				{
					System.out.println( "******************ReceiverQueue not initialized*****************" );
				}
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
		}
	}

}
*/