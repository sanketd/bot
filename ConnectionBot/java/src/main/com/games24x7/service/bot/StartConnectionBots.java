package com.games24x7.service.bot;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StartConnectionBots
{
	ExecutorService executor = null;
	public static Logger logger = LoggerFactory.getLogger( StartConnectionBots.class );;

	public StartConnectionBots( int threads )
	{
		//BasicThreadFactory cbFactory = new BasicThreadFactory.Builder().namingPattern( "connectionbot-%d" ).build();
		executor = Executors.newFixedThreadPool( threads );
	}

	public void execute( int noOfTables, int tableIncreament, long tableId, int noOfPlayers, long playerId, String CSIp, int CSPort, int playerIncreament, boolean mttTableMap,
			long mttTournamentId, int settlementType, int mod_channel, boolean isFMG, int FMGAmount, int totalPlayers )
	{
		logger.debug( "Inside execute function ... " );
		if( isFMG )
		{
			int j = 0;
			while( j < totalPlayers )
			{
				System.out.println( "Player Id FMG-->> " + playerId );
				executor.execute( new WorkerBot( 0, playerId++, CSIp, CSPort, noOfPlayers, settlementType, mttTableMap, mttTournamentId, mod_channel, isFMG, FMGAmount ) );
				j = j + playerIncreament;
				System.out.println( "Inside ISFMG Flag" );
				// t.schedule( new
				// HeartBeatWorkerThreadTempForFMG( isFMG ), 0,
				// 2000 );
			}

		}
		else
		{

			for( int i = 0; i < noOfTables; i = i + tableIncreament )
			{
				int j = 0;
				while( j < noOfPlayers )
				{
					System.out.println( "Player Id-->> " + playerId );
					executor.execute( new WorkerBot( tableId + i, playerId++, CSIp, CSPort, noOfPlayers, settlementType, mttTableMap, mttTournamentId, mod_channel, isFMG,
							FMGAmount ) );
					j = j + playerIncreament;
				}
				// }

			}
		}
	}

	public void execute( int noOfTables, int tableIncreament, long tableId, int noOfPlayers, long[] playerIds, String CSIp, int CSPort, int playerIncreament, boolean mttTableMap,
			long mttTournamentId, int settlementType, int mod_channel, boolean isFMG, int FMGAmount, int totalPlayers )
	{
		logger.debug( "Inside execute function ... " );
		long playerId = 0;

		int j = 0;
		if( isFMG )
			tableId = 0;

		for( j = 0; j < playerIds.length; j++ )
		{
			playerId = playerIds[j];
			System.out.println( "Player Id -->> " + playerId );
			executor.execute( new WorkerBot( tableId, playerId, CSIp, CSPort, noOfPlayers, settlementType, mttTableMap, mttTournamentId, mod_channel,
	  isFMG,FMGAmount ) ); } }
	 

	public void executeMTT( int noOfTables, int tableIncreament, long tableId, int noOfPlayers, long playerId, String CSIp, int CSPort, int playerIncreament, boolean mttTableMap,
			long mttTournamentId, int settlementType, int mod_channel, boolean isFMG, int FMGAmount )
	{
		executor.execute( new WorkerBot( tableId, playerId++, CSIp, CSPort, noOfPlayers, settlementType, mttTableMap, mttTournamentId, mod_channel, isFMG, FMGAmount ) );
	}

	public void executePool( int noOfTables, int tableIncreament, long tableId, int noOfPlayers, long playerId, String CSIp, int CSPort, int playerIncreament, boolean mttTableMap,
			long mttTournamentId, int settlementType, int mod_channel, boolean isFMG, int FMGAmount, int totalPlayers )
	{
		executor.execute( new WorkerBot( tableId, playerId, CSIp, CSPort, noOfPlayers, settlementType, mttTableMap, mttTournamentId, mod_channel, isFMG, FMGAmount ) );
	}

	public void executeAfterPlayerElem( long tableId, int noOfPlayers, long playerId, String CSIp, int CSPort, int mod_channel, boolean isFMG, int FMGAmount )
	{
		System.out.println( "Inside after ElEM" );
		executor.execute( new WorkerBot( 0, playerId, CSIp, CSPort, noOfPlayers, 2, false, 0, mod_channel, isFMG, FMGAmount ) );

	}

	public ExecutorService getExecutor()
	{
		return executor;
	}

}
