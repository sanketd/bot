package com.games24x7.init;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;
import com.games24x7.framework.db.JDBCFrameworkFactory;
import com.games24x7.framework.logging.LoggingFramework;
import com.games24x7.util.PlayerInfoWithChannelId;

import java.sql.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BotMain
{
	private static Logger logger;
	public static final String DEFAULT_CONFIGURATION_FILE = "/home/sanket/SVN/Kiran/ConnectionBot/deployready/bot/conf/bots.props";
	private static PropsFileBasedConfiguration mainConfig;
	public static ConcurrentHashMap< Long, ArrayList< PlayerInfoWithChannelId > > playerchannelMap = new ConcurrentHashMap< Long, ArrayList< PlayerInfoWithChannelId > >();
	private static BotImpl bot;
	static long playerId;
	static String DB_HOST;
	static String DB_PORT;
	static String DB_USER;
	static String DB_PASS;
	static String DB_SCHEMA;
	static int SETTLEMENT_TYPE;
	static int playersOnTable;
	static int entryFee;
	static String query;
	static String query1;
	static ArrayList< Long > list;
	static boolean fmg_flag;
	static long tableId;
	static long[] players;
	static String connectionURL;
	static Connection connection;
	static Statement statement;
	static int prizeType;

	public static void init() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException
	{
		DB_HOST = mainConfig.getStringValue( "DB_HOST" );
		DB_PORT = mainConfig.getStringValue( "DB_PORT" );
		DB_USER = mainConfig.getStringValue( "DB_USER" );
		DB_PASS = mainConfig.getStringValue( "DB_PASS" );
		DB_SCHEMA = mainConfig.getStringValue( "DB_SCHEMA" );
		SETTLEMENT_TYPE = mainConfig.getIntValue( "SETTLEMENT_TYPE" );
		entryFee = mainConfig.getIntValue( "FMG_AMOUNT" );
		prizeType = mainConfig.getIntValue( "PRIZE_TYPE" );
		playersOnTable = mainConfig.getIntValue( "PLAYERS_ON_A_TABLE" );
		tableId = mainConfig.getLongValue( "STARTING_TABLE_ID" );
		query = "select tem.table_id,t.id from table_engine_mapping tem ,trnlist t where tem.tid=t.id and t.gameid=" + SETTLEMENT_TYPE + " and t.status=3 and t.entryFee=" + entryFee
				+ " and ptype=" + prizeType + " and maxplayers=" + playersOnTable + " order by t.id desc limit 1;";
		// query1 is for CFP
		query1 = "select tem.table_id,t.id from table_engine_mapping tem ,trnlist t where tem.tid=t.id and t.gameid=" + SETTLEMENT_TYPE + " and t.status=3 and ptype=" + prizeType
				+ " and maxplayers= " + playersOnTable + " order by t.id desc limit 1;";

		list = new ArrayList< Long >();
		logger = LoggerFactory.getLogger( BotMain.class );
		fmg_flag = mainConfig.getBooleanValue( "FMG_FLAG" );
		tableId = mainConfig.getLongValue( "STARTING_TABLE_ID" );
		players = new long[playersOnTable];

		connectionURL = "jdbc:mysql://" + DB_HOST + ":" + DB_PORT + "/" + DB_SCHEMA;

		connection = getConnection( connectionURL, DB_USER, DB_PASS );
		statement = connection.createStatement();
	}

	public static void main( String args[] ) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, InterruptedException
	{
		String file = DEFAULT_CONFIGURATION_FILE;

		if( args != null && args.length == 1 )
		{
			file = args[0];
		}
		mainConfig = new PropsFileBasedConfiguration( file );
		LoggingFramework.init( mainConfig );
		JDBCFrameworkFactory.init( mainConfig );
		bot = BotImpl.init( mainConfig );
		bot.startup();
		init();

		// TODO pick this from db - DOne
		// logger.debug("\nSETTLEMENT_TYPE = "+SETTLEMENT_TYPE+"\n");
		// String connectionURL = "jdbc:mysql://"+ DB_HOST
		// +":"+DB_PORT+"/"+DB_SCHEMA;
		//
		// Connection connection = getConnection(connectionURL, DB_USER,
		// DB_PASS);
		// Statement statement = connection.createStatement();

		logger.debug( "\nQUERY = " + query + "\n" );

		long noOfTournaments = mainConfig.getLongValue( "NO_OF_TOURNAMENT" );

		if( fmg_flag )
		{
			tableId = 0;
		}
		playerId = mainConfig.getLongValue( "STARTING_PLAYER_ID" );
		long[] players = new long[playersOnTable];

		for( int n = 1; n <= noOfTournaments; n++, playerId += playersOnTable )
		{

			for( int i = 0; i < playersOnTable; i++ )
			{
				players[i] = playerId + i;
			}
			Thread.sleep( 3000 );

			boolean isAutomatedTableId = mainConfig.getBooleanValue( "AUTOMATED_TABLE_ID" );
			if( isAutomatedTableId && !fmg_flag )
			{

				Thread.sleep( 7000 );
				ResultSet rs;
				if( SETTLEMENT_TYPE != 1 )
				{
					logger.debug( "" );
					rs = statement.executeQuery( query );
				}
				else

					rs = statement.executeQuery( query1 );

				while( rs.next() )
				{
					tableId = rs.getLong( "table_id" );
					logger.debug( "\nQUERY Result " + tableId + "\n" );
				}

				logger.debug( "Table id from bot main --->> " + tableId );
			}

			bot.getBots().execute( mainConfig.getIntValue( "NO_OF_TOURNAMENT" ), mainConfig.getIntValue( "TOURNAMENT_ID_INCREAMENT" ), tableId,
					mainConfig.getIntValue( "PLAYERS_ON_A_TABLE" ), players, mainConfig.getStringValue( "CS_IP" ), mainConfig.getIntValue( "CS_PORT" ),
					mainConfig.getIntValue( "PLAYER_ID_INCREAMENT" ), false, mainConfig.getLongValue( "MTT_TOURNAMENT_ID" ), mainConfig.getIntValue( "SETTLEMENT_TYPE" ),
					mainConfig.getIntValue( "CHANNEL_ID" ), mainConfig.getBooleanValue( "FMG_FLAG" ), mainConfig.getIntValue( "FMG_AMOUNT" ),
					mainConfig.getIntValue( "NO_OF_PLAYERS" ) );
			if( !fmg_flag )
			{
				tableId++;
			}

		}
	}

	public static PropsFileBasedConfiguration getMainConfig()
	{
		return mainConfig;
	}
	public static BotImpl getBotImpl()
	{
		return bot;
	}

	public synchronized static void executeInfiniteGame( long playerId )
	{

		list.add( Long.valueOf( playerId ) );
		logger.debug( "\nSize of List : " + list.size() );
		long tableId = 0;
		
		// MAKE_SPLIT_2_PLAYERS is the default case
		int eliminateplayer = 4;
		if(BotImpl.getInstance().getConfiguration().getBooleanValue( "MAKE_SPLIT_3_PLAYERS" ))
		{
			eliminateplayer = 3;
		}

		// list.size() >= playersOnTable - handle case where game play should work infinite for 6 players
		// list.size() >= eliminateplayer - handle case where player is eliminated in case of split

		if( list.size() >= playersOnTable || list.size() >= eliminateplayer )
		{
			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "PLAYNOW" ) )
			{
				for( int i = eliminateplayer - 1; i >= 0; i-- )
				{
					players[i] = list.get( i ).longValue();
					list.remove( i );
				}

			}
			else
			{

				for( int i = playersOnTable - 1; i >= 0; i-- )
				{
					players[i] = list.get( i ).longValue();
					list.remove( i );
				}
			}

			// bot.startupOneTable();

			if( !fmg_flag )
			{
				logger.debug( "\nSETTLEMENT_TYPE = " + SETTLEMENT_TYPE + "\n" );

				try
				{

					logger.debug( "\nQUERY = " + query + "\n" );
					Thread.sleep( 5000 );
					ResultSet rs;
					if( SETTLEMENT_TYPE != 1 )
						rs = statement.executeQuery( query );
					else
						rs = statement.executeQuery( query1 );

					while( rs.next() )
					{
						tableId = rs.getLong( "table_id" );
						logger.debug( "\nQUERY Result " + tableId + "\n" );
					}

					logger.debug( "Table Id -->> " + tableId );

				}
				catch( Exception e )
				{
					logger.debug( "\nException occured ... " );
					e.printStackTrace();
				}
			}

			bot.getBots().execute( mainConfig.getIntValue( "NO_OF_TOURNAMENT" ), mainConfig.getIntValue( "TOURNAMENT_ID_INCREAMENT" ), tableId,
					mainConfig.getIntValue( "PLAYERS_ON_A_TABLE" ), players, mainConfig.getStringValue( "CS_IP" ), mainConfig.getIntValue( "CS_PORT" ),
					mainConfig.getIntValue( "PLAYER_ID_INCREAMENT" ), false, mainConfig.getLongValue( "MTT_TOURNAMENT_ID" ), mainConfig.getIntValue( "SETTLEMENT_TYPE" ),
					mainConfig.getIntValue( "CHANNEL_ID" ), mainConfig.getBooleanValue( "FMG_FLAG" ), mainConfig.getIntValue( "FMG_AMOUNT" ),
					mainConfig.getIntValue( "NO_OF_PLAYERS" ) );
		}

	}

	public static Connection getConnection( String connectionURL, String username, String password ) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException
	{
		Connection connection = null;

		Class.forName( "com.mysql.jdbc.Driver" ).newInstance();
		connection = DriverManager.getConnection( connectionURL, username, password );

		return connection;
	}
}