package com.games24x7.init;

import java.net.URL;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import com.games24x7.framework.configuration.propsfile.PropsFileBasedConfiguration;

public class JoinMTTMain
{
	public static final String DEFAULT_CONFIGURATION_FILE = "/home/kirank/Documents/svn/Games24x7/Backend-Automation/GamePlay/ConnectionBot/deployready/bot/conf/bots.props";
	private static PropsFileBasedConfiguration mainConfig;

	public static void main( String[] args ) {
		String file = DEFAULT_CONFIGURATION_FILE;
		if( args != null && args.length == 1 )
		{
			file = args[0];
		}
		mainConfig = new PropsFileBasedConfiguration(file);
		long userId = mainConfig.getLongValue( "STARTING_PLAYER_ID" );
		long tournamenId = mainConfig.getLongValue("MTT_TOURNAMENT_ID");
		int action = mainConfig.getIntValue("MTT_ACTION");
		String urlString = mainConfig.getStringValue( "DAEMON_URL" );
		System.out.println("urlString:"+urlString);
		long rampUpTime = mainConfig.getLongValue( "MTT_JOIN_REQUEST_DURATION_IN_SEC" )/mainConfig.getLongValue( "NO_OF_PLAYERS" );
		JoinMTTMain joinMTT = new JoinMTTMain();
		try {
			for(int i=0;i<mainConfig.getLongValue( "NO_OF_PLAYERS" );i++) {
				List<String> cookies=null;
				System.out.println("Sending request for getting session for user " + (userId+i));
				cookies = joinMTT.sendGetRequest( urlString+"/?userId="+(userId+i), cookies );
				if(cookies==null)
				{
					System.out.println("cokkies null");
				}
				else
				{
					
					for( String cookie : cookies ) 
					{
						System.out.println("cookie length "+cookie.length());
					}
					
					System.out.println("Sending join Request for user "+(userId+i));
					System.out.println(cookies.toString() + "cookies value");
					joinMTT.sendGetRequest( urlString+"/joinTourn.do?jsonp_callback=jQuery1810026167724980041385_1403094733068&tournamentId="+tournamenId+"&action="+action+"&rpJoined=0&userId="+(userId+i)+"&_=1403095246847",cookies );
					Thread.sleep( rampUpTime );
				}
				
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	private List< String > sendGetRequest(String urlString, List< String > cookies) throws Exception {
		URL url = new URL( urlString );
		//HttpURLConnection conn = ( HttpURLConnection ) url.openConnection();
		HttpsURLConnection conn = ( HttpsURLConnection ) url.openConnection();
		conn.setRequestMethod( "GET" );
		
			try {
				System.out.println("cookies here--->>> "+cookies);
				if(cookies!=null)
				{
				for( String cookie : cookies ) {
					conn.setRequestProperty( "Cookie", cookie.split( ";", 1 )[0] );
				}}
			}
			catch(NullPointerException e){
				System.out.println("Exception in catch");
				e.printStackTrace();
			}
			conn.connect();
			System.out.println("Response "+conn.getResponseCode());
			return conn.getHeaderFields().get( "Set-Cookie" );
		

		
		
	}
	
	}