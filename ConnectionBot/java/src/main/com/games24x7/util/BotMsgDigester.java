package com.games24x7.util;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.framework.configuration.Configuration;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.service.message.serializer.Serializer;
import com.games24x7.service.message.serializer.io.taggedtext.json.JSONMessageInputStream;

public class BotMsgDigester
{
	private ExecutorService executorDigester;
	private static Logger logger = LoggerFactory.getLogger( BotMsgDigester.class );
	private final ConcurrentMap< Long, DigesterTask > channelIdVsReceivedQ = new ConcurrentHashMap< Long, DigesterTask >();

	public BotMsgDigester( Configuration configuration )
		{
			executorDigester = Executors.newFixedThreadPool( configuration.getIntValue( "botAsync.tp.size", 40 ) );
		}

	public void enqueue( Message message, long channelId )
	{
		DigesterTask digesterTask = new DigesterTask();
		digesterTask.channelId = channelId;
		DigesterTask exisitingDigesterTask = channelIdVsReceivedQ.putIfAbsent( channelId, digesterTask );
		if( exisitingDigesterTask != null )
		{
			exisitingDigesterTask.messageQueue.add( message );
			digesterTask = exisitingDigesterTask;
		}
		else
		{
			digesterTask.messageQueue.add( message );
		}
		executorDigester.submit( digesterTask );

	}

	private static class DigesterTask implements Runnable
	{
		private long channelId;
		private BlockingQueue< Message > messageQueue = new LinkedBlockingQueue< Message >();

		@Override
		public void run()
		{
			List< Message > messages = null;
			synchronized( messageQueue )
			{
				messages = new ArrayList< Message >( messageQueue.size() );
				messageQueue.drainTo( messages );
			}
			for( Message message : messages )
			{
				try
				{
					BotMessageHandler messageHandler = BotImpl.getInstance().getMsgHandler( message.getClassID() );
					if( messageHandler == null )
					{
						logger.warn( "No message handler found for class ID - " + message.getClassID() );
					}
					else
					{
						Serializer serializer = BotImpl.getInstance().getBotSerializer();
						logger.debug( "Message Handler : " + messageHandler.getClass().getCanonicalName() + " with message " + message );
						messageHandler.handleMessage( ChannelIdVsPlayerInfo.get( channelId ).getUserSession(), message, ChannelIdVsPlayerInfo.get( channelId ) );
					}
				}
				catch( Exception e )
				{
					logger.error( "Exception :", e );
				}
			}

		}
	}

	public void removeSessionKey( String sessionKey )
	{
		channelIdVsReceivedQ.remove( sessionKey );
	}

}
