package com.games24x7.util;

import java.io.IOException;

import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFuture;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.service.message.Frame;
import com.games24x7.service.message.Message;
import com.games24x7.service.message.serializer.Serializer;
import com.games24x7.service.message.serializer.io.taggedtext.json.JSONMessageOutputStream;

public class SendMsgOnChannel
{
	public static boolean send(Message msg, Channel channel)
	{
		Serializer messageSerializer = BotImpl.getInstance().getBotSerializer();
		try
		{
			byte[] messageBytes = messageSerializer.serialize( new JSONMessageOutputStream( messageSerializer ), msg );
			ChannelFuture channelFuture = channel.write( new Frame( msg.getClassID(), msg.getReceiverID(), messageBytes ) );
			//System.out.println(channelFuture.getChannel().isConnected()+" "+channelFuture.getChannel().isOpen()+" "+channelFuture.getChannel().isWritable());
			/*if(channelFuture.isSuccess())
			{
				System.out.println("Success "+channelFuture.isDone());
				
			}
			else
				System.out.println("fail "+channelFuture.getCause());*/
			return true;

		}
		catch( IOException e )
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}

	}
}
