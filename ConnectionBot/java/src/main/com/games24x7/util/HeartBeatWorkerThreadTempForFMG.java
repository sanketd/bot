package com.games24x7.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

import com.games24x7.bot.message.HeartBeat;
import com.games24x7.init.BotMain;
import com.games24x7.service.message.Message;

public class HeartBeatWorkerThreadTempForFMG extends TimerTask
{
	boolean isFMGS = false;

	public HeartBeatWorkerThreadTempForFMG( boolean isFMG )
	{
		// TODO Auto-generated constructor stub
		this.isFMGS = isFMG;
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		if( BotMain.playerchannelMap.size() > 0 )
		{
			System.out.println( "Inside Run method:-" + BotMain.playerchannelMap.keySet() );
			for( Long key : BotMain.playerchannelMap.keySet() )
			{
				// BotMain.playerchannelMap.containsKey( key );
				System.out.println( "Inside " + key );
				long tableId = key;
				ArrayList< PlayerInfoWithChannelId > list = ( ArrayList< PlayerInfoWithChannelId > ) BotMain.playerchannelMap.get( key );
				if( list != null && list.size() > 0 )
				{
					for( int i = 0; i < list.size(); i++ )
					{
						PlayerInfoWithChannelId infoWithChannelId = ( PlayerInfoWithChannelId ) list.get( i );
						Message heartBeat = new HeartBeat( tableId, infoWithChannelId.getPlayerid() );
						System.out.println( "New Heart Beat Jitendra Time=" + new Date( System.currentTimeMillis() ) + ", sending heartbeat " + heartBeat.toString() + " "
								+ tableId + " playerId " + infoWithChannelId.getPlayerid() );
						SendMsgOnChannel.send( heartBeat, infoWithChannelId.getChannel() );
					}
				}
			}
		}

	}
}
