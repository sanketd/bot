package com.games24x7.util;

import java.util.List;
import java.util.Timer;
import java.util.concurrent.ScheduledFuture;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.message.PlayerObj;
import com.games24x7.service.UserSession;

public class PlayerInfo
{
	long playerId;
	long mPlayerId;
	long matchId;
	PlayerObj plObj = null;
	private List<List<String>> cards = null;
	String jkrCard = null;
	String openCard = null;
	int score = -1;
	Channel ch = null;
	int seatId;
	boolean isSeated = false;
	int settlementType;
	long tableId;
	Timer timer = null;
	int playerMoveCount = 0;
	UserSession us = null;
	boolean isHandshakeReceived = false;
	ScheduledFuture heartBeatTask = null;
	
	public PlayerInfo(long playerId, Channel ch)
	{
		this.playerId = playerId;
		this.ch=ch;
	}

	public long getMPlayerId()
	{
		return mPlayerId;
	}

	public void setMPlayerId( long mPlayerId )
	{
		this.mPlayerId = mPlayerId;
	}

	public PlayerObj getPlayerObj()
	{
		return plObj;
	}

	public void setPlayerObj( PlayerObj plObj )
	{
		this.plObj = plObj;
	}

	public List< List< String >> getCards()
	{
		return cards;
	}

	public void setCards( List< List< String >> cards )
	{
		this.cards = cards;
	}

	public String getJkrCard()
	{
		return jkrCard;
	}

	public void setJkrCard( String jkrCard )
	{
		this.jkrCard = jkrCard;
	}

	public long getMatchId()
	{
		return matchId;
	}

	public void setMatchId( long matchId )
	{
		this.matchId = matchId;
	}

	public String getOpenCard()
	{
		return openCard;
	}

	public void setOpenCard( String openCard )
	{
		this.openCard = openCard;
	}

	public int getScore()
	{
		return score;
	}

	public void setScore( int score )
	{
		this.score = score;
	}

	public Channel getChannel()
	{
		return ch;
	}

	public void setChannel( Channel ch )
	{
		this.ch = ch;
	}

	public long getPlayerId()
	{
		return playerId;
	}

	public void setPlayerId( long playerId )
	{
		this.playerId = playerId;
	}

	public int getSeatId()
	{
		return seatId;
	}

	public void setSeatId( int seatId )
	{
		this.seatId = seatId;
	}

	public boolean isSeated()
	{
		return isSeated;
	}

	public void setSeated( boolean isSeated )
	{
		this.isSeated = isSeated;
	}

	public int getSettlementType()
	{
		return settlementType;
	}

	public void setSettlementType( int settlementType )
	{
		this.settlementType = settlementType;
	}

	public long getTableId()
	{
		return tableId;
	}

	public void setTableId( long tableId )
	{
		this.tableId = tableId;
	}

	public Timer getTimer()
	{
		return timer;
	}

	public void setTimer( Timer setTimer)
	{
		this.timer = setTimer;
	}

	public int getPlayerMoveCount()
	{
		return playerMoveCount;
	}

	public void setPlayerMoveCount( int playerMoveCount )
	{
		this.playerMoveCount = playerMoveCount;
	}

	public UserSession getUserSession()
	{
		return us;
	}

	public void setUserSession( UserSession us )
	{
		this.us = us;
	}

	public boolean isHandshakeReceived()
	{
		return isHandshakeReceived;
	}

	public void setHandshakeReceived( boolean isHandshakeReceived )
	{
		this.isHandshakeReceived = isHandshakeReceived;
	}

	public ScheduledFuture getHeartBeatTask()
	{
		return heartBeatTask;
	}

	public void setHeartBeatTask( ScheduledFuture heartBeatTask )
	{
		this.heartBeatTask = heartBeatTask;
	}
	
	}
