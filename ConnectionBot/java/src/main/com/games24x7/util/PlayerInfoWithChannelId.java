package com.games24x7.util;

import org.jboss.netty.channel.Channel;

public class PlayerInfoWithChannelId
{
	private Channel channel = null;
	private Long playerid = null;

	public Channel getChannel()
	{
		return channel;
	}

	public void setChannel( Channel channel )
	{
		this.channel = channel;
	}

	public Long getPlayerid()
	{
		return playerid;
	}

	public void setPlayerid( Long playerid )
	{
		this.playerid = playerid;
	}
}
