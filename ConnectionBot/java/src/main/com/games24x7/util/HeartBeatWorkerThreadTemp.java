package com.games24x7.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.TimerTask;

import com.games24x7.bot.message.HeartBeat;
import com.games24x7.init.BotMain;
import com.games24x7.service.message.Message;

public class HeartBeatWorkerThreadTemp extends TimerTask
{

	long tableId;

	public HeartBeatWorkerThreadTemp( long tableid )
	{
		// TODO Auto-generated constructor stub
		this.tableId = tableid;
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		//System.out.println("condition:-"+BotMain.playerchannelMap.containsKey( tableId ));
		//System.out.println("My table id is:"+tableId);
		if( BotMain.playerchannelMap.containsKey( tableId ) )
		{
			ArrayList< PlayerInfoWithChannelId > list = ( ArrayList< PlayerInfoWithChannelId > ) BotMain.playerchannelMap.get( tableId );
			if( list != null && list.size() > 0 )
			{
				for( int i = 0; i < list.size(); i++ )
				{
					PlayerInfoWithChannelId infoWithChannelId = (PlayerInfoWithChannelId)list.get( i );
					Message heartBeat = new HeartBeat( tableId, infoWithChannelId.getPlayerid() );
					System.out.println( "New Heart Beat Jitendra Time=" + new Date( System.currentTimeMillis() ) + ", sending heartbeat " + heartBeat.toString() + " " + tableId + " playerId "+ infoWithChannelId.getPlayerid() );
					SendMsgOnChannel.send( heartBeat, infoWithChannelId.getChannel() );
				}
			}
		}
	}
}
