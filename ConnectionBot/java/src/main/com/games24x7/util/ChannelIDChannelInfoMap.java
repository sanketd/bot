package com.games24x7.util;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.jboss.netty.channel.Channel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChannelIDChannelInfoMap
{

        private static final Map<Long, ChannelInfo> channelIDvsChannelMap = new ConcurrentHashMap<Long, ChannelInfo>();
        private static Logger logger = LoggerFactory.getLogger( ChannelIDChannelInfoMap.class );

        public static ChannelInfo createChannelIDAndAddInfoToMap( Channel channel )
        {
        	System.out.println("inside channelInfo channelmap");
                long channelID = channel.getId();
                ChannelInfo channelInfo = new ChannelInfo( channelID, channel, UUID.randomUUID().toString() );
                return channelInfo;
        }

        public static void removeMapping( long channelID )
        {
                channelIDvsChannelMap.remove( channelID );
        }

        public static ChannelInfo getChannelInfo( long channelID )
        {
                return channelIDvsChannelMap.get( channelID );
        }
}