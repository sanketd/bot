package com.games24x7.util;

import org.jboss.netty.channel.Channel;

/**
 * User: phani Date: 5/28/13 Time: 12:28 PM
 */
public class ChannelInfo
{
	private long channelID;
	private Channel channel;
	private String sessionKey;

	public ChannelInfo( long channelID, Channel channel, String sessionKey )
	{
		this.channelID = channelID;
		this.channel = channel;
		this.sessionKey = sessionKey;
	}

	public Long getChannelID()
	{
		return channelID;
	}

	public Channel getChannel()
	{
		return channel;
	}

	public String getSessionKey()
	{
		return sessionKey;
	}
}
