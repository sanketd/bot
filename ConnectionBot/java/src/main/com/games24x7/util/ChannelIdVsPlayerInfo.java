package com.games24x7.util;

import java.util.concurrent.ConcurrentHashMap;

public class ChannelIdVsPlayerInfo
{

	static ConcurrentHashMap< Long, PlayerInfo > channelIdvsPlrInfo = new ConcurrentHashMap< Long, PlayerInfo>();
	public static PlayerInfo get(long channelId)
	{
		return channelIdvsPlrInfo.get( channelId );
	}
	public static void put(long channelId, PlayerInfo plrInfo)
	{
		channelIdvsPlrInfo.put( channelId, plrInfo );
	}
}
