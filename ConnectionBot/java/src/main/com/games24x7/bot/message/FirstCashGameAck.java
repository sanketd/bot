package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class FirstCashGameAck extends AbstractMessage { 
 

    public FirstCashGameAck() {
        super(1048592, 0, 0);
    }

    public FirstCashGameAck(long receiverID, long playerID) {
        super(1048592, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"FirstCashGameAck{" +
        "}";
    }
}
