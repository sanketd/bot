package com.games24x7.bot.message.handler;

import java.util.ArrayList;

import com.games24x7.bot.message.Ready;
import com.games24x7.bot.message.SetupRes;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.PlayerInfoWithChannelId;
import com.games24x7.util.SendMsgOnChannel;

public class SetupResHandler implements BotMessageHandler< SetupRes >
{

	@Override
	public void handleMessage( UserSession session, SetupRes message, PlayerInfo playerInfo )
	{
		try
		{
			long aaid = message.getAaid();
			long playerId = playerInfo.getPlayerId();
			String sessionId = message.getSessionId();
			long tableId = message.getTableid();
			String bgImage = message.getBgimgurl();
			int maxPlayer = message.getMaxplayer();
			System.out.println( "Setup Response 111 " + aaid + " " + playerId + " " + sessionId + " " + tableId + " " + bgImage + " " + maxPlayer );

			// getting timer for a table
			/*if( !TableIDVSTimer.contains( message.getAaid() ) )
			{
				System.out.println( "Timer not recived.............................." );
				TableIDVSTimer.add( message.getAaid(), new Timer() );
				System.out.println( "Timer created.............................." );
			}*/
			Ready ready = new Ready( aaid, playerId );
			ready.setSource( String.valueOf( playerId ) );
			ready.setPlayerID( playerId );
			System.out.println( "Sending Ready msg " + ready );
			if( BotMain.playerchannelMap.containsKey( message.getAaid() ) )
			{
				// System.out.println("Aaaid======================================"+
				// BotMain.playerchannelMap.get(
				// plrInfo.getTableId()).toString());
				ArrayList< PlayerInfoWithChannelId > list = ( ArrayList< PlayerInfoWithChannelId > ) BotMain.playerchannelMap.get( message.getAaid() );
				PlayerInfoWithChannelId playerInfoWithChannelId = new PlayerInfoWithChannelId();
				playerInfoWithChannelId.setChannel( playerInfo.getChannel() );
				playerInfoWithChannelId.setPlayerid( playerInfo.getPlayerId() );
				list.add( playerInfoWithChannelId );
				BotMain.playerchannelMap.put( message.getAaid(), list );
			}
			else
			{
				// System.out.println("Aaaid2======================================"+
				// BotMain.playerchannelMap.get(
				// plrInfo.getTableId()).toString());
				ArrayList< PlayerInfoWithChannelId > list = new ArrayList< PlayerInfoWithChannelId >();
				PlayerInfoWithChannelId playerInfoWithChannelId = new PlayerInfoWithChannelId();
				playerInfoWithChannelId.setChannel( playerInfo.getChannel() );
				playerInfoWithChannelId.setPlayerid( playerInfo.getPlayerId() );
				list.add( playerInfoWithChannelId );
				BotMain.playerchannelMap.put( message.getAaid(), list );
			}
			//TableIDVSTimer.get( aaid ).schedule( new HeartBeatWorkerThread( playerInfo.getChannel(), aaid, playerId ), 2000, 2000 );
			SendMsgOnChannel.send( ready, playerInfo.getChannel() );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

	}

}
