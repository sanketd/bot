package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class LastDealReq extends AbstractMessage { 
 

    public LastDealReq() {
        super(1048597, 0, 0);
    }

    public LastDealReq(long receiverID, long playerID) {
        super(1048597, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"LastDealReq{" +
        "}";
    }
}
