package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class SplitStatusPlayerList extends ChildAbstractMessage{
 
    private String loginid;
    private String status;

    public SplitStatusPlayerList() {
    super(1048658);
    }

    public SplitStatusPlayerList( String loginid,String status) {
         super(1048658);
        this.loginid = loginid;
        this.status = status;
    }




    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SplitStatusPlayerList{" +
            "loginid=" + loginid +
            "status=" + status +
        "}";
    }
}
