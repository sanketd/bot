package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DisableSplit extends AbstractMessage { 
 

    public DisableSplit() {
        super(1048734, 0, 0);
    }

    public DisableSplit(long receiverID, long playerID) {
        super(1048734, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DisableSplit{" +
        "}";
    }
}
