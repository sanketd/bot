package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ThemeReq extends AbstractMessage { 
 
    private String game;
    private int value;

    public ThemeReq() {
        super(1048613, 0, 0);
    }

    public ThemeReq(long receiverID, long playerID, String game,int value) {
        super(1048613, receiverID, playerID);
        this.game = game;
        this.value = value;
    }




    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ThemeReq{" +
            "game=" + game +
            "value=" + value +
        "}";
    }
}
