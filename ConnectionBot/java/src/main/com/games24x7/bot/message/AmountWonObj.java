package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AmountWonObj extends AbstractMessage { 
 
    private double amount;
    private String loginid;
    private int totalscore;
    private double tablebal;
    private long mpid;

    public AmountWonObj() {
        super(1048687, 0, 0);
    }

    public AmountWonObj(long receiverID, long playerID, double amount,String loginid,int totalscore,double tablebal,long mpid) {
        super(1048687, receiverID, playerID);
        this.amount = amount;
        this.loginid = loginid;
        this.totalscore = totalscore;
        this.tablebal = tablebal;
        this.mpid = mpid;
    }




    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public int getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(int totalscore) {
        this.totalscore = totalscore;
    }

    public double getTablebal() {
        return tablebal;
    }

    public void setTablebal(double tablebal) {
        this.tablebal = tablebal;
    }

    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AmountWonObj{" +
            "amount=" + amount +
            "loginid=" + loginid +
            "totalscore=" + totalscore +
            "tablebal=" + tablebal +
            "mpid=" + mpid +
        "}";
    }
}
