package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ErrorMsg extends AbstractMessage { 
 
    private int msgid;
    private String title;
    private boolean bootout;
    private String msg;
    private long aaid;
    private long matchid;
    private long gameid;

    public ErrorMsg() {
        super(1048712, 0, 0);
    }

    public ErrorMsg(long receiverID, long playerID, int msgid,String title,boolean bootout,String msg,long aaid,long matchid,long gameid) {
        super(1048712, receiverID, playerID);
        this.msgid = msgid;
        this.title = title;
        this.bootout = bootout;
        this.msg = msg;
        this.aaid = aaid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public int getMsgid() {
        return msgid;
    }

    public void setMsgid(int msgid) {
        this.msgid = msgid;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBootout() {
        return bootout;
    }

    public void setBootout(boolean bootout) {
        this.bootout = bootout;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ErrorMsg{" +
            "msgid=" + msgid +
            "title=" + title +
            "bootout=" + bootout +
            "msg=" + msg +
            "aaid=" + aaid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
