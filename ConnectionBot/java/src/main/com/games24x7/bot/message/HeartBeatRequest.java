package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class HeartBeatRequest extends AbstractMessage
{
	private int msgId;

	public HeartBeatRequest()
	{
		super( 1048583, 0, 0 );
	}

	public HeartBeatRequest( long receiverID, long playerID, int msgId )
	{
		super( 1048583, receiverID, playerID );
		this.msgId = msgId;
	}

	public long getMsgId()
	{
		return msgId;
	}

	public void setMsgId( int msgId )
	{
		this.msgId = msgId;
	}

	@Override
	public String toString()
	{
		return "HeartBeatRequest{" + "msgId=" + msgId + "}";
	}
}
