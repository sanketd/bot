package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class MoveToTable extends AbstractMessage
{

	public MoveToTable()
	{
		super( 1048773, 0, 0 );
	}

	public MoveToTable( long receiverID, long playerID )
	{
		super( 1048773, receiverID, playerID );
	}

	@Override
	public String toString()
	{
		return "Super :" + super.toString() + " Child " + "MoveToTable{" + "}";
	}
}