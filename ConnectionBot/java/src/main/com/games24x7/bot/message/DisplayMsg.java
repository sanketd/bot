package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DisplayMsg extends AbstractMessage { 
 
    private String msg;
    private long aaid;

    public DisplayMsg() {
        super(1048732, 0, 0);
    }

    public DisplayMsg(long receiverID, long playerID, String msg,long aaid) {
        super(1048732, receiverID, playerID);
        this.msg = msg;
        this.aaid = aaid;
    }




    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DisplayMsg{" +
            "msg=" + msg +
            "aaid=" + aaid +
        "}";
    }
}
