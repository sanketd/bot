package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RmValue extends AbstractMessage { 
 
    private String rmname;
    private String rmnumber;
    private String rmemail;

    public RmValue() {
        super(1048740, 0, 0);
    }

    public RmValue(long receiverID, long playerID, String rmname,String rmnumber,String rmemail) {
        super(1048740, receiverID, playerID);
        this.rmname = rmname;
        this.rmnumber = rmnumber;
        this.rmemail = rmemail;
    }




    public String getRmname() {
        return rmname;
    }

    public void setRmname(String rmname) {
        this.rmname = rmname;
    }

    public String getRmnumber() {
        return rmnumber;
    }

    public void setRmnumber(String rmnumber) {
        this.rmnumber = rmnumber;
    }

    public String getRmemail() {
        return rmemail;
    }

    public void setRmemail(String rmemail) {
        this.rmemail = rmemail;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RmValue{" +
            "rmname=" + rmname +
            "rmnumber=" + rmnumber +
            "rmemail=" + rmemail +
        "}";
    }
}
