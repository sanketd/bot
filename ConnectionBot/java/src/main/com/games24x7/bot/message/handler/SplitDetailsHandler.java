package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.AcceptSplit;
import com.games24x7.bot.message.SplitDetails;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class SplitDetailsHandler implements BotMessageHandler< SplitDetails >
{
	

	@Override
	public void handleMessage( UserSession session, SplitDetails message, PlayerInfo plrInfo )

	{
		
		System.out.println( "Split details Message" + message );
		Message acceptsplit = new AcceptSplit( message.getReceiverID(), plrInfo.getPlayerId() );
		SendMsgOnChannel.send( acceptsplit, plrInfo.getChannel() );
		System.out.println( "***************SPLIT SUCCESSFULLY DONE****************" +plrInfo.getPlayerId());

		long[] players = new long[1];
		players[0] = plrInfo.getPlayerId();
		BotMain.getBotImpl().getBots().execute( BotMain.getMainConfig().getIntValue( "NO_OF_TOURNAMENT" ), BotMain.getMainConfig().getIntValue( "TOURNAMENT_ID_INCREAMENT" ), 0,
				BotMain.getMainConfig().getIntValue( "PLAYERS_ON_A_TABLE" ), players, BotMain.getMainConfig().getStringValue( "CS_IP" ),
				BotMain.getMainConfig().getIntValue( "CS_PORT" ), BotMain.getMainConfig().getIntValue( "PLAYER_ID_INCREAMENT" ), false,
				BotMain.getMainConfig().getLongValue( "MTT_TOURNAMENT_ID" ), BotMain.getMainConfig().getIntValue( "SETTLEMENT_TYPE" ),
				BotMain.getMainConfig().getIntValue( "CHANNEL_ID" ), BotMain.getMainConfig().getBooleanValue( "FMG_FLAG" ), BotMain.getMainConfig().getIntValue( "FMG_AMOUNT" ),
				BotMain.getMainConfig().getIntValue( "NO_OF_PLAYERS" ) );

	}

}
