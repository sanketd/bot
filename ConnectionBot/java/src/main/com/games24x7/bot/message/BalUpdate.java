package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BalUpdate extends AbstractMessage { 
 
    private long aaid;
    private List<BalUpdateObj> balarray;

    public BalUpdate() {
        super(1048693, 0, 0);
    }

    public BalUpdate(long receiverID, long playerID, long aaid,List<BalUpdateObj> balarray) {
        super(1048693, receiverID, playerID);
        this.aaid = aaid;
        this.balarray = balarray;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<BalUpdateObj> getBalarray() {
        return balarray;
    }

    public void setBalarray(List<BalUpdateObj> balarray) {
        this.balarray = balarray;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BalUpdate{" +
            "aaid=" + aaid +
            "balarray=" + balarray +
        "}";
    }
}
