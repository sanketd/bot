package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PrizePool extends AbstractMessage { 
 
    private double prize;
    private long aaid;

    public PrizePool() {
        super(1048638, 0, 0);
    }

    public PrizePool(long receiverID, long playerID, double prize,long aaid) {
        super(1048638, receiverID, playerID);
        this.prize = prize;
        this.aaid = aaid;
    }




    public double getPrize() {
        return prize;
    }

    public void setPrize(double prize) {
        this.prize = prize;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PrizePool{" +
            "prize=" + prize +
            "aaid=" + aaid +
        "}";
    }
}
