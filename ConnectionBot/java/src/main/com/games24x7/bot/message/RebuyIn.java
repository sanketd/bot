package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RebuyIn extends AbstractMessage { 
 
    private int seatid;
    private double bal;
    private double tablemax;
    private double tablemin;
    private double customamnt;
    private int clientvariant;

    public RebuyIn() {
        super(1048604, 0, 0);
    }

    public RebuyIn(long receiverID, long playerID, int seatid,double bal,double tablemax,double tablemin,double customamnt,int clientvariant) {
        super(1048604, receiverID, playerID);
        this.seatid = seatid;
        this.bal = bal;
        this.tablemax = tablemax;
        this.tablemin = tablemin;
        this.customamnt = customamnt;
        this.clientvariant = clientvariant;
    }




    public int getSeatid() {
        return seatid;
    }

    public void setSeatid(int seatid) {
        this.seatid = seatid;
    }

    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    public double getTablemax() {
        return tablemax;
    }

    public void setTablemax(double tablemax) {
        this.tablemax = tablemax;
    }

    public double getTablemin() {
        return tablemin;
    }

    public void setTablemin(double tablemin) {
        this.tablemin = tablemin;
    }

    public double getCustomamnt() {
        return customamnt;
    }

    public void setCustomamnt(double customamnt) {
        this.customamnt = customamnt;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RebuyIn{" +
            "seatid=" + seatid +
            "bal=" + bal +
            "tablemax=" + tablemax +
            "tablemin=" + tablemin +
            "customamnt=" + customamnt +
            "clientvariant=" + clientvariant +
        "}";
    }
}
