package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class UpdateTheme extends AbstractMessage { 
 
    private long tableid;
    private int themeid;

    public UpdateTheme() {
        super(1048608, 0, 0);
    }

    public UpdateTheme(long receiverID, long playerID, long tableid,int themeid) {
        super(1048608, receiverID, playerID);
        this.tableid = tableid;
        this.themeid = themeid;
    }




    public long getTableid() {
        return tableid;
    }

    public void setTableid(long tableid) {
        this.tableid = tableid;
    }

    public int getThemeid() {
        return themeid;
    }

    public void setThemeid(int themeid) {
        this.themeid = themeid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"UpdateTheme{" +
            "tableid=" + tableid +
            "themeid=" + themeid +
        "}";
    }
}
