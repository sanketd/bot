package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class MttTableMap extends AbstractMessage {
    private long tournamentid;

    public MttTableMap() {
        super(2097157, 0, 0);
    }

    public MttTableMap(long receiverID, long playerID , long tournamentid) {
        super(2097157, receiverID, playerID);
        this.tournamentid = tournamentid;
    }

    public long getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(long tournamentid) {
        this.tournamentid = tournamentid;
    }

    @Override
    public String toString() {
        return "MttTableMap{" +
            "tournamentid=" + tournamentid +
        "}";
    }
}
