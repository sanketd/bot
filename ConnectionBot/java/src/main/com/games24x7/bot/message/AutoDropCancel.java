package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoDropCancel extends AbstractMessage { 
 

    public AutoDropCancel() {
        super(1048579, 0, 0);
    }

    public AutoDropCancel(long receiverID, long playerID) {
        super(1048579, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoDropCancel{" +
        "}";
    }
}
