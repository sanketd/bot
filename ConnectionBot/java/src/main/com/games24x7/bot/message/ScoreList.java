package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ScoreList extends ChildAbstractMessage{
 
    private long score;
    private String loginid;
    private long totalscore;

    public ScoreList() {
    super(1048629);
    }

    public ScoreList( long score,String loginid,long totalscore) {
         super(1048629);
        this.score = score;
        this.loginid = loginid;
        this.totalscore = totalscore;
    }




    public long getScore() {
        return score;
    }

    public void setScore(long score) {
        this.score = score;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public long getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(long totalscore) {
        this.totalscore = totalscore;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreList{" +
            "score=" + score +
            "loginid=" + loginid +
            "totalscore=" + totalscore +
        "}";
    }
}
