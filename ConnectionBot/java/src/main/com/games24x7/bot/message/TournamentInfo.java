package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TournamentInfo extends AbstractMessage { 
 
    private double totalprize;
    private boolean hasticket;
    private TournamentInfoObj tournamentinfo;

    public TournamentInfo() {
        super(1048670, 0, 0);
    }

    public TournamentInfo(long receiverID, long playerID, double totalprize,boolean hasticket,TournamentInfoObj tournamentinfo) {
        super(1048670, receiverID, playerID);
        this.totalprize = totalprize;
        this.hasticket = hasticket;
        this.tournamentinfo = tournamentinfo;
    }




    public double getTotalprize() {
        return totalprize;
    }

    public void setTotalprize(double totalprize) {
        this.totalprize = totalprize;
    }

    public boolean isHasticket() {
        return hasticket;
    }

    public void setHasticket(boolean hasticket) {
        this.hasticket = hasticket;
    }

    public TournamentInfoObj getTournamentinfo() {
        return tournamentinfo;
    }

    public void setTournamentinfo(TournamentInfoObj tournamentinfo) {
        this.tournamentinfo = tournamentinfo;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TournamentInfo{" +
            "totalprize=" + totalprize +
            "hasticket=" + hasticket +
            "tournamentinfo=" + tournamentinfo +
        "}";
    }
}
