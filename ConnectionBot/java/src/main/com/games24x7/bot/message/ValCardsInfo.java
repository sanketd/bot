package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ValCardsInfo extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private boolean lastdeal;
    private String loginid;
    private ValCardTxns valcardtxns;

    public ValCardsInfo() {
        super(1048676, 0, 0);
    }

    public ValCardsInfo(long receiverID, long playerID, long matchid,long gameid,boolean lastdeal,String loginid,ValCardTxns valcardtxns) {
        super(1048676, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.lastdeal = lastdeal;
        this.loginid = loginid;
        this.valcardtxns = valcardtxns;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public boolean isLastdeal() {
        return lastdeal;
    }

    public void setLastdeal(boolean lastdeal) {
        this.lastdeal = lastdeal;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public ValCardTxns getValcardtxns() {
        return valcardtxns;
    }

    public void setValcardtxns(ValCardTxns valcardtxns) {
        this.valcardtxns = valcardtxns;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ValCardsInfo{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "lastdeal=" + lastdeal +
            "loginid=" + loginid +
            "valcardtxns=" + valcardtxns +
        "}";
    }
}
