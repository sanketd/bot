package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ClubInfoArrayObj extends AbstractMessage { 
 
    private double bonuses;
    private double rewardpoints;
    private int qualperiod;
    private int validity;
    private double tournaments;
    private int clubid;
    private int lpsneeded;
    private String clubname;

    public ClubInfoArrayObj() {
        super(1048723, 0, 0);
    }

    public ClubInfoArrayObj(long receiverID, long playerID, double bonuses,double rewardpoints,int qualperiod,int validity,double tournaments,int clubid,int lpsneeded,String clubname) {
        super(1048723, receiverID, playerID);
        this.bonuses = bonuses;
        this.rewardpoints = rewardpoints;
        this.qualperiod = qualperiod;
        this.validity = validity;
        this.tournaments = tournaments;
        this.clubid = clubid;
        this.lpsneeded = lpsneeded;
        this.clubname = clubname;
    }




    public double getBonuses() {
        return bonuses;
    }

    public void setBonuses(double bonuses) {
        this.bonuses = bonuses;
    }

    public double getRewardpoints() {
        return rewardpoints;
    }

    public void setRewardpoints(double rewardpoints) {
        this.rewardpoints = rewardpoints;
    }

    public int getQualperiod() {
        return qualperiod;
    }

    public void setQualperiod(int qualperiod) {
        this.qualperiod = qualperiod;
    }

    public int getValidity() {
        return validity;
    }

    public void setValidity(int validity) {
        this.validity = validity;
    }

    public double getTournaments() {
        return tournaments;
    }

    public void setTournaments(double tournaments) {
        this.tournaments = tournaments;
    }

    public int getClubid() {
        return clubid;
    }

    public void setClubid(int clubid) {
        this.clubid = clubid;
    }

    public int getLpsneeded() {
        return lpsneeded;
    }

    public void setLpsneeded(int lpsneeded) {
        this.lpsneeded = lpsneeded;
    }

    public String getClubname() {
        return clubname;
    }

    public void setClubname(String clubname) {
        this.clubname = clubname;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ClubInfoArrayObj{" +
            "bonuses=" + bonuses +
            "rewardpoints=" + rewardpoints +
            "qualperiod=" + qualperiod +
            "validity=" + validity +
            "tournaments=" + tournaments +
            "clubid=" + clubid +
            "lpsneeded=" + lpsneeded +
            "clubname=" + clubname +
        "}";
    }
}
