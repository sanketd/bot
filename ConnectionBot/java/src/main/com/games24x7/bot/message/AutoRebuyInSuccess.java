package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoRebuyInSuccess extends AbstractMessage { 
 
    private double rebuyinamount;

    public AutoRebuyInSuccess() {
        super(1048741, 0, 0);
    }

    public AutoRebuyInSuccess(long receiverID, long playerID, double rebuyinamount) {
        super(1048741, receiverID, playerID);
        this.rebuyinamount = rebuyinamount;
    }




    public double getRebuyinamount() {
        return rebuyinamount;
    }

    public void setRebuyinamount(double rebuyinamount) {
        this.rebuyinamount = rebuyinamount;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoRebuyInSuccess{" +
            "rebuyinamount=" + rebuyinamount +
        "}";
    }
}
