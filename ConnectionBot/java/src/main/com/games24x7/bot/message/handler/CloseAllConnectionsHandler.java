package com.games24x7.bot.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.CloseAllConnections;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class CloseAllConnectionsHandler implements BotMessageHandler< CloseAllConnections >
{
	private Logger logger = LoggerFactory.getLogger(CloseAllConnectionsHandler.class );
	@Override
	public void handleMessage( UserSession session, CloseAllConnections message, PlayerInfo playerInfo )
	{
		System.out.println(  "Completed creating new 1");
		
		if( BotImpl.getInstance().getConfiguration().getIntValue( "SETTLEMENT_TYPE" ) == 2 && BotImpl.getInstance().getConfiguration().getBooleanValue( "INFINITE_GAME" ) == true )
		{

			
			long playerId = playerInfo.getPlayerId();
			System.out.println(  "Completed creating new "+playerId );
			BotMain.executeInfiniteGame( playerId );
		}
		else
		{
			playerInfo.getChannel().disconnect();
		}
	}

}
