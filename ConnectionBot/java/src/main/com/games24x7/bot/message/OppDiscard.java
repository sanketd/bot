package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OppDiscard extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private long mplayerid;
    private String cardid;

    public OppDiscard() {
        super(1048619, 0, 0);
    }

    public OppDiscard(long receiverID, long playerID, long matchid,long gameid,long mplayerid,String cardid) {
        super(1048619, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.cardid = cardid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OppDiscard{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "cardid=" + cardid +
        "}";
    }
}
