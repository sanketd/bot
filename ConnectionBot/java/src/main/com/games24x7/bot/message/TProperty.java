package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TProperty extends AbstractMessage { 
 
    private long splittimeout;
    private double entryfee;
    private long rejointimeout;
    private int settlementtype;
    private double typezeroentryfeelimit;
    private double maxbuyin;
    private double pointvalue;
    private double servicefee;
    private int mindropsforsplit;
    private int maxplayer;
    private boolean splitallowed;
    private boolean rejoinallowed;
    private int poolmaxscore;
    private long countinbtwmatch;
    private long buyintimeout;
    private String name;
    private int mindealtoplay;
    private int servicefeetype;
    private int prizetype;
    private long countdwnstartgame;
    private double minbuyin;

    public TProperty() {
        super(1048682, 0, 0);
    }

    public TProperty(long receiverID, long playerID, long splittimeout,double entryfee,long rejointimeout,int settlementtype,double typezeroentryfeelimit,double maxbuyin,double pointvalue,double servicefee,int mindropsforsplit,int maxplayer,boolean splitallowed,boolean rejoinallowed,int poolmaxscore,long countinbtwmatch,long buyintimeout,String name,int mindealtoplay,int servicefeetype,int prizetype,long countdwnstartgame,double minbuyin) {
        super(1048682, receiverID, playerID);
        this.splittimeout = splittimeout;
        this.entryfee = entryfee;
        this.rejointimeout = rejointimeout;
        this.settlementtype = settlementtype;
        this.typezeroentryfeelimit = typezeroentryfeelimit;
        this.maxbuyin = maxbuyin;
        this.pointvalue = pointvalue;
        this.servicefee = servicefee;
        this.mindropsforsplit = mindropsforsplit;
        this.maxplayer = maxplayer;
        this.splitallowed = splitallowed;
        this.rejoinallowed = rejoinallowed;
        this.poolmaxscore = poolmaxscore;
        this.countinbtwmatch = countinbtwmatch;
        this.buyintimeout = buyintimeout;
        this.name = name;
        this.mindealtoplay = mindealtoplay;
        this.servicefeetype = servicefeetype;
        this.prizetype = prizetype;
        this.countdwnstartgame = countdwnstartgame;
        this.minbuyin = minbuyin;
    }




    public long getSplittimeout() {
        return splittimeout;
    }

    public void setSplittimeout(long splittimeout) {
        this.splittimeout = splittimeout;
    }

    public double getEntryfee() {
        return entryfee;
    }

    public void setEntryfee(double entryfee) {
        this.entryfee = entryfee;
    }

    public long getRejointimeout() {
        return rejointimeout;
    }

    public void setRejointimeout(long rejointimeout) {
        this.rejointimeout = rejointimeout;
    }

    public int getSettlementtype() {
        return settlementtype;
    }

    public void setSettlementtype(int settlementtype) {
        this.settlementtype = settlementtype;
    }

    public double getTypezeroentryfeelimit() {
        return typezeroentryfeelimit;
    }

    public void setTypezeroentryfeelimit(double typezeroentryfeelimit) {
        this.typezeroentryfeelimit = typezeroentryfeelimit;
    }

    public double getMaxbuyin() {
        return maxbuyin;
    }

    public void setMaxbuyin(double maxbuyin) {
        this.maxbuyin = maxbuyin;
    }

    public double getPointvalue() {
        return pointvalue;
    }

    public void setPointvalue(double pointvalue) {
        this.pointvalue = pointvalue;
    }

    public double getServicefee() {
        return servicefee;
    }

    public void setServicefee(double servicefee) {
        this.servicefee = servicefee;
    }

    public int getMindropsforsplit() {
        return mindropsforsplit;
    }

    public void setMindropsforsplit(int mindropsforsplit) {
        this.mindropsforsplit = mindropsforsplit;
    }

    public int getMaxplayer() {
        return maxplayer;
    }

    public void setMaxplayer(int maxplayer) {
        this.maxplayer = maxplayer;
    }

    public boolean isSplitallowed() {
        return splitallowed;
    }

    public void setSplitallowed(boolean splitallowed) {
        this.splitallowed = splitallowed;
    }

    public boolean isRejoinallowed() {
        return rejoinallowed;
    }

    public void setRejoinallowed(boolean rejoinallowed) {
        this.rejoinallowed = rejoinallowed;
    }

    public int getPoolmaxscore() {
        return poolmaxscore;
    }

    public void setPoolmaxscore(int poolmaxscore) {
        this.poolmaxscore = poolmaxscore;
    }

    public long getCountinbtwmatch() {
        return countinbtwmatch;
    }

    public void setCountinbtwmatch(long countinbtwmatch) {
        this.countinbtwmatch = countinbtwmatch;
    }

    public long getBuyintimeout() {
        return buyintimeout;
    }

    public void setBuyintimeout(long buyintimeout) {
        this.buyintimeout = buyintimeout;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMindealtoplay() {
        return mindealtoplay;
    }

    public void setMindealtoplay(int mindealtoplay) {
        this.mindealtoplay = mindealtoplay;
    }

    public int getServicefeetype() {
        return servicefeetype;
    }

    public void setServicefeetype(int servicefeetype) {
        this.servicefeetype = servicefeetype;
    }

    public int getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(int prizetype) {
        this.prizetype = prizetype;
    }

    public long getCountdwnstartgame() {
        return countdwnstartgame;
    }

    public void setCountdwnstartgame(long countdwnstartgame) {
        this.countdwnstartgame = countdwnstartgame;
    }

    public double getMinbuyin() {
        return minbuyin;
    }

    public void setMinbuyin(double minbuyin) {
        this.minbuyin = minbuyin;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TProperty{" +
            "splittimeout=" + splittimeout +
            "entryfee=" + entryfee +
            "rejointimeout=" + rejointimeout +
            "settlementtype=" + settlementtype +
            "typezeroentryfeelimit=" + typezeroentryfeelimit +
            "maxbuyin=" + maxbuyin +
            "pointvalue=" + pointvalue +
            "servicefee=" + servicefee +
            "mindropsforsplit=" + mindropsforsplit +
            "maxplayer=" + maxplayer +
            "splitallowed=" + splitallowed +
            "rejoinallowed=" + rejoinallowed +
            "poolmaxscore=" + poolmaxscore +
            "countinbtwmatch=" + countinbtwmatch +
            "buyintimeout=" + buyintimeout +
            "name=" + name +
            "mindealtoplay=" + mindealtoplay +
            "servicefeetype=" + servicefeetype +
            "prizetype=" + prizetype +
            "countdwnstartgame=" + countdwnstartgame +
            "minbuyin=" + minbuyin +
        "}";
    }
}
