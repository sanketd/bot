package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DisconProtectionPrefUpdateRes extends AbstractMessage { 
 
    private boolean flag;

    public DisconProtectionPrefUpdateRes() {
        super(1048708, 0, 0);
    }

    public DisconProtectionPrefUpdateRes(long receiverID, long playerID, boolean flag) {
        super(1048708, receiverID, playerID);
        this.flag = flag;
    }




    public boolean isFlag() {
        return flag;
    }

    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DisconProtectionPrefUpdateRes{" +
            "flag=" + flag +
        "}";
    }
}
