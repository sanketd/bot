package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Ready extends AbstractMessage { 
 

    public Ready() {
        super(1048603, 0, 0);
    }

    public Ready(long receiverID, long playerID) {
        super(1048603, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Ready{" +
        "}";
    }
}
