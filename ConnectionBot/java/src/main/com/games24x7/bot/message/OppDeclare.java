package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OppDeclare extends AbstractMessage { 
 
    private int succ;
    private int score;
    private long matchid;
    private long gameid;
    private long mplayerid;

    public OppDeclare() {
        super(1048618, 0, 0);
    }

    public OppDeclare(long receiverID, long playerID, int succ,int score,long matchid,long gameid,long mplayerid) {
        super(1048618, receiverID, playerID);
        this.succ = succ;
        this.score = score;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
    }




    public int getSucc() {
        return succ;
    }

    public void setSucc(int succ) {
        this.succ = succ;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OppDeclare{" +
            "succ=" + succ +
            "score=" + score +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
        "}";
    }
}
