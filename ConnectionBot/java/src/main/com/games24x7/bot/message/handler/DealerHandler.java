package com.games24x7.bot.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.message.Dealer;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class DealerHandler implements BotMessageHandler<Dealer>{
	private static Logger logger=LoggerFactory.getLogger( DealerHandler.class );
	public static int dealerseatid;
	@Override
	public void handleMessage(UserSession session, Dealer message,PlayerInfo plrInfo) 
	{
		
		System.out.println("Inside Dealer message handler");
		//Dealer d = new Dealer(message.getReceiverID(), plrInfo.getPlayerId(), message.getMatchid(),message.getGameid(),message.getMplayerid());
		if(message.getMplayerid()==plrInfo.getMPlayerId())
		{
			dealerseatid=plrInfo.getSeatId();
			//logger.debug("=========||||||Seat id from dealer handler is :-)"+dealerseatid);
		}
		
	}

}
