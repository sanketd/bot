package com.games24x7.bot.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.FMGRequest;
import com.games24x7.bot.message.MoveToTable;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class MoveToTableHandler implements BotMessageHandler< MoveToTable >
{
	private static Logger logger = LoggerFactory.getLogger( MoveToTableHandler.class );

	@Override
	public void handleMessage( UserSession session, MoveToTable message, PlayerInfo plrInfo )
	{
		logger.debug( "---------------Inside Move to table Hanlder------------" );

		long playerId = plrInfo.getPlayerId();
		int gameFormat = 0;
		int players = BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" );
		int gameType = BotImpl.getInstance().getConfiguration().getIntValue( "SETTLEMENT_TYPE" );
		int amt = BotImpl.getInstance().getConfiguration().getIntValue( "FMG_AMOUNT" );
		int deck = BotImpl.getInstance().getConfiguration().getIntValue( "NO_OF_DECK" );
		int pp = BotImpl.getInstance().getConfiguration().getIntValue( "PRIZE_PER" );
		int prizetype=BotImpl.getInstance().getConfiguration().getIntValue( "PRIZE_TYPE" );
		if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MOVE_NOW" ) )
		{
			if( players >= 3 )
			{

				logger.debug( "inisde ifffff.................................." );
				deck = BotImpl.getInstance().getConfiguration().getIntValue( "NO_OF_DECK" );
				System.out.println( "Gametype is" + gameType + "Deck is" + deck );
				if( gameType == 1 && deck == 2 )

					gameFormat = 100;

				else if( gameType == 1 && deck == 3 )

					gameFormat = 103;

			}
			FMGRequest fmgReq = new FMGRequest( 0, playerId, 0l, 0l, prizetype, gameFormat, players, pp, amt, 0, "", 0, false, false, true );
			logger.debug( "--------------------Message for MOVE NOW------------" + fmgReq.toString() );
			SendMsgOnChannel.send( fmgReq, plrInfo.getChannel() );
			logger.debug( "===================MOVE NOW Sent message======================= " );
		}
	}

}
