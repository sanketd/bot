package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TossCard extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private List<TossCardObj> tosscardarry;

    public TossCard() {
        super(1048666, 0, 0);
    }

    public TossCard(long receiverID, long playerID, long matchid,long gameid,List<TossCardObj> tosscardarry) {
        super(1048666, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.tosscardarry = tosscardarry;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public List<TossCardObj> getTosscardarry() {
        return tosscardarry;
    }

    public void setTosscardarry(List<TossCardObj> tosscardarry) {
        this.tosscardarry = tosscardarry;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TossCard{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "tosscardarry=" + tosscardarry +
        "}";
    }
}
