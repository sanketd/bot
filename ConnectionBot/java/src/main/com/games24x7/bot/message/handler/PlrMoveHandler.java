package com.games24x7.bot.message.handler;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.Drop;
import com.games24x7.bot.message.Pickc;
import com.games24x7.bot.message.PlrMove;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class PlrMoveHandler implements BotMessageHandler< PlrMove >
{
	private static Logger logger = LoggerFactory.getLogger( PlrMoveHandler.class );

	public static ArrayList< String > spades = new ArrayList< String >();
	public static ArrayList< String > hearts = new ArrayList< String >();
	public static ArrayList< String > diamonds = new ArrayList< String >();
	public static ArrayList< String > clubs = new ArrayList< String >();
	public static ArrayList< String > jokers = new ArrayList< String >();
	protected static List< List< String > > groupOfCards = new ArrayList< List< String > >();

	boolean alreadyDicon = false;
	int players = BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" );
	boolean playnow = BotImpl.getInstance().getConfiguration().getBooleanValue( "PLAYNOW" );
	int gameType = BotImpl.getInstance().getConfiguration().getIntValue( "SETTLEMENT_TYPE" );
	int poolVariant = BotImpl.getInstance().getConfiguration().getIntValue( "GAME_VARIANT" );
	int amt = BotImpl.getInstance().getConfiguration().getIntValue( "FMG_AMOUNT" );
	int deck = BotImpl.getInstance().getConfiguration().getIntValue( "NO_OF_DECK" );

	@Override
	public void handleMessage( UserSession session, PlrMove message, PlayerInfo playerInfo )
	{
		long receiverId = message.getReceiverID();
		long playerId = playerInfo.getPlayerId();
		long mpid = message.getMplayerid();
		long matchId = message.getMatchid();
		//System.out.println( "PlrMove handler msg " + message );
		if( mpid == playerInfo.getMPlayerId() )
		{
			System.out.println( "Current Player Move " + playerId );

			try
			{
				Thread.sleep( BotImpl.getInstance().getConfiguration().getIntValue( "PLR_MOVE_DELAY_IN_SEC" ) * 1000 );
			}
			catch( InterruptedException e )
			{ // TODO Auto-generated
				// e.printStackTrace();
			}

			System.out.println( BotImpl.getInstance().getConfiguration().getBooleanValue( "VALID_FINISH" ) );
			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "VALID_FINISH" ) )
			{
				

				ArrayList< String > diomondCards = new ArrayList< String >();
				ArrayList< String > spadeCards = new ArrayList< String >();
				ArrayList< String > clubCards = new ArrayList< String >();
				ArrayList< String > heartCards = new ArrayList< String >();
				List< List< String > > cards = sort( playerInfo.getCards() );
				List< String > allcards = new ArrayList< String >();
				System.out.println( "OPen card is:" + playerInfo.getOpenCard() );
				System.out.println( "Cards are......>" + playerInfo.getCards().toString() );
				System.out.println( "Joker card is....... >>>>" + playerInfo.getJkrCard() );
				String removalBrackets = playerInfo.getCards().toString().substring( 2, playerInfo.getCards().toString().length() - 2 );
				System.out.println( "New cards are-----------------------" + removalBrackets );
				String[] splitCards = removalBrackets.split( "," );
				int length = playerInfo.getJkrCard().length();
				String jokerVal = playerInfo.getJkrCard().substring( 0, length - 1 );
				for( String card : splitCards )
				{
					if( card.substring( 0, card.length() - 1 ).equals( jokerVal ) )
					{
						System.out.println( "Joker Cards::::::_----- " + jokerVal );
					}
					if( card.contains( "d" ) )
					{
						diomondCards.add( card.substring( 0, card.length() ) );
						System.out.println( "Daimond cards are:" + diomondCards );

					}
					else if( card.contains( "s" ) )
					{
						spadeCards.add( card.substring( 0, card.length() ) );
						System.out.println( "spades cards are" + spadeCards );
					}
					else if( card.contains( "c" ) )
					{
						clubCards.add( card.substring( 0, card.length() ) );
						System.out.println( "Club cards are" + clubCards );
					}
					else if( card.contains( "h" ) )
					{
						heartCards.add( card.substring( 0, card.length() ) );
						System.out.println( "Hearts cards are" + heartCards );
					}
					else if( card.contains( "j" ) )
					{
						jokers.add( card );
						System.out.println( "Joker cards are" + card );
					}
					cards.add( diomondCards );
					cards.add( spadeCards );
					cards.add( clubCards );
					cards.add( heartCards );

				}

				System.out.printf( "Cards are:" + cards.get( 1 ) + "," + cards.get( 2 ) + "," + cards.get( 3 ) + "," + cards.get( 4 ) );
				String FinalsCards = cards.get( 1 ) + "," + cards.get( 2 ) + "," + cards.get( 3 ) + "," + cards.get( 4 );
				allcards.add( FinalsCards );
				Collections.sort( allcards );
				System.out.println( "Final Cards are" + allcards );
				// TODO make score 0

			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "QUICK_FINISH" ) )
			{
				System.out.println( "Finishing quickly " + BotImpl.getInstance().getConfiguration().getBooleanValue( "QUICK_FINISH" ) );
				if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DISCONNECTION", false ) )
				{
					if( !alreadyDicon )
					{
						playerInfo.getTimer().cancel();
						try
						{
							Thread.sleep( 15000 );
						}
						catch( InterruptedException e )
						{
							// TODO Auto-generated
							// catch block
							e.printStackTrace();
						}

					}
				}
				else
				{
					Message pickclose = new Pickc( receiverId, playerId, mpid, matchId );
					SendMsgOnChannel.send( pickclose, playerInfo.getChannel() );
				}

			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DROP" ) )
			{

				Message drop = new Drop( receiverId, playerId, mpid, matchId, true, false );
				System.out.println( "Seat id" + playerInfo.getSeatId() );
				SendMsgOnChannel.send( drop, playerInfo.getChannel() );

			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MIDDLE_DROP" ) )
			{

				Message drop = new Drop( receiverId, playerId, mpid, matchId, false, true );
				System.out.println( "Seat id" + playerInfo.getSeatId() );
				SendMsgOnChannel.send( drop, playerInfo.getChannel() );

			}

			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "INFINITE_MOVE" ) )
			{

				if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DISCONNECTION" ) )
				{
					if( !alreadyDicon )
					{
						playerInfo.getTimer().cancel();
						try
						{
							Thread.sleep( 15000 );
						}
						catch( InterruptedException e )
						{
							// TODO Auto-generated
							// catch block
							e.printStackTrace();
						}
						// processDisconnection(
						// session, message, playerInfo
						// );
					}
				}
				else
				{
					Message pickclose = new Pickc( receiverId, playerId, mpid, matchId );
					SendMsgOnChannel.send( pickclose, playerInfo.getChannel() );

				}

			}

			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "LIMITED_MOVE" ) )
			{

				Message pickclose = new Pickc( receiverId, playerId, mpid, matchId );
				SendMsgOnChannel.send( pickclose, playerInfo.getChannel() );
				playerInfo.setPlayerMoveCount( playerInfo.getPlayerMoveCount() + 1 );

			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MAKE_SPLIT_2_PLAYERS" ) )

			{
				System.out.println( " Match Player id " + playerInfo.getMPlayerId() + " Player id -- " + playerInfo.getPlayerId() + "Seatid Info:-" + playerInfo.getSeatId() );
				System.out.println(
						"-------------------------*************************************AUTO SPLIT******************************************---------------------------------" );
				int seatId = playerInfo.getSeatId();
				if( seatId == 0 || seatId == 3 )
				{
					System.out.println( "********Seat id of the player in Drop Condition******---------" + playerInfo.getSeatId() );
					Message drop = new Drop( receiverId, playerId, mpid, matchId, true, false );
					SendMsgOnChannel.send( drop, playerInfo.getChannel() );
				}

				else
				{
					System.out.println( "Seat id of the player in wrong show*****************---------" + playerInfo.getSeatId() );
					Message pickclose = new Pickc( receiverId, playerId, mpid, matchId );
					SendMsgOnChannel.send( pickclose, playerInfo.getChannel() );
				}

			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MAKE_SPLIT_3_PLAYERS" ) )
			{

				int seatId = playerInfo.getSeatId();
				if( seatId == 0 || seatId == 1 || seatId == 2 )
				{
					//System.out.println( "score for the player" + playerInfo.getScore() +"Seat id of player"+playerInfo.getSeatId() );
					//System.out.println( "********Seat id of the player in Drop Condition for MAKE_SPLIT_3_PLAYERS ******---------" + playerInfo.getSeatId() );
					Message drop = new Drop( receiverId, playerId, mpid, matchId, true, false );
					SendMsgOnChannel.send( drop, playerInfo.getChannel() );
				}
				else
				{
					//System.out.println( "Seat id of the player in wrong show MAKE_SPLIT_3_PLAYERS*****************---------" + playerInfo.getSeatId() );
					Message pickclose = new Pickc( receiverId, playerId, mpid, matchId );
					SendMsgOnChannel.send( pickclose, playerInfo.getChannel() );
				}
			}
		}

	}

	public List< List< String > > sort( List< List< String > > cards )
	{
		// TODO Sort cards

		return cards;
	}

	/*
	 * public void processDisconnection( UserSession session, PlrMove
	 * message, PlayerInfo playerInfo ) { this.channel =
	 * BotImpl.getInstance().createCSClientChannel( "54.254.183.150", 9443
	 * ); this.channelInfo =
	 * ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( channel ); //
	 * this.msgOnChannel=new SendMsgOnChannel(); this.us = new UserSession(
	 * Long.toString( channelInfo.getChannelID() ),
	 * channelInfo.getSessionKey() ); this.plrInfo = new PlayerInfo(
	 * playerInfo.getPlayerId(), channel ); HandShake msg = new HandShake(
	 * message.getReceiverID(), playerInfo.getPlayerId(), "192.168.48.8" );
	 * us.setAccountID( Thread.currentThread().getId(),
	 * playerInfo.getPlayerId() ); us.setAccountName(
	 * Thread.currentThread().getName() ); System.out.println(
	 * "DisconnectHandler ---->>> " + channel );
	 * 
	 * boolean sendSuccess = SendMsgOnChannel.send( msg, channel ); if(
	 * sendSuccess ) { try { Thread.sleep( 15000 ); System.out.println(
	 * " DisconnectHandler Geting receiver queue" ); receivedMsgQueue =
	 * ChannelIdVsReceiverQueue.get( channelInfo.getChannelID() );
	 * 
	 * if( receivedMsgQueue != null ) { // TODO receive msg // poll queue
	 * for msgs while( true ) { // System.out.println("polling queue "
	 * +receivedMsgQueue); Message message2 = receivedMsgQueue.get(); if(
	 * message2 != null ) { System.out.println(
	 * "DisconnectHandler Message2 " + message2.getClass().toString() ); if(
	 * message2.getClassID() == BotMessageConstants.HANDSHAKERESPONSE ) {
	 * handshakeReceived = true; plrInfo.getTimer().schedule( new
	 * HeartBeatWorkerThread( channel, message2.getReceiverID(),
	 * message2.getPlayerID() ), 0, 2000 ); // reconnection Recon recon =
	 * new Recon( message2.getReceiverID(), plrInfo.getPlayerId(), "", "",
	 * "", 1, plrInfo.getMatchId(), plrInfo.getTableId(),
	 * playerInfo.getMPlayerId() ); SendMsgOnChannel.send( recon, channel );
	 * } else { if( handshakeReceived ) {
	 * 
	 * if( message2.getClassID() == BotMessageConstants.HEARTBEATRESPONSE )
	 * { HeartBeatResponse heartBeatResponse = ( HeartBeatResponse )
	 * message2; // System.out.println("HeartBeatResponse "
	 * +heartBeatResponse .toString()); System.out.println(
	 * "DisconnectHandler HeartBeatResponse " + heartBeatResponse.toString()
	 * ); } // Player // getting // disconnected // after // pick // card
	 * BotMessageHandler messageHandler =
	 * BotImpl.getInstance().getMsgHandler( message2.getClassID() ); try {
	 * if( message2.getClassID() == 1048636 ) { if(
	 * BotImpl.getInstance().getConfiguration().getBooleanValue(
	 * "DISCON_INFINITE_MOVE" ) ) { System.out.println(
	 * "DISCON_INFINITE_MOVE AFTER RECON" ); Message pickclose = new Pickc(
	 * message2.getReceiverID(), plrInfo.getPlayerId(),
	 * plrInfo.getMPlayerId(), plrInfo.getMatchId() );
	 * SendMsgOnChannel.send( pickclose, plrInfo.getChannel() );
	 * 
	 * } else if( BotImpl.getInstance().getConfiguration().getBooleanValue(
	 * "DISCON_QUICK_FINISH" ) ) { System.out.println(
	 * "<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DISCON_QUICK_FINISH AFTER RECON>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>"
	 * ); Message finish = new Finish( message2.getReceiverID(),
	 * message.getPlayerID(), plrInfo.getCards() .get( 0 ).get( 0 ),
	 * message.getMplayerid(), message.getMatchid() );
	 * SendMsgOnChannel.send( finish, playerInfo.getChannel() );
	 * 
	 * }
	 * 
	 * } else { messageHandler.handleMessage( us, message2, plrInfo ); }
	 * 
	 * } catch( NullPointerException e ) { System.out.println(
	 * " DisconnectHandler INsdide null pointer exception -- " + message2 );
	 * System.out.println( " DisconnectHandler us" + us );
	 * System.out.println( " DisconnectHandler plrInfo" + plrInfo );
	 * System.out.println( " DisconnectHandler messageHandler" +
	 * messageHandler ); } } } } } } } catch( Exception e ) {
	 * e.printStackTrace(); } } }
	 */
}
