package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DisconProtectionPref extends AbstractMessage { 
 
    private int pointsrummy;
    private int mtt;
    private int ischecked;
    private int pointsrummy21;
    private int dealsrummy;
    private boolean messageshown;
    private int poolrummy;
    private boolean dppflag;

    public DisconProtectionPref() {
        super(1048733, 0, 0);
    }

    public DisconProtectionPref(long receiverID, long playerID, int pointsrummy,int mtt,int ischecked,int pointsrummy21,int dealsrummy,boolean messageshown,int poolrummy,boolean dppflag) {
        super(1048733, receiverID, playerID);
        this.pointsrummy = pointsrummy;
        this.mtt = mtt;
        this.ischecked = ischecked;
        this.pointsrummy21 = pointsrummy21;
        this.dealsrummy = dealsrummy;
        this.messageshown = messageshown;
        this.poolrummy = poolrummy;
        this.dppflag = dppflag;
    }




    public int getPointsrummy() {
        return pointsrummy;
    }

    public void setPointsrummy(int pointsrummy) {
        this.pointsrummy = pointsrummy;
    }

    public int getMtt() {
        return mtt;
    }

    public void setMtt(int mtt) {
        this.mtt = mtt;
    }

    public int getIschecked() {
        return ischecked;
    }

    public void setIschecked(int ischecked) {
        this.ischecked = ischecked;
    }

    public int getPointsrummy21() {
        return pointsrummy21;
    }

    public void setPointsrummy21(int pointsrummy21) {
        this.pointsrummy21 = pointsrummy21;
    }

    public int getDealsrummy() {
        return dealsrummy;
    }

    public void setDealsrummy(int dealsrummy) {
        this.dealsrummy = dealsrummy;
    }

    public boolean isMessageshown() {
        return messageshown;
    }

    public void setMessageshown(boolean messageshown) {
        this.messageshown = messageshown;
    }

    public int getPoolrummy() {
        return poolrummy;
    }

    public void setPoolrummy(int poolrummy) {
        this.poolrummy = poolrummy;
    }

    public boolean isDppflag() {
        return dppflag;
    }

    public void setDppflag(boolean dppflag) {
        this.dppflag = dppflag;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DisconProtectionPref{" +
            "pointsrummy=" + pointsrummy +
            "mtt=" + mtt +
            "ischecked=" + ischecked +
            "pointsrummy21=" + pointsrummy21 +
            "dealsrummy=" + dealsrummy +
            "messageshown=" + messageshown +
            "poolrummy=" + poolrummy +
            "dppflag=" + dppflag +
        "}";
    }
}
