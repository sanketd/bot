package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class SetupRes extends AbstractMessage { 
 
    private boolean breplayer;
    private int mp;
    private long tableid;
    private String swfmainpath;
    private String bgimgurl;
    private long aaid;
    private boolean chatblocked;
    private String conifgfilepath;
    private int maxplayer;
    private boolean showfmg;
    private boolean mttChatBlock;

    public SetupRes() {
        super(1048653, 0, 0);
    }

    public SetupRes(long receiverID, long playerID, boolean breplayer,int mp,long tableid,String swfmainpath,String bgimgurl,long aaid,boolean chatblocked,String conifgfilepath,int maxplayer,boolean showfmg,boolean mttChatBlock) {
        super(1048653, receiverID, playerID);
        this.breplayer = breplayer;
        this.mp = mp;
        this.tableid = tableid;
        this.swfmainpath = swfmainpath;
        this.bgimgurl = bgimgurl;
        this.aaid = aaid;
        this.chatblocked = chatblocked;
        this.conifgfilepath = conifgfilepath;
        this.maxplayer = maxplayer;
        this.showfmg = showfmg;
        this.mttChatBlock = mttChatBlock;
    }




    public boolean isBreplayer() {
        return breplayer;
    }

    public void setBreplayer(boolean breplayer) {
        this.breplayer = breplayer;
    }

    public int getMp() {
        return mp;
    }

    public void setMp(int mp) {
        this.mp = mp;
    }

    public long getTableid() {
        return tableid;
    }

    public void setTableid(long tableid) {
        this.tableid = tableid;
    }

    public String getSwfmainpath() {
        return swfmainpath;
    }

    public void setSwfmainpath(String swfmainpath) {
        this.swfmainpath = swfmainpath;
    }

    public String getBgimgurl() {
        return bgimgurl;
    }

    public void setBgimgurl(String bgimgurl) {
        this.bgimgurl = bgimgurl;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public boolean isChatblocked() {
        return chatblocked;
    }

    public void setChatblocked(boolean chatblocked) {
        this.chatblocked = chatblocked;
    }

    public String getConifgfilepath() {
        return conifgfilepath;
    }

    public void setConifgfilepath(String conifgfilepath) {
        this.conifgfilepath = conifgfilepath;
    }

    public int getMaxplayer() {
        return maxplayer;
    }

    public void setMaxplayer(int maxplayer) {
        this.maxplayer = maxplayer;
    }

    public boolean isShowfmg() {
        return showfmg;
    }

    public void setShowfmg(boolean showfmg) {
        this.showfmg = showfmg;
    }

    public boolean isMttChatBlock() {
        return mttChatBlock;
    }

    public void setMttChatBlock(boolean mttChatBlock) {
        this.mttChatBlock = mttChatBlock;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SetupRes{" +
            "breplayer=" + breplayer +
            "mp=" + mp +
            "tableid=" + tableid +
            "swfmainpath=" + swfmainpath +
            "bgimgurl=" + bgimgurl +
            "aaid=" + aaid +
            "chatblocked=" + chatblocked +
            "conifgfilepath=" + conifgfilepath +
            "maxplayer=" + maxplayer +
            "showfmg=" + showfmg +
            "mttChatBlock=" + mttChatBlock +
        "}";
    }
}
