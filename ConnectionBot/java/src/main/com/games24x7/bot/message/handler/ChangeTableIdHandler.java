package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.ChangeTableId;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class ChangeTableIdHandler implements BotMessageHandler< ChangeTableId >
{

	@Override
	public void handleMessage( UserSession session, ChangeTableId message, PlayerInfo playerInfo )
	{
		System.out.println("########*******Change TableId Handler .........."+message.toString());
		System.out.println("########*******Old table id - "+ playerInfo.getTableId());
		playerInfo.setTableId( message.getNewtableid() );
		System.out.println("########*******New table id - "+ playerInfo.getTableId());
	}

}
