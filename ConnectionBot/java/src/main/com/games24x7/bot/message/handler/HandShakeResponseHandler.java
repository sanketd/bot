package com.games24x7.bot.message.handler;

import java.util.concurrent.TimeUnit;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.FMGRequest;
import com.games24x7.bot.message.HandShakeResponse;
import com.games24x7.bot.message.Setup;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.HeartBeatWorkerThread;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class HandShakeResponseHandler implements BotMessageHandler< HandShakeResponse >
{

	@Override
	public void handleMessage( UserSession session, HandShakeResponse message, PlayerInfo playerInfo )
	{
		System.out.println( "Handshake Response" + message);
		long receiverId = message.getReceiverID();
		long playerId = message.getPlayerID();
		playerInfo.setHandshakeReceived( true );
		//System.out.println( "HandShake Response" + message.toString() );
		playerInfo.setHeartBeatTask( BotImpl.getInstance().getScheduledThreadPool().scheduleWithFixedDelay(new HeartBeatWorkerThread( playerInfo.getChannel(), receiverId, playerId ) , BotImpl.getInstance().getConfiguration().getIntValue( "HEARTBEAT_TIME_INTERVAL", 2000 ), BotImpl.getInstance().getConfiguration().getIntValue( "HEARTBEAT_TIME_INTERVAL", 2000 ), TimeUnit.MILLISECONDS ));

		if( BotImpl.getInstance().getConfiguration().getBooleanValue( "FMG_FLAG" ) )
		{
			int gameType = BotImpl.getInstance().getConfiguration().getIntValue( "SETTLEMENT_TYPE" );
			int poolVariant = BotImpl.getInstance().getConfiguration().getIntValue( "GAME_VARIANT" );
			int deck = BotImpl.getInstance().getConfiguration().getIntValue( "NO_OF_DECK" );
			int noOfPlayer = BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" );
			double pointvalue = 0d;

			int gameFormat = 0;
			if( gameType == 1 )
			{
				pointvalue = BotImpl.getInstance().getConfiguration().getDoubleValue( "PRIZE_PER" );
			}
			// =
			// 302;
		//	System.out.println( "noOfPlayer:" + BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" ) + ",gameType:" + gameType + ",poolVariant:" + poolVariant );
			if( noOfPlayer == 6 )
			{
				if( gameType == 5 && deck == 3 )
					gameFormat = 500;

				else if( gameType == 3 )
					gameFormat = 306;

				else if( gameType == 2 && poolVariant == 101 )
					gameFormat = 201;

				else if( gameType == 2 && poolVariant == 201 )
					gameFormat = 202;

				else if( gameType == 1 & deck == 3 )
					gameFormat = 103;
				else
				{
					gameFormat = 100;
				}

			}
			else if( noOfPlayer == 2 )
			{
				if( gameType == 5 && deck == 3 )
					gameFormat = 500;

				else if( gameType == 3 )
					gameFormat = 302;

				else if( gameType == 2 && poolVariant == 101 )
					gameFormat = 201;

				else if( gameType == 2 && poolVariant == 201 )
					gameFormat = 201;

				else if( gameType == 1 & deck == 3 )
					gameFormat = 103;
				else
				{
					gameFormat = 100;
				}

			}
		//	System.out.println( "Game format is" + gameFormat );
			FMGRequest fmgRequest = new FMGRequest( 0, playerId, 0l, 0l, 2, gameFormat, noOfPlayer, pointvalue, BotImpl.getInstance().getConfiguration().getDoubleValue( "FMG_AMOUNT" ), 1, "", 0, false, true, false );
			// fmgRequest.setSessionId(
			// channelInfo.getSessionKey()
			// );
			fmgRequest.setDest( "TE" );
		//	System.out.println( "Sending FMGRequest Message " + fmgRequest );
			SendMsgOnChannel.send( fmgRequest, playerInfo.getChannel() );
			System.out.println( "Playnow Sent message " );

		}
		else
		{
			byte channel_id = 1;
			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DESKTOP_TO_MOBILE" ) )
			{
				channel_id = 2;
			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DESKTOP_TO_APK" ) )
			{
				channel_id = 3;
			}

			// normal
			// flow
			// without
			// FMG
			Setup setup = new Setup( receiverId, playerId, session.getSessionKey(), null, channel_id );
			setup.setSource( String.valueOf( playerId ) );
		//	System.out.println( "Sending Setup msg " + setup.toString() );
			SendMsgOnChannel.send( setup, playerInfo.getChannel() );
		}
	}
}
