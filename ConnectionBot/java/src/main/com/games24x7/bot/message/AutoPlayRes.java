package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoPlayRes extends AbstractMessage { 
 
    private long mid;
    private long mpid;
    private boolean isautoplay;

    public AutoPlayRes() {
        super(1048580, 0, 0);
    }

    public AutoPlayRes(long receiverID, long playerID, long mid,long mpid,boolean isautoplay) {
        super(1048580, receiverID, playerID);
        this.mid = mid;
        this.mpid = mpid;
        this.isautoplay = isautoplay;
    }




    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    public boolean isIsautoplay() {
        return isautoplay;
    }

    public void setIsautoplay(boolean isautoplay) {
        this.isautoplay = isautoplay;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoPlayRes{" +
            "mid=" + mid +
            "mpid=" + mpid +
            "isautoplay=" + isautoplay +
        "}";
    }
}
