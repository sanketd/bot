package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PFinishResp extends AbstractMessage { 
 
    private long timetodeclare;
    private long totaltimetodeclare;
    private long extratime;
    private long matchid;
    private long gameid;
    private long mplayerid;
    private String cardid;

    public PFinishResp() {
        super(1048624, 0, 0);
    }

    public PFinishResp(long receiverID, long playerID, long timetodeclare,long totaltimetodeclare,long extratime,long matchid,long gameid,long mplayerid,String cardid) {
        super(1048624, receiverID, playerID);
        this.timetodeclare = timetodeclare;
        this.totaltimetodeclare = totaltimetodeclare;
        this.extratime = extratime;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.cardid = cardid;
    }




    public long getTimetodeclare() {
        return timetodeclare;
    }

    public void setTimetodeclare(long timetodeclare) {
        this.timetodeclare = timetodeclare;
    }

    public long getTotaltimetodeclare() {
        return totaltimetodeclare;
    }

    public void setTotaltimetodeclare(long totaltimetodeclare) {
        this.totaltimetodeclare = totaltimetodeclare;
    }

    public long getExtratime() {
        return extratime;
    }

    public void setExtratime(long extratime) {
        this.extratime = extratime;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PFinishResp{" +
            "timetodeclare=" + timetodeclare +
            "totaltimetodeclare=" + totaltimetodeclare +
            "extratime=" + extratime +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "cardid=" + cardid +
        "}";
    }
}
