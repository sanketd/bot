package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class LPInfo extends AbstractMessage { 
 
    private int rptospend;
    private int lptospend;
    private List<ClubInfoArrayObj> clubinfoarray;

    public LPInfo() {
        super(1048722, 0, 0);
    }

    public LPInfo(long receiverID, long playerID, int rptospend,int lptospend,List<ClubInfoArrayObj> clubinfoarray) {
        super(1048722, receiverID, playerID);
        this.rptospend = rptospend;
        this.lptospend = lptospend;
        this.clubinfoarray = clubinfoarray;
    }




    public int getRptospend() {
        return rptospend;
    }

    public void setRptospend(int rptospend) {
        this.rptospend = rptospend;
    }

    public int getLptospend() {
        return lptospend;
    }

    public void setLptospend(int lptospend) {
        this.lptospend = lptospend;
    }

    public List<ClubInfoArrayObj> getClubinfoarray() {
        return clubinfoarray;
    }

    public void setClubinfoarray(List<ClubInfoArrayObj> clubinfoarray) {
        this.clubinfoarray = clubinfoarray;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"LPInfo{" +
            "rptospend=" + rptospend +
            "lptospend=" + lptospend +
            "clubinfoarray=" + clubinfoarray +
        "}";
    }
}
