package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class StartGameTimer extends AbstractMessage { 
 
    private long timer;
    private long aaid;
    private List<String> rejoiningplayerarray;
    private String sessionid;

    public StartGameTimer() {
        super(1048660, 0, 0);
    }

    public StartGameTimer(long receiverID, long playerID, long timer,long aaid,List<String> rejoiningplayerarray,String sessionid) {
        super(1048660, receiverID, playerID);
        this.timer = timer;
        this.aaid = aaid;
        this.rejoiningplayerarray = rejoiningplayerarray;
        this.sessionid = sessionid;
    }




    public long getTimer() {
        return timer;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<String> getRejoiningplayerarray() {
        return rejoiningplayerarray;
    }

    public void setRejoiningplayerarray(List<String> rejoiningplayerarray) {
        this.rejoiningplayerarray = rejoiningplayerarray;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"StartGameTimer{" +
            "timer=" + timer +
            "aaid=" + aaid +
            "rejoiningplayerarray=" + rejoiningplayerarray +
            "sessionid=" + sessionid +
        "}";
    }
}
