package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class InvalidFinish21Card extends AbstractMessage { 
 
    private int dbltodclr;
    private int tnlcnt;
    private int tnltodclr;
    private boolean jkrsflag;
    private int pscount;
    private int dblcnt;
    private int jkrcnt;
    private int psreq;
    private long matchid;
    private List<GrpsInfoObj> grpsinfo;
    private long gameid;
    private int jkrtodclr;
    private boolean allvalidsets;

    public InvalidFinish21Card() {
        super(1048729, 0, 0);
    }

    public InvalidFinish21Card(long receiverID, long playerID, int dbltodclr,int tnlcnt,int tnltodclr,boolean jkrsflag,int pscount,int dblcnt,int jkrcnt,int psreq,long matchid,List<GrpsInfoObj> grpsinfo,long gameid,int jkrtodclr,boolean allvalidsets) {
        super(1048729, receiverID, playerID);
        this.dbltodclr = dbltodclr;
        this.tnlcnt = tnlcnt;
        this.tnltodclr = tnltodclr;
        this.jkrsflag = jkrsflag;
        this.pscount = pscount;
        this.dblcnt = dblcnt;
        this.jkrcnt = jkrcnt;
        this.psreq = psreq;
        this.matchid = matchid;
        this.grpsinfo = grpsinfo;
        this.gameid = gameid;
        this.jkrtodclr = jkrtodclr;
        this.allvalidsets = allvalidsets;
    }




    public int getDbltodclr() {
        return dbltodclr;
    }

    public void setDbltodclr(int dbltodclr) {
        this.dbltodclr = dbltodclr;
    }

    public int getTnlcnt() {
        return tnlcnt;
    }

    public void setTnlcnt(int tnlcnt) {
        this.tnlcnt = tnlcnt;
    }

    public int getTnltodclr() {
        return tnltodclr;
    }

    public void setTnltodclr(int tnltodclr) {
        this.tnltodclr = tnltodclr;
    }

    public boolean isJkrsflag() {
        return jkrsflag;
    }

    public void setJkrsflag(boolean jkrsflag) {
        this.jkrsflag = jkrsflag;
    }

    public int getPscount() {
        return pscount;
    }

    public void setPscount(int pscount) {
        this.pscount = pscount;
    }

    public int getDblcnt() {
        return dblcnt;
    }

    public void setDblcnt(int dblcnt) {
        this.dblcnt = dblcnt;
    }

    public int getJkrcnt() {
        return jkrcnt;
    }

    public void setJkrcnt(int jkrcnt) {
        this.jkrcnt = jkrcnt;
    }

    public int getPsreq() {
        return psreq;
    }

    public void setPsreq(int psreq) {
        this.psreq = psreq;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public List<GrpsInfoObj> getGrpsinfo() {
        return grpsinfo;
    }

    public void setGrpsinfo(List<GrpsInfoObj> grpsinfo) {
        this.grpsinfo = grpsinfo;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getJkrtodclr() {
        return jkrtodclr;
    }

    public void setJkrtodclr(int jkrtodclr) {
        this.jkrtodclr = jkrtodclr;
    }

    public boolean isAllvalidsets() {
        return allvalidsets;
    }

    public void setAllvalidsets(boolean allvalidsets) {
        this.allvalidsets = allvalidsets;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"InvalidFinish21Card{" +
            "dbltodclr=" + dbltodclr +
            "tnlcnt=" + tnlcnt +
            "tnltodclr=" + tnltodclr +
            "jkrsflag=" + jkrsflag +
            "pscount=" + pscount +
            "dblcnt=" + dblcnt +
            "jkrcnt=" + jkrcnt +
            "psreq=" + psreq +
            "matchid=" + matchid +
            "grpsinfo=" + grpsinfo +
            "gameid=" + gameid +
            "jkrtodclr=" + jkrtodclr +
            "allvalidsets=" + allvalidsets +
        "}";
    }
}
