package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class Drop extends AbstractMessage
{

	private long mpid;
	private long mid;
	private boolean autodrop;
	private boolean mdrop;

	public Drop()
	{
		super( 1048590, 0, 0 );
	}

	public Drop( long receiverID, long playerID, long mpid, long mid, boolean autodrop, boolean mdrop )
	{
		super( 1048590, receiverID, playerID );
		this.mpid = mpid;
		this.mid = mid;
		this.autodrop = autodrop;
		this.mdrop = mdrop;
	}

	public long getMpid()
	{
		return mpid;
	}

	public void setMpid( long mpid )
	{
		this.mpid = mpid;
	}

	public long getMid()
	{
		return mid;
	}

	public void setMid( long mid )
	{
		this.mid = mid;
	}

	public boolean isAutodrop()
	{
		return autodrop;
	}

	public void setAutodrop( boolean autodrop )
	{
		this.autodrop = autodrop;
	}

	public boolean isMdrop()
	{
		return mdrop;
	}

	public void setMdrop( boolean mdrop )
	{
		this.mdrop = mdrop;
	}

	@Override
	public String toString()
	{
		return "Super :" + super.toString() + " Child " + "Drop{" + "mpid=" + mpid + "mid=" + mid + "autodrop=" + autodrop + "mdrop=" + mdrop + "}";
	}
}
