package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ScoreListResp extends ChildAbstractMessage{
 
    private List<String> scores;
    private String loginid;
    private int total;

    public ScoreListResp() {
    super(1048647);
    }

    public ScoreListResp( List<String> scores,String loginid,int total) {
         super(1048647);
        this.scores = scores;
        this.loginid = loginid;
        this.total = total;
    }




    public List<String> getScores() {
        return scores;
    }

    public void setScores(List<String> scores) {
        this.scores = scores;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreListResp{" +
            "scores=" + scores +
            "loginid=" + loginid +
            "total=" + total +
        "}";
    }
}
