package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class FMGResponse extends AbstractMessage
{

	private long aaid;
	private long tournamentId;
	private int maxplayer;
	private String errorMessage;
	private int status;
	private boolean fmgOnTable;

	public FMGResponse()
	{
		super( 2097162, 0, 0 );
	}

	public FMGResponse( long receiverID, long playerID, String errorMessage )
	{
		super( 2097162, receiverID, playerID );
		this.aaid =receiverID;
		this.errorMessage=errorMessage;
	}

	public FMGResponse( long receiverID, long playerID, long aaid, long tournamentId, int maxplayer, String errorMessage, int status, boolean fmgOnTable )
	{
		super( 2097162, receiverID, playerID );
		this.aaid = aaid;
		this.tournamentId = tournamentId;
		this.maxplayer = maxplayer;
		this.errorMessage = errorMessage;
		this.status = status;
		this.fmgOnTable = fmgOnTable;
	}

	public long getAaid()
	{
		return aaid;
	}

	public void setAaid( long aaid )
	{
		this.aaid = aaid;
	}

	public long getTournamentId()
	{
		return tournamentId;
	}

	public void setTournamentId( long tournamentId )
	{
		this.tournamentId = tournamentId;
	}

	public int getMaxplayer()
	{
		return maxplayer;
	}

	public void setMaxplayer( int maxplayer )
	{
		this.maxplayer = maxplayer;
	}

	public String getErrorMessage()
	{
		return errorMessage;
	}

	public void setErrorMessage( String errorMessage )
	{
		this.errorMessage = errorMessage;
	}

	public int getStatus()
	{
		return status;
	}

	public void setStatus( int status )
	{
		this.status = status;
	}

	public boolean isFmgOnTable()
	{
		return fmgOnTable;
	}

	public void setFmgOnTable( boolean fmgOnTable )
	{
		this.fmgOnTable = fmgOnTable;
	}

	@Override
	public String toString()
	{
		return "Super :" + super.toString() + " Child " + "FMGResponse{" + "aaid=" + aaid + "tournamentId=" + tournamentId + "maxplayer=" + maxplayer + "errorMessage=" + errorMessage
				+ "status=" + status + "fmgOnTable=" + fmgOnTable + "}";
	}
}
