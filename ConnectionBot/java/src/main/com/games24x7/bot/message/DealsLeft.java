package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DealsLeft extends AbstractMessage { 
 

    public DealsLeft() {
        super(1048584, 0, 0);
    }

    public DealsLeft(long receiverID, long playerID) {
        super(1048584, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DealsLeft{" +
        "}";
    }
}
