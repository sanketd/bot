package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MatchSeatArrayObj extends AbstractMessage { 
 
    private String result;
    private long matchplayerid;
    private int disconprefence;
    private PlayerObj player;
    private int netvaluecardgains;
    private int score;
    private boolean recon;
    private long matchid;
    private int seatid;
    private double amountown;
    private String sessionid;
    private int totalgains;
    private boolean autodrop;
    private boolean firstpracticegame;
    private int consmissmovecount;
    private int totalscore;
    private int handcardgain;

    public MatchSeatArrayObj() {
        super(1048727, 0, 0);
    }

    public MatchSeatArrayObj(long receiverID, long playerID, String result,long matchplayerid,int disconprefence,PlayerObj player,int netvaluecardgains,int score,boolean recon,long matchid,int seatid,double amountown,String sessionid,int totalgains,boolean autodrop,boolean firstpracticegame,int consmissmovecount,int totalscore,int handcardgain) {
        super(1048727, receiverID, playerID);
        this.result = result;
        this.matchplayerid = matchplayerid;
        this.disconprefence = disconprefence;
        this.player = player;
        this.netvaluecardgains = netvaluecardgains;
        this.score = score;
        this.recon = recon;
        this.matchid = matchid;
        this.seatid = seatid;
        this.amountown = amountown;
        this.sessionid = sessionid;
        this.totalgains = totalgains;
        this.autodrop = autodrop;
        this.firstpracticegame = firstpracticegame;
        this.consmissmovecount = consmissmovecount;
        this.totalscore = totalscore;
        this.handcardgain = handcardgain;
    }




    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public long getMatchplayerid() {
        return matchplayerid;
    }

    public void setMatchplayerid(long matchplayerid) {
        this.matchplayerid = matchplayerid;
    }

    public int getDisconprefence() {
        return disconprefence;
    }

    public void setDisconprefence(int disconprefence) {
        this.disconprefence = disconprefence;
    }

    public PlayerObj getPlayer() {
        return player;
    }

    public void setPlayer(PlayerObj player) {
        this.player = player;
    }

    public int getNetvaluecardgains() {
        return netvaluecardgains;
    }

    public void setNetvaluecardgains(int netvaluecardgains) {
        this.netvaluecardgains = netvaluecardgains;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isRecon() {
        return recon;
    }

    public void setRecon(boolean recon) {
        this.recon = recon;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public int getSeatid() {
        return seatid;
    }

    public void setSeatid(int seatid) {
        this.seatid = seatid;
    }

    public double getAmountown() {
        return amountown;
    }

    public void setAmountown(double amountown) {
        this.amountown = amountown;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getTotalgains() {
        return totalgains;
    }

    public void setTotalgains(int totalgains) {
        this.totalgains = totalgains;
    }

    public boolean isAutodrop() {
        return autodrop;
    }

    public void setAutodrop(boolean autodrop) {
        this.autodrop = autodrop;
    }

    public boolean isFirstpracticegame() {
        return firstpracticegame;
    }

    public void setFirstpracticegame(boolean firstpracticegame) {
        this.firstpracticegame = firstpracticegame;
    }

    public int getConsmissmovecount() {
        return consmissmovecount;
    }

    public void setConsmissmovecount(int consmissmovecount) {
        this.consmissmovecount = consmissmovecount;
    }

    public int getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(int totalscore) {
        this.totalscore = totalscore;
    }

    public int getHandcardgain() {
        return handcardgain;
    }

    public void setHandcardgain(int handcardgain) {
        this.handcardgain = handcardgain;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MatchSeatArrayObj{" +
            "result=" + result +
            "matchplayerid=" + matchplayerid +
            "disconprefence=" + disconprefence +
            "player=" + player +
            "netvaluecardgains=" + netvaluecardgains +
            "score=" + score +
            "recon=" + recon +
            "matchid=" + matchid +
            "seatid=" + seatid +
            "amountown=" + amountown +
            "sessionid=" + sessionid +
            "totalgains=" + totalgains +
            "autodrop=" + autodrop +
            "firstpracticegame=" + firstpracticegame +
            "consmissmovecount=" + consmissmovecount +
            "totalscore=" + totalscore +
            "handcardgain=" + handcardgain +
        "}";
    }
}
