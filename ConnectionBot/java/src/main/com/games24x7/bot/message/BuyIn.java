package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BuyIn extends AbstractMessage { 
 
    private double customamnt;
    private double bal;
    private double tablemin;
    private int seatid;
    private double tablemax;
    private int clientvariant;

    public BuyIn() {
        super(1048582, 0, 0);
    }

    public BuyIn(long receiverID, long playerID, double customamnt,double bal,double tablemin,int seatid,double tablemax,int clientvariant) {
        super(1048582, receiverID, playerID);
        this.customamnt = customamnt;
        this.bal = bal;
        this.tablemin = tablemin;
        this.seatid = seatid;
        this.tablemax = tablemax;
        this.clientvariant = clientvariant;
    }




    public double getCustomamnt() {
        return customamnt;
    }

    public void setCustomamnt(double customamnt) {
        this.customamnt = customamnt;
    }

    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    public double getTablemin() {
        return tablemin;
    }

    public void setTablemin(double tablemin) {
        this.tablemin = tablemin;
    }

    public int getSeatid() {
        return seatid;
    }

    public void setSeatid(int seatid) {
        this.seatid = seatid;
    }

    public double getTablemax() {
        return tablemax;
    }

    public void setTablemax(double tablemax) {
        this.tablemax = tablemax;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BuyIn{" +
            "customamnt=" + customamnt +
            "bal=" + bal +
            "tablemin=" + tablemin +
            "seatid=" + seatid +
            "tablemax=" + tablemax +
            "clientvariant=" + clientvariant +
        "}";
    }
}
