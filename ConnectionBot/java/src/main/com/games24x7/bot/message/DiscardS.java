package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DiscardS extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private List<String> cards;

    public DiscardS() {
        super(1048707, 0, 0);
    }

    public DiscardS(long receiverID, long playerID, long matchid,long gameid,List<String> cards) {
        super(1048707, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.cards = cards;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public List<String> getCards() {
        return cards;
    }

    public void setCards(List<String> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DiscardS{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "cards=" + cards +
        "}";
    }
}
