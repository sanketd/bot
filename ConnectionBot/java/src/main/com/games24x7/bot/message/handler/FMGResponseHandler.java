package com.games24x7.bot.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.FMGResponse;
import com.games24x7.bot.message.HandShake;
import com.games24x7.bot.message.Setup;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.WorkerBot;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.PlayerInfoWithChannelId;
import com.games24x7.util.SendMsgOnChannel;

public class FMGResponseHandler implements BotMessageHandler< FMGResponse >
{
	Logger logger = LoggerFactory.getLogger( FMGResponseHandler.class );

	@Override
	public void handleMessage( UserSession session, FMGResponse message, PlayerInfo playerInfo )
	{
		// TODO Auto-generated method stub
		try
		{
			logger.debug( "From FMGResponse handler -->> " + message.getAaid() + " foR PLAYER id -- " + playerInfo.getPlayerId() + "Channel Info:-" + playerInfo.getChannel()
					+ "Error Message" + message.getErrorMessage() );
			//System.out.println( "FMG RESPONSE RECIEVED" + message.getAaid() + "Error Message" + message.getErrorMessage() );
			int channel = BotImpl.getInstance().getConfiguration().getIntValue( "CHANNEL_ID" );
			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MOBILE_TO_DESKTOP" ) || BotImpl.getInstance().getConfiguration().getBooleanValue( "APK_TO_DESKTOP" ) )
			{
				channel = 1;
			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "APK_TO_MOBILE" ) || BotImpl.getInstance().getConfiguration().getBooleanValue( "DESKTOP_TO_MOBILE" ) )
			{
				channel = 2;
			}
			else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MOBILE_TO_APK" ) || BotImpl.getInstance().getConfiguration().getBooleanValue( "DESKTOP_TO_APK" ) )
			{
				channel = 3;
			}
			//System.out.println( "Channel Id from setup request--->>> " + channel );
			Setup setup = new Setup( message.getAaid(), playerInfo.getPlayerId(), session.getSessionKey(), null, channel );
			System.out.println( "set up message " + setup );
			System.out.println( setup.toString() );
			setup.setSource( String.valueOf( playerInfo.getPlayerId() ) );
			setup.setDest( "TE" );
			SendMsgOnChannel.send( setup, playerInfo.getChannel() );
		}
		catch( Exception e )
		{
			e.printStackTrace();
		}

	}

}
