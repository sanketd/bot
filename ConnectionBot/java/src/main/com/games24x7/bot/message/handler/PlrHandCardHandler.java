/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.PlrHandCard;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class PlrHandCardHandler implements BotMessageHandler< PlrHandCard >
{
	@Override
	public void handleMessage( UserSession session, PlrHandCard message, PlayerInfo playerInfo )
	{
		playerInfo.setCards( message.getCards() );
		System.out.println("Player Hand Cards "+message.getCards().toString() );
	}

}
