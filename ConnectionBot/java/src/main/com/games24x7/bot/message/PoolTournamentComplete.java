package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PoolTournamentComplete extends AbstractMessage { 
 
    private long aaid;
    private List<String> winners;
    private long matchid;
    private long tournamentId;

    public PoolTournamentComplete() {
        super(1048749, 0, 0);
    }

    public PoolTournamentComplete(long receiverID, long playerID, long aaid,List<String> winners,long matchid,long tournamentId) {
        super(1048749, receiverID, playerID);
        this.aaid = aaid;
        this.winners = winners;
        this.matchid = matchid;
        this.tournamentId = tournamentId;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<String> getWinners() {
        return winners;
    }

    public void setWinners(List<String> winners) {
        this.winners = winners;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(long tournamentId) {
        this.tournamentId = tournamentId;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PoolTournamentComplete{" +
            "aaid=" + aaid +
            "winners=" + winners +
            "matchid=" + matchid +
            "tournamentId=" + tournamentId +
        "}";
    }
}
