package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Hud extends AbstractMessage { 
 
    private int nextclub;
    private int currentloyaltypoints;
    private int currentrewardpoints;
    private int viployaltypoints;
    private String clubvalidity;
    private boolean isfirstcashgame;
    private int currentclub;
    private boolean isvipuser;
    private String superuserimageurl;
    private RmValue rmvalue;
    private BonusValueObj bonusvalue;

    public Hud() {
        super(1048719, 0, 0);
    }

    public Hud(long receiverID, long playerID, int nextclub,int currentloyaltypoints,int currentrewardpoints,int viployaltypoints,String clubvalidity,boolean isfirstcashgame,int currentclub,boolean isvipuser,String superuserimageurl,RmValue rmvalue,BonusValueObj bonusvalue) {
        super(1048719, receiverID, playerID);
        this.nextclub = nextclub;
        this.currentloyaltypoints = currentloyaltypoints;
        this.currentrewardpoints = currentrewardpoints;
        this.viployaltypoints = viployaltypoints;
        this.clubvalidity = clubvalidity;
        this.isfirstcashgame = isfirstcashgame;
        this.currentclub = currentclub;
        this.isvipuser = isvipuser;
        this.superuserimageurl = superuserimageurl;
        this.rmvalue = rmvalue;
        this.bonusvalue = bonusvalue;
    }




    public int getNextclub() {
        return nextclub;
    }

    public void setNextclub(int nextclub) {
        this.nextclub = nextclub;
    }

    public int getCurrentloyaltypoints() {
        return currentloyaltypoints;
    }

    public void setCurrentloyaltypoints(int currentloyaltypoints) {
        this.currentloyaltypoints = currentloyaltypoints;
    }

    public int getCurrentrewardpoints() {
        return currentrewardpoints;
    }

    public void setCurrentrewardpoints(int currentrewardpoints) {
        this.currentrewardpoints = currentrewardpoints;
    }

    public int getViployaltypoints() {
        return viployaltypoints;
    }

    public void setViployaltypoints(int viployaltypoints) {
        this.viployaltypoints = viployaltypoints;
    }

    public String getClubvalidity() {
        return clubvalidity;
    }

    public void setClubvalidity(String clubvalidity) {
        this.clubvalidity = clubvalidity;
    }

    public boolean isIsfirstcashgame() {
        return isfirstcashgame;
    }

    public void setIsfirstcashgame(boolean isfirstcashgame) {
        this.isfirstcashgame = isfirstcashgame;
    }

    public int getCurrentclub() {
        return currentclub;
    }

    public void setCurrentclub(int currentclub) {
        this.currentclub = currentclub;
    }

    public boolean isIsvipuser() {
        return isvipuser;
    }

    public void setIsvipuser(boolean isvipuser) {
        this.isvipuser = isvipuser;
    }

    public String getSuperuserimageurl() {
        return superuserimageurl;
    }

    public void setSuperuserimageurl(String superuserimageurl) {
        this.superuserimageurl = superuserimageurl;
    }

    public RmValue getRmvalue() {
        return rmvalue;
    }

    public void setRmvalue(RmValue rmvalue) {
        this.rmvalue = rmvalue;
    }

    public BonusValueObj getBonusvalue() {
        return bonusvalue;
    }

    public void setBonusvalue(BonusValueObj bonusvalue) {
        this.bonusvalue = bonusvalue;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Hud{" +
            "nextclub=" + nextclub +
            "currentloyaltypoints=" + currentloyaltypoints +
            "currentrewardpoints=" + currentrewardpoints +
            "viployaltypoints=" + viployaltypoints +
            "clubvalidity=" + clubvalidity +
            "isfirstcashgame=" + isfirstcashgame +
            "currentclub=" + currentclub +
            "isvipuser=" + isvipuser +
            "superuserimageurl=" + superuserimageurl +
            "rmvalue=" + rmvalue +
            "bonusvalue=" + bonusvalue +
        "}";
    }
}
