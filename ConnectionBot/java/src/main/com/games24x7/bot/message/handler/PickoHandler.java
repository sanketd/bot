package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.Finish;
import com.games24x7.bot.message.PickORes;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class PickoHandler implements BotMessageHandler< PickORes >
{

	@Override
	public void handleMessage( UserSession session, PickORes message, PlayerInfo playerInfo )
	{
		long receiverId = message.getReceiverID();
		long playerId = playerInfo.getPlayerId();
		long matchId = message.getMatchid();
		if(playerInfo.getScore()==0)
		{
			Message finish = new Finish( receiverId, playerId, "cardid", playerInfo.getMPlayerId(), matchId);
			SendMsgOnChannel.send( finish, playerInfo.getChannel() );
		}
		
	}

}
