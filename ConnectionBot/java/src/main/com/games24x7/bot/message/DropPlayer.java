package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DropPlayer extends AbstractMessage { 
 
    private List<Long> players;
    private int score;
    private long matchid;
    private long gameid;

    public DropPlayer() {
        super(1048709, 0, 0);
    }

    public DropPlayer(long receiverID, long playerID, List<Long> players,int score,long matchid,long gameid) {
        super(1048709, receiverID, playerID);
        this.players = players;
        this.score = score;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public List<Long> getPlayers() {
        return players;
    }

    public void setPlayers(List<Long> players) {
        this.players = players;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DropPlayer{" +
            "players=" + players +
            "score=" + score +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
