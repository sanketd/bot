package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BalUpdateObj extends AbstractMessage { 
 
    private double bal;

    public BalUpdateObj() {
        super(1048694, 0, 0);
    }

    public BalUpdateObj(long receiverID, long playerID, double bal) {
        super(1048694, receiverID, playerID);
        this.bal = bal;
    }




    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BalUpdateObj{" +
            "bal=" + bal +
        "}";
    }
}
