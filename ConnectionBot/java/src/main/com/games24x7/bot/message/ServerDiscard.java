package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ServerDiscard extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private String cardid;
    private boolean wrongf;

    public ServerDiscard() {
        super(1048652, 0, 0);
    }

    public ServerDiscard(long receiverID, long playerID, long matchid,long gameid,String cardid,boolean wrongf) {
        super(1048652, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.cardid = cardid;
        this.wrongf = wrongf;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public boolean isWrongf() {
        return wrongf;
    }

    public void setWrongf(boolean wrongf) {
        this.wrongf = wrongf;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ServerDiscard{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "cardid=" + cardid +
            "wrongf=" + wrongf +
        "}";
    }
}
