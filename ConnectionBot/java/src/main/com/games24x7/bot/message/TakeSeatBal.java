package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TakeSeatBal extends AbstractMessage { 
 
    private double bal;
    private long timer;
    private double tablemax;
    private double tablemin;
    private boolean rebuy;
    private List<Seats> seats;

    public TakeSeatBal() {
        super(1048661, 0, 0);
    }

    public TakeSeatBal(long receiverID, long playerID, double bal,long timer,double tablemax,double tablemin,boolean rebuy,List<Seats> seats) {
        super(1048661, receiverID, playerID);
        this.bal = bal;
        this.timer = timer;
        this.tablemax = tablemax;
        this.tablemin = tablemin;
        this.rebuy = rebuy;
        this.seats = seats;
    }




    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    public long getTimer() {
        return timer;
    }

    public void setTimer(long timer) {
        this.timer = timer;
    }

    public double getTablemax() {
        return tablemax;
    }

    public void setTablemax(double tablemax) {
        this.tablemax = tablemax;
    }

    public double getTablemin() {
        return tablemin;
    }

    public void setTablemin(double tablemin) {
        this.tablemin = tablemin;
    }

    public boolean isRebuy() {
        return rebuy;
    }

    public void setRebuy(boolean rebuy) {
        this.rebuy = rebuy;
    }

    public List<Seats> getSeats() {
        return seats;
    }

    public void setSeats(List<Seats> seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TakeSeatBal{" +
            "bal=" + bal +
            "timer=" + timer +
            "tablemax=" + tablemax +
            "tablemin=" + tablemin +
            "rebuy=" + rebuy +
            "seats=" + seats +
        "}";
    }
}
