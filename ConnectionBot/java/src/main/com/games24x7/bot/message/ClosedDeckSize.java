package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ClosedDeckSize extends AbstractMessage { 
 
    private long matchid;
    private long size;
    private long gameid;

    public ClosedDeckSize() {
        super(1048700, 0, 0);
    }

    public ClosedDeckSize(long receiverID, long playerID, long matchid,long size,long gameid) {
        super(1048700, receiverID, playerID);
        this.matchid = matchid;
        this.size = size;
        this.gameid = gameid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ClosedDeckSize{" +
            "matchid=" + matchid +
            "size=" + size +
            "gameid=" + gameid +
        "}";
    }
}
