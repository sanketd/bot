package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ChangeTableId extends AbstractMessage { 
 
    private long newtableid;

    public ChangeTableId() {
        super(1048761, 0, 0);
    }

    public ChangeTableId(long receiverID, long playerID, long newtableid) {
        super(1048761, receiverID, playerID);
        this.newtableid = newtableid;
    }




    public long getNewtableid() {
        return newtableid;
    }

    public void setNewtableid(long newtableid) {
        this.newtableid = newtableid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ChangeTableId{" +
            "newtableid=" + newtableid +
        "}";
    }
}
