package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlayerScores extends AbstractMessage { 
 
    private long matchid;
    private long mid;
    private long gameid;
    private List<ScoreList> scorelist;

    public PlayerScores() {
        super(1048628, 0, 0);
    }

    public PlayerScores(long receiverID, long playerID, long matchid,long mid,long gameid,List<ScoreList> scorelist) {
        super(1048628, receiverID, playerID);
        this.matchid = matchid;
        this.mid = mid;
        this.gameid = gameid;
        this.scorelist = scorelist;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public List<ScoreList> getScorelist() {
        return scorelist;
    }

    public void setScorelist(List<ScoreList> scorelist) {
        this.scorelist = scorelist;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerScores{" +
            "matchid=" + matchid +
            "mid=" + mid +
            "gameid=" + gameid +
            "scorelist=" + scorelist +
        "}";
    }
}
