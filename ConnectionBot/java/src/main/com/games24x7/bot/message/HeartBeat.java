package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class HeartBeat extends AbstractMessage {

    public HeartBeat() {
        super(2097153, 0, 0);
    }

    public HeartBeat(long receiverID, long playerID) {
        super(2097153, receiverID, playerID);
    }

    @Override
    public String toString() {
        return "HeartBeat{" +
        "}";
    }
}
