package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class WinnerMsg extends AbstractMessage { 
 
    private int rank;
    private double amount;
    private boolean split;
    private boolean showfmgflag;
    private boolean isactive;
    private boolean balanceverifed;

    public WinnerMsg() {
        super(1048678, 0, 0);
    }

    public WinnerMsg(long receiverID, long playerID, int rank,double amount,boolean split,boolean showfmgflag,boolean isactive,boolean balanceverifed) {
        super(1048678, receiverID, playerID);
        this.rank = rank;
        this.amount = amount;
        this.split = split;
        this.showfmgflag = showfmgflag;
        this.isactive = isactive;
        this.balanceverifed = balanceverifed;
    }




    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public boolean isSplit() {
        return split;
    }

    public void setSplit(boolean split) {
        this.split = split;
    }

    public boolean isShowfmgflag() {
        return showfmgflag;
    }

    public void setShowfmgflag(boolean showfmgflag) {
        this.showfmgflag = showfmgflag;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public boolean isBalanceverifed() {
        return balanceverifed;
    }

    public void setBalanceverifed(boolean balanceverifed) {
        this.balanceverifed = balanceverifed;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"WinnerMsg{" +
            "rank=" + rank +
            "amount=" + amount +
            "split=" + split +
            "showfmgflag=" + showfmgflag +
            "isactive=" + isactive +
            "balanceverifed=" + balanceverifed +
        "}";
    }
}
