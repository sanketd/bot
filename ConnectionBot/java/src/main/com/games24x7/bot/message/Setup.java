package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Setup extends AbstractMessage { 
 
    private String sessionid;
    private String fmgparams;
    private int clientvariant;

    public Setup() {
        super(1048611, 0, 0);
    }

    public Setup(long receiverID, long playerID, String sessionid,String fmgparams,int clientvariant) {
        super(1048611, receiverID, playerID);
        this.sessionid = sessionid;
        this.fmgparams = fmgparams;
        this.clientvariant = clientvariant;
    }




    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getFmgparams() {
        return fmgparams;
    }

    public void setFmgparams(String fmgparams) {
        this.fmgparams = fmgparams;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Setup{" +
            "sessionid=" + sessionid +
            "fmgparams=" + fmgparams +
            "clientvariant=" + clientvariant +
        "}";
    }
}
