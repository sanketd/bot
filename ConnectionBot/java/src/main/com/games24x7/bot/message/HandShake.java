package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class HandShake extends AbstractMessage {
    private String ipaddress;

    public HandShake() {
        super(2097155, 0, 0);
    }

    public HandShake(long receiverID, long playerID , String ipaddress) {
        super(2097155, receiverID, playerID);
        this.ipaddress = ipaddress;
    }

    public String getIpaddress() {
        return ipaddress;
    }

    public void setIpaddress(String ipaddress) {
        this.ipaddress = ipaddress;
    }

    @Override
    public String toString() {
        return "HandShake{" +
            "ipaddress=" + ipaddress +
        "}";
    }
}
