package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlrDisconnect extends AbstractMessage { 
 
    private List<Long> playerid;
    private long matchid;
    private long gameid;

    public PlrDisconnect() {
        super(1048634, 0, 0);
    }

    public PlrDisconnect(long receiverID, long playerID, List<Long> playerid,long matchid,long gameid) {
        super(1048634, receiverID, playerID);
        this.playerid = playerid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public List<Long> getPlayerid() {
        return playerid;
    }

    public void setPlayerid(List<Long> playerid) {
        this.playerid = playerid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlrDisconnect{" +
            "playerid=" + playerid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
