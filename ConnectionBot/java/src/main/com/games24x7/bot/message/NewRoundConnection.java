package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class NewRoundConnection extends AbstractMessage { 
 
    private String fmgparams;
    private String sessionid;
    private int clientvariant;

    public NewRoundConnection() {
        super(1048599, 0, 0);
    }

    public NewRoundConnection(long receiverID, long playerID, String fmgparams,String sessionid,int clientvariant) {
        super(1048599, receiverID, playerID);
        this.fmgparams = fmgparams;
        this.sessionid = sessionid;
        this.clientvariant = clientvariant;
    }




    public String getFmgparams() {
        return fmgparams;
    }

    public void setFmgparams(String fmgparams) {
        this.fmgparams = fmgparams;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"NewRoundConnection{" +
            "fmgparams=" + fmgparams +
            "sessionid=" + sessionid +
            "clientvariant=" + clientvariant +
        "}";
    }
}
