package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Dealer extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private long mplayerid;

    public Dealer() {
        super(1048702, 0, 0);
    }

    public Dealer(long receiverID, long playerID, long matchid,long gameid,long mplayerid) {
        super(1048702, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Dealer{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
        "}";
    }
}
