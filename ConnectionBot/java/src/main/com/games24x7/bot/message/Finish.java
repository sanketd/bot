package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Finish extends AbstractMessage { 
 
    private String cardid;
    private long mpid;
    private long mid;

    public Finish() {
        super(1048591, 0, 0);
    }

    public Finish(long receiverID, long playerID, String cardid,long mpid,long mid) {
        super(1048591, receiverID, playerID);
        this.cardid = cardid;
        this.mpid = mpid;
        this.mid = mid;
    }




    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Finish{" +
            "cardid=" + cardid +
            "mpid=" + mpid +
            "mid=" + mid +
        "}";
    }
}
