package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class FmgError extends AbstractMessage { 
 
    private String title;
    private String swfmainpath;
    private String action;
    private String errmsg;
    private String fmgparams;

    public FmgError() {
        super(1048731, 0, 0);
    }

    public FmgError(long receiverID, long playerID, String title,String swfmainpath,String action,String errmsg,String fmgparams) {
        super(1048731, receiverID, playerID);
        this.title = title;
        this.swfmainpath = swfmainpath;
        this.action = action;
        this.errmsg = errmsg;
        this.fmgparams = fmgparams;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSwfmainpath() {
        return swfmainpath;
    }

    public void setSwfmainpath(String swfmainpath) {
        this.swfmainpath = swfmainpath;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public String getFmgparams() {
        return fmgparams;
    }

    public void setFmgparams(String fmgparams) {
        this.fmgparams = fmgparams;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"FmgError{" +
            "title=" + title +
            "swfmainpath=" + swfmainpath +
            "action=" + action +
            "errmsg=" + errmsg +
            "fmgparams=" + fmgparams +
        "}";
    }
}
