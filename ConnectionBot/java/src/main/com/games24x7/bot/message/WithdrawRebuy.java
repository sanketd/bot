package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class WithdrawRebuy extends AbstractMessage { 
 
    private int seatid;
    private String sessionid;

    public WithdrawRebuy() {
        super(1048615, 0, 0);
    }

    public WithdrawRebuy(long receiverID, long playerID, int seatid,String sessionid) {
        super(1048615, receiverID, playerID);
        this.seatid = seatid;
        this.sessionid = sessionid;
    }




    public int getSeatid() {
        return seatid;
    }

    public void setSeatid(int seatid) {
        this.seatid = seatid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"WithdrawRebuy{" +
            "seatid=" + seatid +
            "sessionid=" + sessionid +
        "}";
    }
}
