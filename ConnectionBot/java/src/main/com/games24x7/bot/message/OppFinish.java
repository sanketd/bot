package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OppFinish extends AbstractMessage { 
 
    private long extratime;
    private long timetodeclare;
    private long matchid;
    private long totaltimetodeclare;
    private long gameid;
    private long mplayerid;
    private String cardid;

    public OppFinish() {
        super(1048621, 0, 0);
    }

    public OppFinish(long receiverID, long playerID, long extratime,long timetodeclare,long matchid,long totaltimetodeclare,long gameid,long mplayerid,String cardid) {
        super(1048621, receiverID, playerID);
        this.extratime = extratime;
        this.timetodeclare = timetodeclare;
        this.matchid = matchid;
        this.totaltimetodeclare = totaltimetodeclare;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.cardid = cardid;
    }




    public long getExtratime() {
        return extratime;
    }

    public void setExtratime(long extratime) {
        this.extratime = extratime;
    }

    public long getTimetodeclare() {
        return timetodeclare;
    }

    public void setTimetodeclare(long timetodeclare) {
        this.timetodeclare = timetodeclare;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getTotaltimetodeclare() {
        return totaltimetodeclare;
    }

    public void setTotaltimetodeclare(long totaltimetodeclare) {
        this.totaltimetodeclare = totaltimetodeclare;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OppFinish{" +
            "extratime=" + extratime +
            "timetodeclare=" + timetodeclare +
            "matchid=" + matchid +
            "totaltimetodeclare=" + totaltimetodeclare +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "cardid=" + cardid +
        "}";
    }
}
