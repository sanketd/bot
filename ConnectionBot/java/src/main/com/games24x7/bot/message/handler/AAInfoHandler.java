package com.games24x7.bot.message.handler;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.AAInfo;
import com.games24x7.bot.message.BuyIn;
import com.games24x7.bot.message.TakeSeat;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class AAInfoHandler implements BotMessageHandler< AAInfo >
{

	@Override
	public void handleMessage( UserSession session, AAInfo message, PlayerInfo playerInfo )
	{
		long amt = BotImpl.getInstance().getConfiguration().getLongValue( "FMG_AMOUNT" );
		long aaid = message.getAaid();
		long playerId = playerInfo.getPlayerId();
		long newplayerid = message.getPlayerID();
		String sessionId = session.getSessionKey();
		//System.out.println( "AAInfo Msg " + aaid + " " + newplayerid + " " + sessionId + " " );
		if( !playerInfo.isSeated() )
		{
			TakeSeat takeSeat = new TakeSeat();
			takeSeat.setPlayerID( newplayerid );
			takeSeat.setReceiverID( message.getReceiverID() );
			takeSeat.setAaid( aaid );
			takeSeat.setSeatid( playerInfo.getSeatId() );
			takeSeat.setEntryfee( amt );
			takeSeat.setSessionid( sessionId );
			/* takeSeat.setFmg( false ); */
			takeSeat.setSource( String.valueOf( newplayerid ) );
			System.out.println( "Sending take seat msg " + takeSeat );
			if( playerInfo.getSettlementType() != 4 && BotImpl.getInstance().getConfiguration().getBooleanValue( "TAKE_SEAT" ) )
				SendMsgOnChannel.send( takeSeat, playerInfo.getChannel() );
			else if( playerInfo.getSettlementType() == 1 || playerInfo.getSettlementType() == 5 )
			{
				try
				{
					Thread.sleep( 1000 );
				}
				catch( InterruptedException e )
				{
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				BuyIn buyIn = new BuyIn();
				buyIn.setReceiverID( message.getReceiverID() );
				buyIn.setCustomamnt( BotImpl.getInstance().getConfiguration().getIntValue( "CFP_BUYIN_AMOUNT" ) );
				buyIn.setClientvariant( 1 );
				buyIn.setTablemax( BotImpl.getInstance().getConfiguration().getIntValue( "CFP_BUYIN_MAX_AMOUNT" ) );
				buyIn.setTablemin( BotImpl.getInstance().getConfiguration().getIntValue( "CFP_BUYIN_MIN_AMOUNT" ) );
				buyIn.setSeatid( playerInfo.getSeatId() );
				//System.out.println( buyIn.toString() + "" + "-----------------hello---------------------" );
				System.out.println( "Sending Buyin msg " );
				SendMsgOnChannel.send( buyIn, playerInfo.getChannel() );

			}
			else if( playerInfo.getSettlementType() == 2 )
			{
				System.out.println( "Game type : pool " );
			}
		}
		else
		{
			System.out.println( "Player is already seated " );
		}

	}

}
