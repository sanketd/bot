package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ChatRes extends AbstractMessage { 
 
    private String playername;
    private String ctext;

    public ChatRes() {
        super(1048735, 0, 0);
    }

    public ChatRes(long receiverID, long playerID, String playername,String ctext) {
        super(1048735, receiverID, playerID);
        this.playername = playername;
        this.ctext = ctext;
    }




    public String getPlayername() {
        return playername;
    }

    public void setPlayername(String playername) {
        this.playername = playername;
    }

    public String getCtext() {
        return ctext;
    }

    public void setCtext(String ctext) {
        this.ctext = ctext;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ChatRes{" +
            "playername=" + playername +
            "ctext=" + ctext +
        "}";
    }
}
