package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoPlayReq extends AbstractMessage { 
 
    private long time;
    private long matchid;
    private long gameid;

    public AutoPlayReq() {
        super(1048691, 0, 0);
    }

    public AutoPlayReq(long receiverID, long playerID, long time,long matchid,long gameid) {
        super(1048691, receiverID, playerID);
        this.time = time;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoPlayReq{" +
            "time=" + time +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
