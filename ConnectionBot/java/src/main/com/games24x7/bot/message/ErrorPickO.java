package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ErrorPickO extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private String msg;

    public ErrorPickO() {
        super(1048713, 0, 0);
    }

    public ErrorPickO(long receiverID, long playerID, long matchid,long gameid,String msg) {
        super(1048713, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.msg = msg;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ErrorPickO{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "msg=" + msg +
        "}";
    }
}
