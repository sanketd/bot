package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RejoinFail extends AbstractMessage { 
 
    private String msg;
    private String title;

    public RejoinFail() {
        super(1048641, 0, 0);
    }

    public RejoinFail(long receiverID, long playerID, String msg,String title) {
        super(1048641, receiverID, playerID);
        this.msg = msg;
        this.title = title;
    }




    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RejoinFail{" +
            "msg=" + msg +
            "title=" + title +
        "}";
    }
}
