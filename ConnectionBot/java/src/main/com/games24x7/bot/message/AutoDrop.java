package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoDrop extends AbstractMessage { 
 
    private long mplayerid;
    private long matchid;
    private long gameid;

    public AutoDrop() {
        super(1048578, 0, 0);
    }

    public AutoDrop(long receiverID, long playerID, long mplayerid,long matchid,long gameid) {
        super(1048578, receiverID, playerID);
        this.mplayerid = mplayerid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoDrop{" +
            "mplayerid=" + mplayerid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
