package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TECrash extends AbstractMessage { 
 
    private long matchid;
    private long gameid;

    public TECrash() {
        super(1048750, 0, 0);
    }

    public TECrash(long receiverID, long playerID, long matchid,long gameid) {
        super(1048750, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TECrash{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
