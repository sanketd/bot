package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TakeSeat extends AbstractMessage { 
 
    private int seatid;
    private String pptip;
    private int clientvariant;
    private double entryfee;
    private int inprogress;
    private String loginid;
    private String sessionid;
    private int lastte;
    private String hashstr;
    private long aaid;

    public TakeSeat() {
        super(1048612, 0, 0);
    }

    public TakeSeat(long receiverID, long playerID, int seatid,String pptip,int clientvariant,double entryfee,int inprogress,String loginid,String sessionid,int lastte,String hashstr,long aaid) {
        super(1048612, receiverID, playerID);
        this.seatid = seatid;
        this.pptip = pptip;
        this.clientvariant = clientvariant;
        this.entryfee = entryfee;
        this.inprogress = inprogress;
        this.loginid = loginid;
        this.sessionid = sessionid;
        this.lastte = lastte;
        this.hashstr = hashstr;
        this.aaid = aaid;
    }




    public int getSeatid() {
        return seatid;
    }

    public void setSeatid(int seatid) {
        this.seatid = seatid;
    }

    public String getPptip() {
        return pptip;
    }

    public void setPptip(String pptip) {
        this.pptip = pptip;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    public double getEntryfee() {
        return entryfee;
    }

    public void setEntryfee(double entryfee) {
        this.entryfee = entryfee;
    }

    public int getInprogress() {
        return inprogress;
    }

    public void setInprogress(int inprogress) {
        this.inprogress = inprogress;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public int getLastte() {
        return lastte;
    }

    public void setLastte(int lastte) {
        this.lastte = lastte;
    }

    public String getHashstr() {
        return hashstr;
    }

    public void setHashstr(String hashstr) {
        this.hashstr = hashstr;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TakeSeat{" +
            "seatid=" + seatid +
            "pptip=" + pptip +
            "clientvariant=" + clientvariant +
            "entryfee=" + entryfee +
            "inprogress=" + inprogress +
            "loginid=" + loginid +
            "sessionid=" + sessionid +
            "lastte=" + lastte +
            "hashstr=" + hashstr +
            "aaid=" + aaid +
        "}";
    }
}
