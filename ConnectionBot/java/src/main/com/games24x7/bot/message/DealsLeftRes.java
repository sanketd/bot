package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DealsLeftRes extends AbstractMessage { 
 
    private int dealsleft;

    public DealsLeftRes() {
        super(1048585, 0, 0);
    }

    public DealsLeftRes(long receiverID, long playerID, int dealsleft) {
        super(1048585, receiverID, playerID);
        this.dealsleft = dealsleft;
    }




    public int getDealsleft() {
        return dealsleft;
    }

    public void setDealsleft(int dealsleft) {
        this.dealsleft = dealsleft;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DealsLeftRes{" +
            "dealsleft=" + dealsleft +
        "}";
    }
}
