/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.Declare;
import com.games24x7.bot.message.StartDeclare;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

/**
 * @author rupesh
 * 
 */
public class StartDeclareHandler implements BotMessageHandler< StartDeclare >

{
	@Override
	public void handleMessage( UserSession session, StartDeclare message, PlayerInfo playerInfo )
	{
		System.out.println("Start Declare "+message.toString());
		Message declare = new Declare( message.getReceiverID(), playerInfo.getPlayerId(), playerInfo.getMPlayerId(), playerInfo.getMatchId(), playerInfo.getCards(), playerInfo.getScore() );
		SendMsgOnChannel.send( declare, playerInfo.getChannel());
	}

}
