package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DisconProtectionPrefUpdate extends AbstractMessage { 
 
    private int poolrummy;
    private int pointsrummy21;
    private boolean messageshown;
    private int ischecked;
    private int pointsrummy;
    private int mtt;
    private int dealsrummy;

    public DisconProtectionPrefUpdate() {
        super(1048589, 0, 0);
    }

    public DisconProtectionPrefUpdate(long receiverID, long playerID, int poolrummy,int pointsrummy21,boolean messageshown,int ischecked,int pointsrummy,int mtt,int dealsrummy) {
        super(1048589, receiverID, playerID);
        this.poolrummy = poolrummy;
        this.pointsrummy21 = pointsrummy21;
        this.messageshown = messageshown;
        this.ischecked = ischecked;
        this.pointsrummy = pointsrummy;
        this.mtt = mtt;
        this.dealsrummy = dealsrummy;
    }




    public int getPoolrummy() {
        return poolrummy;
    }

    public void setPoolrummy(int poolrummy) {
        this.poolrummy = poolrummy;
    }

    public int getPointsrummy21() {
        return pointsrummy21;
    }

    public void setPointsrummy21(int pointsrummy21) {
        this.pointsrummy21 = pointsrummy21;
    }

    public boolean isMessageshown() {
        return messageshown;
    }

    public void setMessageshown(boolean messageshown) {
        this.messageshown = messageshown;
    }

    public int getIschecked() {
        return ischecked;
    }

    public void setIschecked(int ischecked) {
        this.ischecked = ischecked;
    }

    public int getPointsrummy() {
        return pointsrummy;
    }

    public void setPointsrummy(int pointsrummy) {
        this.pointsrummy = pointsrummy;
    }

    public int getMtt() {
        return mtt;
    }

    public void setMtt(int mtt) {
        this.mtt = mtt;
    }

    public int getDealsrummy() {
        return dealsrummy;
    }

    public void setDealsrummy(int dealsrummy) {
        this.dealsrummy = dealsrummy;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DisconProtectionPrefUpdate{" +
            "poolrummy=" + poolrummy +
            "pointsrummy21=" + pointsrummy21 +
            "messageshown=" + messageshown +
            "ischecked=" + ischecked +
            "pointsrummy=" + pointsrummy +
            "mtt=" + mtt +
            "dealsrummy=" + dealsrummy +
        "}";
    }
}
