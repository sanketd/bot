package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoRebuyinPref extends AbstractMessage { 
 
    private boolean autorebuyinenabled;
    private boolean autorebuyindonotshow;

    public AutoRebuyinPref() {
        super(1048752, 0, 0);
    }

    public AutoRebuyinPref(long receiverID, long playerID, boolean autorebuyinenabled,boolean autorebuyindonotshow) {
        super(1048752, receiverID, playerID);
        this.autorebuyinenabled = autorebuyinenabled;
        this.autorebuyindonotshow = autorebuyindonotshow;
    }




    public boolean isAutorebuyinenabled() {
        return autorebuyinenabled;
    }

    public void setAutorebuyinenabled(boolean autorebuyinenabled) {
        this.autorebuyinenabled = autorebuyinenabled;
    }

    public boolean isAutorebuyindonotshow() {
        return autorebuyindonotshow;
    }

    public void setAutorebuyindonotshow(boolean autorebuyindonotshow) {
        this.autorebuyindonotshow = autorebuyindonotshow;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoRebuyinPref{" +
            "autorebuyinenabled=" + autorebuyinenabled +
            "autorebuyindonotshow=" + autorebuyindonotshow +
        "}";
    }
}
