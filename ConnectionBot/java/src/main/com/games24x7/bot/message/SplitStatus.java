package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class SplitStatus extends AbstractMessage { 
 
    private long aaid;
    private List<SplitStatusPlayerList> playerlist;

    public SplitStatus() {
        super(1048657, 0, 0);
    }

    public SplitStatus(long receiverID, long playerID, long aaid,List<SplitStatusPlayerList> playerlist) {
        super(1048657, receiverID, playerID);
        this.aaid = aaid;
        this.playerlist = playerlist;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<SplitStatusPlayerList> getPlayerlist() {
        return playerlist;
    }

    public void setPlayerlist(List<SplitStatusPlayerList> playerlist) {
        this.playerlist = playerlist;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SplitStatus{" +
            "aaid=" + aaid +
            "playerlist=" + playerlist +
        "}";
    }
}
