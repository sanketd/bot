package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class FMGRequest extends AbstractMessage
{

	private long mpid;
	private long excludeTableId;
	private int prizeType;
	private int formatType;
	private int noOfPlayer;
	private double prizePerPoint;
	private double entryFee;
	private int includeInProgress;
	private String ipAddress;
	private long oldTableId;
	private boolean fmgOnTable;
	private boolean emptyTable;
	private boolean interimTableSettlement;

	public FMGRequest()
	{
		super( 2097161, 0, 0 );
	}

	public FMGRequest( long receiverID, long playerID, long mpid, long excludeTableId, int prizeType, int formatType, int noOfPlayer, double prizePerPoint, double entryFee, int includeInProgress,
			String ipAddress, long oldTableId, boolean fmgOnTable, boolean emptyTable, boolean interimTableSettlement )
	{
		super( 2097161, receiverID, playerID );
		this.mpid = mpid;
		this.excludeTableId = excludeTableId;
		this.prizeType = prizeType;
		this.formatType = formatType;
		this.noOfPlayer = noOfPlayer;
		this.prizePerPoint = prizePerPoint;
		this.entryFee = entryFee;
		this.includeInProgress = includeInProgress;
		this.ipAddress = ipAddress;
		this.oldTableId = oldTableId;
		this.fmgOnTable = fmgOnTable;
		this.emptyTable = emptyTable;
		this.interimTableSettlement = interimTableSettlement;
	}

	public long getMpid()
	{
		return mpid;
	}

	public void setMpid( long mpid )
	{
		this.mpid = mpid;
	}

	public long getExcludeTableId()
	{
		return excludeTableId;
	}

	public void setExcludeTableId( long excludeTableId )
	{
		this.excludeTableId = excludeTableId;
	}

	public int getPrizeType()
	{
		return prizeType;
	}

	public void setPrizeType( int prizeType )
	{
		this.prizeType = prizeType;
	}

	public int getFormatType()
	{
		return formatType;
	}

	public void setFormatType( int formatType )
	{
		this.formatType = formatType;
	}

	public int getNoOfPlayer()
	{
		return noOfPlayer;
	}

	public void setNoOfPlayer( int noOfPlayer )
	{
		this.noOfPlayer = noOfPlayer;
	}

	public double getPrizePerPoint()
	{
		return prizePerPoint;
	}

	public void setPrizePerPoint( double prizePerPoint )
	{
		this.prizePerPoint = prizePerPoint;
	}

	public double getEntryFee()
	{
		return entryFee;
	}

	public void setEntryFee( double entryFee )
	{
		this.entryFee = entryFee;
	}

	public int getIncludeInProgress()
	{
		return includeInProgress;
	}

	public void setIncludeInProgress( int includeInProgress )
	{
		this.includeInProgress = includeInProgress;
	}

	public String getIpAddress()
	{
		return ipAddress;
	}

	public void setIpAddress( String ipAddress )
	{
		this.ipAddress = ipAddress;
	}

	public long getOldTableId()
	{
		return oldTableId;
	}

	public void setOldTableId( long oldTableId )
	{
		this.oldTableId = oldTableId;
	}

	public boolean isFmgOnTable()
	{
		return fmgOnTable;
	}

	public void setFmgOnTable( boolean fmgOnTable )
	{
		this.fmgOnTable = fmgOnTable;
	}

	public boolean isEmptyTable()
	{
		return emptyTable;
	}

	public void setEmptyTable( boolean emptyTable )
	{
		this.emptyTable = emptyTable;
	}

	public boolean isInterimTableSettlement()
	{
		return interimTableSettlement;
	}

	public void setInterimTableSettlement( boolean interimTableSettlement )
	{
		this.interimTableSettlement = interimTableSettlement;
	}
	
	@Override
	public String toString()
	{
		return "Super :" + super.toString() + " Child " + "FMGRequest{" + "mpid=" + mpid + "excludeTableId=" + excludeTableId + "prizeType=" + prizeType + "formatType=" + formatType
				+ "noOfPlayer=" + noOfPlayer + "prizePerPoint=" + prizePerPoint + "entryFee=" + entryFee + "includeInProgress=" + includeInProgress + "ipAddress=" + ipAddress
				+ "oldTableId=" + oldTableId + "fmgOnTable=" + fmgOnTable + "emptyTable=" + emptyTable + "interimTableSettlement=" + interimTableSettlement + "}";
	}
}
