package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class PlayerList extends ChildAbstractMessage{
 
    private double amount;
    private int dropsremaining;
    private String loginid;
    private int totalscore;
    private String status;

    public PlayerList() {
    super(1048656);
    }

    public PlayerList( double amount,int dropsremaining,String loginid,int totalscore,String status) {
         super(1048656);
        this.amount = amount;
        this.dropsremaining = dropsremaining;
        this.loginid = loginid;
        this.totalscore = totalscore;
        this.status = status;
    }




    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public int getDropsremaining() {
        return dropsremaining;
    }

    public void setDropsremaining(int dropsremaining) {
        this.dropsremaining = dropsremaining;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public int getTotalscore() {
        return totalscore;
    }

    public void setTotalscore(int totalscore) {
        this.totalscore = totalscore;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerList{" +
            "amount=" + amount +
            "dropsremaining=" + dropsremaining +
            "loginid=" + loginid +
            "totalscore=" + totalscore +
            "status=" + status +
        "}";
    }
}
