package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class MTTScoreListResp extends ChildAbstractMessage{
 
    private int chips;
    private String username;
    private int rank;

    public MTTScoreListResp() {
    super(1048760);
    }

    public MTTScoreListResp( int chips,String username,int rank) {
         super(1048760);
        this.chips = chips;
        this.username = username;
        this.rank = rank;
    }




    public int getChips() {
        return chips;
    }

    public void setChips(int chips) {
        this.chips = chips;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MTTScoreListResp{" +
            "chips=" + chips +
            "username=" + username +
            "rank=" + rank +
        "}";
    }
}
