package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ThemeRes extends AbstractMessage { 
 
    private String swfmainpath;
    private String configfilepath;

    public ThemeRes() {
        super(1048664, 0, 0);
    }

    public ThemeRes(long receiverID, long playerID, String swfmainpath,String configfilepath) {
        super(1048664, receiverID, playerID);
        this.swfmainpath = swfmainpath;
        this.configfilepath = configfilepath;
    }




    public String getSwfmainpath() {
        return swfmainpath;
    }

    public void setSwfmainpath(String swfmainpath) {
        this.swfmainpath = swfmainpath;
    }

    public String getConfigfilepath() {
        return configfilepath;
    }

    public void setConfigfilepath(String configfilepath) {
        this.configfilepath = configfilepath;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ThemeRes{" +
            "swfmainpath=" + swfmainpath +
            "configfilepath=" + configfilepath +
        "}";
    }
}
