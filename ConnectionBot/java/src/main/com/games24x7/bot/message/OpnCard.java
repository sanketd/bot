package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OpnCard extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private String cardid;

    public OpnCard() {
        super(1048617, 0, 0);
    }

    public OpnCard(long receiverID, long playerID, long matchid,long gameid,String cardid) {
        super(1048617, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.cardid = cardid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OpnCard{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "cardid=" + cardid +
        "}";
    }
}
