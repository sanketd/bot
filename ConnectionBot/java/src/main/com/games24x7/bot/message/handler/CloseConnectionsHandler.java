package com.games24x7.bot.message.handler;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.CloseConnection;
import com.games24x7.framework.configuration.Configuration;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.StartConnectionBots;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class CloseConnectionsHandler implements BotMessageHandler< CloseConnection >
{
	Configuration config = null;
	StartConnectionBots connBot = null;

	@Override
	public void handleMessage( UserSession session, CloseConnection message, PlayerInfo playerInfo )
	{
		if( BotImpl.getInstance().getConfiguration().getBooleanValue( "INFINITE_GAME" ) )
		{
			
				long playerId = playerInfo.getPlayerId();
				System.out.println( "Completed creating new game from CLOSE CONNECTION " + playerId );
				BotMain.executeInfiniteGame( playerId );
			
		}
	}

}
