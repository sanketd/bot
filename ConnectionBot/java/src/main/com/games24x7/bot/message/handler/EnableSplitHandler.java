package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.EnableSplit;
import com.games24x7.bot.message.InitiateSplit;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class EnableSplitHandler implements BotMessageHandler< EnableSplit >
{

	@Override
	public void handleMessage( UserSession session, EnableSplit message, PlayerInfo plrInfo )
	{
		// TODO Auto-generated method stub
		//System.out.println( "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%Inside Enable split"+ message );
		if( plrInfo.getSeatId() == 2)
		{
		//	System.out.println("Enable split************************************"+plrInfo.getPlayerId() );
			Message initiatesplit = new InitiateSplit( message.getReceiverID(), plrInfo.getPlayerId() );
			SendMsgOnChannel.send( initiatesplit, plrInfo.getChannel() );
			System.out.println( "Split initiated by:"+plrInfo.getPlayerId() );
		}
	}
}
