package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OppPickO extends AbstractMessage { 
 
    private String nextocardid;
    private long matchid;
    private long gameid;
    private long mplayerid;
    private String cardid;

    public OppPickO() {
        super(1048623, 0, 0);
    }

    public OppPickO(long receiverID, long playerID, String nextocardid,long matchid,long gameid,long mplayerid,String cardid) {
        super(1048623, receiverID, playerID);
        this.nextocardid = nextocardid;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.cardid = cardid;
    }




    public String getNextocardid() {
        return nextocardid;
    }

    public void setNextocardid(String nextocardid) {
        this.nextocardid = nextocardid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OppPickO{" +
            "nextocardid=" + nextocardid +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "cardid=" + cardid +
        "}";
    }
}
