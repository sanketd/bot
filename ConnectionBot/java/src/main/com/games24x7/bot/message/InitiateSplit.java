package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class InitiateSplit extends AbstractMessage { 
 

    public InitiateSplit() {
        super(1048596, 0, 0);
    }

    public InitiateSplit(long receiverID, long playerID) {
        super(1048596, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"InitiateSplit{" +
        "}";
    }
}
