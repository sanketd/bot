package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AAStatus extends AbstractMessage { 
 
    private int aastatus;
    private long aaid;
    private long maxplayer;
    private int dealnumber;
    private int roundnumber;
    private boolean tiebreaker;

    public AAStatus() {
        super(1048683, 0, 0);
    }

    public AAStatus(long receiverID, long playerID, int aastatus,long aaid,long maxplayer,int dealnumber,int roundnumber,boolean tiebreaker) {
        super(1048683, receiverID, playerID);
        this.aastatus = aastatus;
        this.aaid = aaid;
        this.maxplayer = maxplayer;
        this.dealnumber = dealnumber;
        this.roundnumber = roundnumber;
        this.tiebreaker = tiebreaker;
    }




    public int getAastatus() {
        return aastatus;
    }

    public void setAastatus(int aastatus) {
        this.aastatus = aastatus;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public long getMaxplayer() {
        return maxplayer;
    }

    public void setMaxplayer(long maxplayer) {
        this.maxplayer = maxplayer;
    }

    public int getDealnumber() {
        return dealnumber;
    }

    public void setDealnumber(int dealnumber) {
        this.dealnumber = dealnumber;
    }

    public int getRoundnumber() {
        return roundnumber;
    }

    public void setRoundnumber(int roundnumber) {
        this.roundnumber = roundnumber;
    }

    public boolean isTiebreaker() {
        return tiebreaker;
    }

    public void setTiebreaker(boolean tiebreaker) {
        this.tiebreaker = tiebreaker;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AAStatus{" +
            "aastatus=" + aastatus +
            "aaid=" + aaid +
            "maxplayer=" + maxplayer +
            "dealnumber=" + dealnumber +
            "roundnumber=" + roundnumber +
            "tiebreaker=" + tiebreaker +
        "}";
    }
}
