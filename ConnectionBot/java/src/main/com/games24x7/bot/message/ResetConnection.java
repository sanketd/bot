package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ResetConnection extends AbstractMessage { 
 
    private long nexttableid;

    public ResetConnection() {
        super(1048644, 0, 0);
    }

    public ResetConnection(long receiverID, long playerID, long nexttableid) {
        super(1048644, receiverID, playerID);
        this.nexttableid = nexttableid;
    }




    public long getNexttableid() {
        return nexttableid;
    }

    public void setNexttableid(long nexttableid) {
        this.nexttableid = nexttableid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ResetConnection{" +
            "nexttableid=" + nexttableid +
        "}";
    }
}
