package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AcceptSplit extends AbstractMessage { 
 

    public AcceptSplit() {
        super(1048577, 0, 0);
    }

    public AcceptSplit(long receiverID, long playerID) {
        super(1048577, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AcceptSplit{" +
        "}";
    }
}
