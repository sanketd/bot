package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AmountWon extends AbstractMessage { 
 
    private long matchid;
    private List<AmountWonObj> amountownlist;
    private boolean lastdeal;

    public AmountWon() {
        super(1048686, 0, 0);
    }

    public AmountWon(long receiverID, long playerID, long matchid,List<AmountWonObj> amountownlist,boolean lastdeal) {
        super(1048686, receiverID, playerID);
        this.matchid = matchid;
        this.amountownlist = amountownlist;
        this.lastdeal = lastdeal;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public List<AmountWonObj> getAmountownlist() {
        return amountownlist;
    }

    public void setAmountownlist(List<AmountWonObj> amountownlist) {
        this.amountownlist = amountownlist;
    }

    public boolean isLastdeal() {
        return lastdeal;
    }

    public void setLastdeal(boolean lastdeal) {
        this.lastdeal = lastdeal;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AmountWon{" +
            "matchid=" + matchid +
            "amountownlist=" + amountownlist +
            "lastdeal=" + lastdeal +
        "}";
    }
}
