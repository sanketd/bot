/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.JkrCard;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class JkrCardHandler implements BotMessageHandler< JkrCard >
{
	@Override
	public void handleMessage( UserSession session, JkrCard message, PlayerInfo playerInfo )
	{
		playerInfo.setJkrCard( message.getCardid() );
		System.out.println("Joker Card "+message.getCardid() );
	}

}
