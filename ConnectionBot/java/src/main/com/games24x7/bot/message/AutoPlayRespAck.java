package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoPlayRespAck extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private boolean isautoplay;

    public AutoPlayRespAck() {
        super(1048692, 0, 0);
    }

    public AutoPlayRespAck(long receiverID, long playerID, long matchid,long gameid,boolean isautoplay) {
        super(1048692, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.isautoplay = isautoplay;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public boolean isIsautoplay() {
        return isautoplay;
    }

    public void setIsautoplay(boolean isautoplay) {
        this.isautoplay = isautoplay;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoPlayRespAck{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "isautoplay=" + isautoplay +
        "}";
    }
}
