package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class SplitDetails extends AbstractMessage { 
 
    private double totalprize;
    private double timer;
    private String initiatedby;
    private long aaid;
    private List<PlayerList> playerlist;

    public SplitDetails() {
        super(1048655, 0, 0);
    }

    public SplitDetails(long receiverID, long playerID, double totalprize,double timer,String initiatedby,long aaid,List<PlayerList> playerlist) {
        super(1048655, receiverID, playerID);
        this.totalprize = totalprize;
        this.timer = timer;
        this.initiatedby = initiatedby;
        this.aaid = aaid;
        this.playerlist = playerlist;
    }




    public double getTotalprize() {
        return totalprize;
    }

    public void setTotalprize(double totalprize) {
        this.totalprize = totalprize;
    }

    public double getTimer() {
        return timer;
    }

    public void setTimer(double timer) {
        this.timer = timer;
    }

    public String getInitiatedby() {
        return initiatedby;
    }

    public void setInitiatedby(String initiatedby) {
        this.initiatedby = initiatedby;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<PlayerList> getPlayerlist() {
        return playerlist;
    }

    public void setPlayerlist(List<PlayerList> playerlist) {
        this.playerlist = playerlist;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SplitDetails{" +
            "totalprize=" + totalprize +
            "timer=" + timer +
            "initiatedby=" + initiatedby +
            "aaid=" + aaid +
            "playerlist=" + playerlist +
        "}";
    }
}
