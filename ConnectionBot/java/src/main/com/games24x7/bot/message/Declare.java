package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Declare extends AbstractMessage { 
 
    private long mpid;
    private long mid;
    private List<List<String>> cards;
    private int topgroupscount;

    public Declare() {
        super(1048586, 0, 0);
    }

    public Declare(long receiverID, long playerID, long mpid,long mid,List<List<String>> cards,int topgroupscount) {
        super(1048586, receiverID, playerID);
        this.mpid = mpid;
        this.mid = mid;
        this.cards = cards;
        this.topgroupscount = topgroupscount;
    }




    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public List<List<String>> getCards() {
        return cards;
    }

    public void setCards(List<List<String>> cards) {
        this.cards = cards;
    }

    public int getTopgroupscount() {
        return topgroupscount;
    }

    public void setTopgroupscount(int topgroupscount) {
        this.topgroupscount = topgroupscount;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Declare{" +
            "mpid=" + mpid +
            "mid=" + mid +
            "cards=" + cards +
            "topgroupscount=" + topgroupscount +
        "}";
    }
}
