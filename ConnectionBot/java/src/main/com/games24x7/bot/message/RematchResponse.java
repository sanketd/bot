package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RematchResponse extends AbstractMessage { 
 
    private int rematchMessageType;
    private long tournamentId;
    private int rematchCountDown;
    private String playerLoginId;
    private String opponentLoginId;
    private double entreeFee;

    public RematchResponse() {
        super(1048755, 0, 0);
    }

    public RematchResponse(long receiverID, long playerID, int rematchMessageType,long tournamentId,int rematchCountDown,String playerLoginId,String opponentLoginId,double entreeFee) {
        super(1048755, receiverID, playerID);
        this.rematchMessageType = rematchMessageType;
        this.tournamentId = tournamentId;
        this.rematchCountDown = rematchCountDown;
        this.playerLoginId = playerLoginId;
        this.opponentLoginId = opponentLoginId;
        this.entreeFee = entreeFee;
    }




    public int getRematchMessageType() {
        return rematchMessageType;
    }

    public void setRematchMessageType(int rematchMessageType) {
        this.rematchMessageType = rematchMessageType;
    }

    public long getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(long tournamentId) {
        this.tournamentId = tournamentId;
    }

    public int getRematchCountDown() {
        return rematchCountDown;
    }

    public void setRematchCountDown(int rematchCountDown) {
        this.rematchCountDown = rematchCountDown;
    }

    public String getPlayerLoginId() {
        return playerLoginId;
    }

    public void setPlayerLoginId(String playerLoginId) {
        this.playerLoginId = playerLoginId;
    }

    public String getOpponentLoginId() {
        return opponentLoginId;
    }

    public void setOpponentLoginId(String opponentLoginId) {
        this.opponentLoginId = opponentLoginId;
    }

    public double getEntreeFee() {
        return entreeFee;
    }

    public void setEntreeFee(double entreeFee) {
        this.entreeFee = entreeFee;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RematchResponse{" +
            "rematchMessageType=" + rematchMessageType +
            "tournamentId=" + tournamentId +
            "rematchCountDown=" + rematchCountDown +
            "playerLoginId=" + playerLoginId +
            "opponentLoginId=" + opponentLoginId +
            "entreeFee=" + entreeFee +
        "}";
    }
}
