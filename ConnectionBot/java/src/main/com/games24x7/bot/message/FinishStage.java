package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class FinishStage extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private String fcard;
    private int showdeclare;

    public FinishStage() {
        super(1048714, 0, 0);
    }

    public FinishStage(long receiverID, long playerID, long matchid,long gameid,String fcard,int showdeclare) {
        super(1048714, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.fcard = fcard;
        this.showdeclare = showdeclare;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getFcard() {
        return fcard;
    }

    public void setFcard(String fcard) {
        this.fcard = fcard;
    }

    public int getShowdeclare() {
        return showdeclare;
    }

    public void setShowdeclare(int showdeclare) {
        this.showdeclare = showdeclare;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"FinishStage{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "fcard=" + fcard +
            "showdeclare=" + showdeclare +
        "}";
    }
}
