package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class. THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class Pickc extends AbstractMessage
{

	private long mpid;
	private long mid;

	public Pickc()
	{
		super( 1048600, 0, 0 );
	}

	public Pickc( long receiverID, long playerID, long mpid, long mid )
	{
		super( 1048600, receiverID, playerID );
		this.mpid = mpid;
		this.mid = mid;
	}

	public long getMpid()
	{
		return mpid;
	}

	public void setMpid( long mpid )
	{
		this.mpid = mpid;
	}

	public long getMid()
	{
		return mid;
	}

	public void setMid( long mid )
	{
		this.mid = mid;
	}

	@Override
	public String toString()
	{
		return "Super :" + super.toString() + " Child " + "Pickc{" + "mpid=" + mpid + "mid=" + mid + "}";
	}
}
