package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class OppDrop extends AbstractMessage { 
 
    private int point;
    private long matchid;
    private long gameid;
    private long mplayerid;
    private int mdrop;

    public OppDrop() {
        super(1048620, 0, 0);
    }

    public OppDrop(long receiverID, long playerID, int point,long matchid,long gameid,long mplayerid,int mdrop) {
        super(1048620, receiverID, playerID);
        this.point = point;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.mdrop = mdrop;
    }




    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public int getMdrop() {
        return mdrop;
    }

    public void setMdrop(int mdrop) {
        this.mdrop = mdrop;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"OppDrop{" +
            "point=" + point +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "mdrop=" + mdrop +
        "}";
    }
}
