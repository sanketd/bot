package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BuyInFail extends AbstractMessage { 
 
    private long aaid;
    private String msg;

    public BuyInFail() {
        super(1048742, 0, 0);
    }

    public BuyInFail(long receiverID, long playerID, long aaid,String msg) {
        super(1048742, receiverID, playerID);
        this.aaid = aaid;
        this.msg = msg;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BuyInFail{" +
            "aaid=" + aaid +
            "msg=" + msg +
        "}";
    }
}
