package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class GrpsInfoObj extends AbstractMessage { 
 
    private String code;
    private int type;
    private List<String> cards;

    public GrpsInfoObj() {
        super(1048730, 0, 0);
    }

    public GrpsInfoObj(long receiverID, long playerID, String code,int type,List<String> cards) {
        super(1048730, receiverID, playerID);
        this.code = code;
        this.type = type;
        this.cards = cards;
    }




    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public List<String> getCards() {
        return cards;
    }

    public void setCards(List<String> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"GrpsInfoObj{" +
            "code=" + code +
            "type=" + type +
            "cards=" + cards +
        "}";
    }
}
