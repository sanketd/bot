package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Discardcs extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private String cards;

    public Discardcs() {
        super(1048588, 0, 0);
    }

    public Discardcs(long receiverID, long playerID, long matchid,long gameid,String cards) {
        super(1048588, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.cards = cards;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getCards() {
        return cards;
    }

    public void setCards(String cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Discardcs{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "cards=" + cards +
        "}";
    }
}
