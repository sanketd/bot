package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class GmStatus extends AbstractMessage { 
 
    private int statusid;
    private long matchid;
    private long gameid;

    public GmStatus() {
        super(1048716, 0, 0);
    }

    public GmStatus(long receiverID, long playerID, int statusid,long matchid,long gameid) {
        super(1048716, receiverID, playerID);
        this.statusid = statusid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public int getStatusid() {
        return statusid;
    }

    public void setStatusid(int statusid) {
        this.statusid = statusid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"GmStatus{" +
            "statusid=" + statusid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
