package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class GmSetup extends AbstractMessage { 
 
    private boolean isfirstpracticegame;
    private int currdeal;
    private long starttime;
    private long matchid;
    private int maxdeal;
    private long gameid;
    private int movenumber;

    public GmSetup() {
        super(1048715, 0, 0);
    }

    public GmSetup(long receiverID, long playerID, boolean isfirstpracticegame,int currdeal,long starttime,long matchid,int maxdeal,long gameid,int movenumber) {
        super(1048715, receiverID, playerID);
        this.isfirstpracticegame = isfirstpracticegame;
        this.currdeal = currdeal;
        this.starttime = starttime;
        this.matchid = matchid;
        this.maxdeal = maxdeal;
        this.gameid = gameid;
        this.movenumber = movenumber;
    }




    public boolean isIsfirstpracticegame() {
        return isfirstpracticegame;
    }

    public void setIsfirstpracticegame(boolean isfirstpracticegame) {
        this.isfirstpracticegame = isfirstpracticegame;
    }

    public int getCurrdeal() {
        return currdeal;
    }

    public void setCurrdeal(int currdeal) {
        this.currdeal = currdeal;
    }

    public long getStarttime() {
        return starttime;
    }

    public void setStarttime(long starttime) {
        this.starttime = starttime;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public int getMaxdeal() {
        return maxdeal;
    }

    public void setMaxdeal(int maxdeal) {
        this.maxdeal = maxdeal;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getMovenumber() {
        return movenumber;
    }

    public void setMovenumber(int movenumber) {
        this.movenumber = movenumber;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"GmSetup{" +
            "isfirstpracticegame=" + isfirstpracticegame +
            "currdeal=" + currdeal +
            "starttime=" + starttime +
            "matchid=" + matchid +
            "maxdeal=" + maxdeal +
            "gameid=" + gameid +
            "movenumber=" + movenumber +
        "}";
    }
}
