package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class LastDealErr extends AbstractMessage { 
 
    private String msg;

    public LastDealErr() {
        super(1048724, 0, 0);
    }

    public LastDealErr(long receiverID, long playerID, String msg) {
        super(1048724, receiverID, playerID);
        this.msg = msg;
    }




    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"LastDealErr{" +
            "msg=" + msg +
        "}";
    }
}
