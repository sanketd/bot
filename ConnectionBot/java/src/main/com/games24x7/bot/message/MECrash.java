package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MECrash extends AbstractMessage { 
 
    private String msg;
    private String title;

    public MECrash() {
        super(1048746, 0, 0);
    }

    public MECrash(long receiverID, long playerID, String msg,String title) {
        super(1048746, receiverID, playerID);
        this.msg = msg;
        this.title = title;
    }




    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MECrash{" +
            "msg=" + msg +
            "title=" + title +
        "}";
    }
}
