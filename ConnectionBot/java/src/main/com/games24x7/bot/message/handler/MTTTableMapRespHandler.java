package com.games24x7.bot.message.handler;

import java.util.concurrent.ExecutorService;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.MttTableMapResp;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class MTTTableMapRespHandler implements BotMessageHandler< MttTableMapResp >
{
	ExecutorService executor = null;

	@Override
	public void handleMessage( UserSession session, MttTableMapResp message, PlayerInfo playerInfo )
	{
		System.out.println( "Inside MTTTableMapRespHandler " + message );
		// TODO spawn worker threads
		System.out.println( message.getMttplayerid() );
		System.out.println( message.getMtttableid() );

		if( message.getMttplayerid().size() != 0 )
		{
			if( message.getMtttableid().size() != 0 )
			{
				for( int i = 0; i < message.getMttplayerid().size(); i++ )
				{
					BotImpl.getInstance()
							.getBots()
							.executeMTT( BotImpl.getInstance().getConfiguration().getIntValue( "NO_OF_TOURNAMENT" ),
									BotImpl.getInstance().getConfiguration().getIntValue( "TOURNAMENT_ID_INCREAMENT" ), message.getMtttableid().get( i ),
									BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" ), message.getMttplayerid().get( i ),
									BotImpl.getInstance().getConfiguration().getStringValue( "CS_IP" ),
									BotImpl.getInstance().getConfiguration().getIntValue( "CS_PORT" ),
									BotImpl.getInstance().getConfiguration().getIntValue( "PLAYER_ID_INCREAMENT" ), false,
									BotImpl.getInstance().getConfiguration().getLongValue( "MTT_TOURNAMENT_ID" ),
									BotImpl.getInstance().getConfiguration().getIntValue( "SETTLEMENT_TYPE" ) ,
									BotImpl.getInstance().getConfiguration().getIntValue( "MOD_CHANNEL" ),
									BotImpl.getInstance().getConfiguration().getBooleanValue( "FMG_FLAG" ),
									BotImpl.getInstance().getConfiguration().getIntValue( "FMG_AMOUNT" ));
				}
			}
			else
			{
				System.out.println( "no tables for given tournament" );
				System.exit( 0 );
			}

		}
		else
		{
			System.out.println( "no players for given tournament" );
			System.exit( 0 );
		}
	}

}
