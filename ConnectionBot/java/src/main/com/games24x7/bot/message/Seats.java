package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class Seats extends ChildAbstractMessage{
 
    private int id;
    private boolean seatedout;
    private int state;
    private double seatplayerbalance;
    private int playerscore;
    private boolean autodrop;
    private Player player;

    public Seats() {
    super(1048632);
    }

    public Seats( int id,boolean seatedout,int state,double seatplayerbalance,int playerscore,boolean autodrop,Player player) {
         super(1048632);
        this.id = id;
        this.seatedout = seatedout;
        this.state = state;
        this.seatplayerbalance = seatplayerbalance;
        this.playerscore = playerscore;
        this.autodrop = autodrop;
        this.player = player;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isSeatedout() {
        return seatedout;
    }

    public void setSeatedout(boolean seatedout) {
        this.seatedout = seatedout;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public double getSeatplayerbalance() {
        return seatplayerbalance;
    }

    public void setSeatplayerbalance(double seatplayerbalance) {
        this.seatplayerbalance = seatplayerbalance;
    }

    public int getPlayerscore() {
        return playerscore;
    }

    public void setPlayerscore(int playerscore) {
        this.playerscore = playerscore;
    }

    public boolean isAutodrop() {
        return autodrop;
    }

    public void setAutodrop(boolean autodrop) {
        this.autodrop = autodrop;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Seats{" +
            "id=" + id +
            "seatedout=" + seatedout +
            "state=" + state +
            "seatplayerbalance=" + seatplayerbalance +
            "playerscore=" + playerscore +
            "autodrop=" + autodrop +
            "player=" + player +
        "}";
    }
}
