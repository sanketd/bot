package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Ack extends AbstractMessage { 
 
    private long lastrecmsgid;

    public Ack() {
        super(1048685, 0, 0);
    }

    public Ack(long receiverID, long playerID, long lastrecmsgid) {
        super(1048685, receiverID, playerID);
        this.lastrecmsgid = lastrecmsgid;
    }




    public long getLastrecmsgid() {
        return lastrecmsgid;
    }

    public void setLastrecmsgid(long lastrecmsgid) {
        this.lastrecmsgid = lastrecmsgid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Ack{" +
            "lastrecmsgid=" + lastrecmsgid +
        "}";
    }
}
