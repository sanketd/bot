package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class SaveTheme extends AbstractMessage { 
 
    private int themeid;
    private long tournamentid;

    public SaveTheme() {
        super(1048758, 0, 0);
    }

    public SaveTheme(long receiverID, long playerID, int themeid,long tournamentid) {
        super(1048758, receiverID, playerID);
        this.themeid = themeid;
        this.tournamentid = tournamentid;
    }




    public int getThemeid() {
        return themeid;
    }

    public void setThemeid(int themeid) {
        this.themeid = themeid;
    }

    public long getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(long tournamentid) {
        this.tournamentid = tournamentid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SaveTheme{" +
            "themeid=" + themeid +
            "tournamentid=" + tournamentid +
        "}";
    }
}
