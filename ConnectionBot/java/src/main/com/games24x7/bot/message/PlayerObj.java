package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlayerObj extends AbstractMessage { 
 
    private boolean hasplayedpracticegame;
    private int status;
    private int clientvariant;
    private String loginid;
    private String emailid;
    private long id;
    private boolean hasplayed21cardpracticegame;
    private int timeofreg;
    private int absoluterank;
    private long idlong;
    private String rating;
    private long tranchrank;
    private long lastpingreceivedtime;

    public PlayerObj() {
        super(1048728, 0, 0);
    }

    public PlayerObj(long receiverID, long playerID, boolean hasplayedpracticegame,int status,int clientvariant,String loginid,String emailid,long id,boolean hasplayed21cardpracticegame,int timeofreg,int absoluterank,long idlong,String rating,long tranchrank,long lastpingreceivedtime) {
        super(1048728, receiverID, playerID);
        this.hasplayedpracticegame = hasplayedpracticegame;
        this.status = status;
        this.clientvariant = clientvariant;
        this.loginid = loginid;
        this.emailid = emailid;
        this.id = id;
        this.hasplayed21cardpracticegame = hasplayed21cardpracticegame;
        this.timeofreg = timeofreg;
        this.absoluterank = absoluterank;
        this.idlong = idlong;
        this.rating = rating;
        this.tranchrank = tranchrank;
        this.lastpingreceivedtime = lastpingreceivedtime;
    }




    public boolean isHasplayedpracticegame() {
        return hasplayedpracticegame;
    }

    public void setHasplayedpracticegame(boolean hasplayedpracticegame) {
        this.hasplayedpracticegame = hasplayedpracticegame;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public String getEmailid() {
        return emailid;
    }

    public void setEmailid(String emailid) {
        this.emailid = emailid;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public boolean isHasplayed21cardpracticegame() {
        return hasplayed21cardpracticegame;
    }

    public void setHasplayed21cardpracticegame(boolean hasplayed21cardpracticegame) {
        this.hasplayed21cardpracticegame = hasplayed21cardpracticegame;
    }

    public int getTimeofreg() {
        return timeofreg;
    }

    public void setTimeofreg(int timeofreg) {
        this.timeofreg = timeofreg;
    }

    public int getAbsoluterank() {
        return absoluterank;
    }

    public void setAbsoluterank(int absoluterank) {
        this.absoluterank = absoluterank;
    }

    public long getIdlong() {
        return idlong;
    }

    public void setIdlong(long idlong) {
        this.idlong = idlong;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public long getTranchrank() {
        return tranchrank;
    }

    public void setTranchrank(long tranchrank) {
        this.tranchrank = tranchrank;
    }

    public long getLastpingreceivedtime() {
        return lastpingreceivedtime;
    }

    public void setLastpingreceivedtime(long lastpingreceivedtime) {
        this.lastpingreceivedtime = lastpingreceivedtime;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerObj{" +
            "hasplayedpracticegame=" + hasplayedpracticegame +
            "status=" + status +
            "clientvariant=" + clientvariant +
            "loginid=" + loginid +
            "emailid=" + emailid +
            "id=" + id +
            "hasplayed21cardpracticegame=" + hasplayed21cardpracticegame +
            "timeofreg=" + timeofreg +
            "absoluterank=" + absoluterank +
            "idlong=" + idlong +
            "rating=" + rating +
            "tranchrank=" + tranchrank +
            "lastpingreceivedtime=" + lastpingreceivedtime +
        "}";
    }
}
