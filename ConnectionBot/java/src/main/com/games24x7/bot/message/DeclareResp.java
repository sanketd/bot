package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DeclareResp extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private long mplayerid;
    private int succ;
    private int score;

    public DeclareResp() {
        super(1048705, 0, 0);
    }

    public DeclareResp(long receiverID, long playerID, long matchid,long gameid,long mplayerid,int succ,int score) {
        super(1048705, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
        this.succ = succ;
        this.score = score;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public int getSucc() {
        return succ;
    }

    public void setSucc(int succ) {
        this.succ = succ;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DeclareResp{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
            "succ=" + succ +
            "score=" + score +
        "}";
    }
}
