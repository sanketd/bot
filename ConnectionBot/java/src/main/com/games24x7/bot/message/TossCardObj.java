package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class TossCardObj extends ChildAbstractMessage{
 
    private long mplayerid;
    private long gameid;
    private String cardid;

    public TossCardObj() {
    super(1048667);
    }

    public TossCardObj( long mplayerid,long gameid,String cardid) {
         super(1048667);
        this.mplayerid = mplayerid;
        this.gameid = gameid;
        this.cardid = cardid;
    }




    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TossCardObj{" +
            "mplayerid=" + mplayerid +
            "gameid=" + gameid +
            "cardid=" + cardid +
        "}";
    }
}
