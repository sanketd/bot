package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ScoreWindowValue extends ChildAbstractMessage{
 
    private String message;
    private String result;
    private int topgroupscount;
    private String username;
    private int score;
    private long mplayerid;
    private String loginid;
    private int handcardgain;
    private ValCardStr valuecardstr;
    private long id;
    private List<List<String>> cards;

    public ScoreWindowValue() {
    super(1048649);
    }

    public ScoreWindowValue( String message,String result,int topgroupscount,String username,int score,long mplayerid,String loginid,int handcardgain,ValCardStr valuecardstr,long id,List<List<String>> cards) {
         super(1048649);
        this.message = message;
        this.result = result;
        this.topgroupscount = topgroupscount;
        this.username = username;
        this.score = score;
        this.mplayerid = mplayerid;
        this.loginid = loginid;
        this.handcardgain = handcardgain;
        this.valuecardstr = valuecardstr;
        this.id = id;
        this.cards = cards;
    }




    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public int getTopgroupscount() {
        return topgroupscount;
    }

    public void setTopgroupscount(int topgroupscount) {
        this.topgroupscount = topgroupscount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public int getHandcardgain() {
        return handcardgain;
    }

    public void setHandcardgain(int handcardgain) {
        this.handcardgain = handcardgain;
    }

    public ValCardStr getValuecardstr() {
        return valuecardstr;
    }

    public void setValuecardstr(ValCardStr valuecardstr) {
        this.valuecardstr = valuecardstr;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<List<String>> getCards() {
        return cards;
    }

    public void setCards(List<List<String>> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreWindowValue{" +
            "message=" + message +
            "result=" + result +
            "topgroupscount=" + topgroupscount +
            "username=" + username +
            "score=" + score +
            "mplayerid=" + mplayerid +
            "loginid=" + loginid +
            "handcardgain=" + handcardgain +
            "valuecardstr=" + valuecardstr +
            "id=" + id +
            "cards=" + cards +
        "}";
    }
}
