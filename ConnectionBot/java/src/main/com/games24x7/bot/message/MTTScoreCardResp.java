package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MTTScoreCardResp extends AbstractMessage { 
 
    private long matchid;
    private List<MTTScoreListResp> scores;

    public MTTScoreCardResp() {
        super(1048759, 0, 0);
    }

    public MTTScoreCardResp(long receiverID, long playerID, long matchid,List<MTTScoreListResp> scores) {
        super(1048759, receiverID, playerID);
        this.matchid = matchid;
        this.scores = scores;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public List<MTTScoreListResp> getScores() {
        return scores;
    }

    public void setScores(List<MTTScoreListResp> scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MTTScoreCardResp{" +
            "matchid=" + matchid +
            "scores=" + scores +
        "}";
    }
}
