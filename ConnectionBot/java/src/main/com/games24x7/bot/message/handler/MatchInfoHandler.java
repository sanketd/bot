/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.MatchInfo;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class MatchInfoHandler implements BotMessageHandler< MatchInfo >
{
	@Override
	public void handleMessage( UserSession session, MatchInfo message, PlayerInfo playerInfo )
	{
		long receiverId = message.getReceiverID();
		long playerId = playerInfo.getPlayerId();
		long mpid = message.getMpid();
		long matchId = message.getMatchid();
		playerInfo.setMPlayerId( mpid );
		playerInfo.setMatchId( matchId );
		System.out.println("MatchInfo msg "+receiverId+" "+playerId+" "+matchId+" "+mpid);

	}

}
