package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AAInfo extends AbstractMessage { 
 
    private GProperty gproperty;
    private TProperty tproperty;
    private int aanum;
    private String gametype;
    private long aaid;
    private boolean progressbarmax;
    private int prizetype;
    private int multiplier;
    private int rewardpointsvalue;

    public AAInfo() {
        super(1048680, 0, 0);
    }

    public AAInfo(long receiverID, long playerID, GProperty gproperty,TProperty tproperty,int aanum,String gametype,long aaid,boolean progressbarmax,int prizetype,int multiplier,int rewardpointsvalue) {
        super(1048680, receiverID, playerID);
        this.gproperty = gproperty;
        this.tproperty = tproperty;
        this.aanum = aanum;
        this.gametype = gametype;
        this.aaid = aaid;
        this.progressbarmax = progressbarmax;
        this.prizetype = prizetype;
        this.multiplier = multiplier;
        this.rewardpointsvalue = rewardpointsvalue;
    }




    public GProperty getGproperty() {
        return gproperty;
    }

    public void setGproperty(GProperty gproperty) {
        this.gproperty = gproperty;
    }

    public TProperty getTproperty() {
        return tproperty;
    }

    public void setTproperty(TProperty tproperty) {
        this.tproperty = tproperty;
    }

    public int getAanum() {
        return aanum;
    }

    public void setAanum(int aanum) {
        this.aanum = aanum;
    }

    public String getGametype() {
        return gametype;
    }

    public void setGametype(String gametype) {
        this.gametype = gametype;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public boolean isProgressbarmax() {
        return progressbarmax;
    }

    public void setProgressbarmax(boolean progressbarmax) {
        this.progressbarmax = progressbarmax;
    }

    public int getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(int prizetype) {
        this.prizetype = prizetype;
    }

    public int getMultiplier() {
        return multiplier;
    }

    public void setMultiplier(int multiplier) {
        this.multiplier = multiplier;
    }

    public int getRewardpointsvalue() {
        return rewardpointsvalue;
    }

    public void setRewardpointsvalue(int rewardpointsvalue) {
        this.rewardpointsvalue = rewardpointsvalue;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AAInfo{" +
            "gproperty=" + gproperty +
            "tproperty=" + tproperty +
            "aanum=" + aanum +
            "gametype=" + gametype +
            "aaid=" + aaid +
            "progressbarmax=" + progressbarmax +
            "prizetype=" + prizetype +
            "multiplier=" + multiplier +
            "rewardpointsvalue=" + rewardpointsvalue +
        "}";
    }
}
