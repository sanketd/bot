package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlayerEliminated extends AbstractMessage { 
 
    private String title;
    private boolean balanceverifed;
    private boolean isactive;
    private boolean showfmgflag;
    private String msg;
    private int rank;
    private int tranchrank;
    private boolean initiateRematch;
    private double entryFee;
    private int rematchCountDown;

    public PlayerEliminated() {
        super(1048627, 0, 0);
    }

    public PlayerEliminated(long receiverID, long playerID, String title,boolean balanceverifed,boolean isactive,boolean showfmgflag,String msg,int rank,int tranchrank,boolean initiateRematch,double entryFee,int rematchCountDown) {
        super(1048627, receiverID, playerID);
        this.title = title;
        this.balanceverifed = balanceverifed;
        this.isactive = isactive;
        this.showfmgflag = showfmgflag;
        this.msg = msg;
        this.rank = rank;
        this.tranchrank = tranchrank;
        this.initiateRematch = initiateRematch;
        this.entryFee = entryFee;
        this.rematchCountDown = rematchCountDown;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBalanceverifed() {
        return balanceverifed;
    }

    public void setBalanceverifed(boolean balanceverifed) {
        this.balanceverifed = balanceverifed;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public boolean isShowfmgflag() {
        return showfmgflag;
    }

    public void setShowfmgflag(boolean showfmgflag) {
        this.showfmgflag = showfmgflag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getTranchrank() {
        return tranchrank;
    }

    public void setTranchrank(int tranchrank) {
        this.tranchrank = tranchrank;
    }

    public boolean isInitiateRematch() {
        return initiateRematch;
    }

    public void setInitiateRematch(boolean initiateRematch) {
        this.initiateRematch = initiateRematch;
    }

    public double getEntryFee() {
        return entryFee;
    }

    public void setEntryFee(double entryFee) {
        this.entryFee = entryFee;
    }

    public int getRematchCountDown() {
        return rematchCountDown;
    }

    public void setRematchCountDown(int rematchCountDown) {
        this.rematchCountDown = rematchCountDown;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerEliminated{" +
            "title=" + title +
            "balanceverifed=" + balanceverifed +
            "isactive=" + isactive +
            "showfmgflag=" + showfmgflag +
            "msg=" + msg +
            "rank=" + rank +
            "tranchrank=" + tranchrank +
            "initiateRematch=" + initiateRematch +
            "entryFee=" + entryFee +
            "rematchCountDown=" + rematchCountDown +
        "}";
    }
}
