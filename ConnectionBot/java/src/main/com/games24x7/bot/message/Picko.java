package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Picko extends AbstractMessage { 
 
    private long mpid;
    private long mid;

    public Picko() {
        super(1048601, 0, 0);
    }

    public Picko(long receiverID, long playerID, long mpid,long mid) {
        super(1048601, receiverID, playerID);
        this.mpid = mpid;
        this.mid = mid;
    }




    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Picko{" +
            "mpid=" + mpid +
            "mid=" + mid +
        "}";
    }
}
