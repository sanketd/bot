package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoDropSuccess extends AbstractMessage { 
 
    private long aaid;
    private long matchid;
    private long gameid;

    public AutoDropSuccess() {
        super(1048690, 0, 0);
    }

    public AutoDropSuccess(long receiverID, long playerID, long aaid,long matchid,long gameid) {
        super(1048690, receiverID, playerID);
        this.aaid = aaid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoDropSuccess{" +
            "aaid=" + aaid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
