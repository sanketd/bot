package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

public class Recon extends AbstractMessage { 
 
    private String fmgparams;
    private String sessionid;
    private String pptip;
    private int clientvariant;
    private long matchid;
    private long gameid;
    private long mplayerid;

    public Recon() {
        super(1048605, 0, 0);
    }

    public Recon(long receiverID, long playerID, String fmgparams,String sessionid,String pptip,int clientvariant,long matchid,long gameid,long mplayerid) {
        super(1048605, receiverID, playerID);
        this.fmgparams = fmgparams;
        this.sessionid = sessionid;
        this.pptip = pptip;
        this.clientvariant = clientvariant;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
    }




    public String getFmgparams() {
        return fmgparams;
    }

    public void setFmgparams(String fmgparams) {
        this.fmgparams = fmgparams;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public String getPptip() {
        return pptip;
    }

    public void setPptip(String pptip) {
        this.pptip = pptip;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Recon{" +
            "fmgparams=" + fmgparams +
            "sessionid=" + sessionid +
            "pptip=" + pptip +
            "clientvariant=" + clientvariant +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
        "}";
    }
}
