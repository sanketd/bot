package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TossWinner extends AbstractMessage { 
 
    private long mplayerid;
    private long gameid;
    private long matchid;

    public TossWinner() {
        super(1048668, 0, 0);
    }

    public TossWinner(long receiverID, long playerID, long mplayerid,long gameid,long matchid) {
        super(1048668, receiverID, playerID);
        this.mplayerid = mplayerid;
        this.gameid = gameid;
        this.matchid = matchid;
    }




    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TossWinner{" +
            "mplayerid=" + mplayerid +
            "gameid=" + gameid +
            "matchid=" + matchid +
        "}";
    }
}
