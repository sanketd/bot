package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ScoreCard extends AbstractMessage { 
 

    public ScoreCard() {
        super(1048609, 0, 0);
    }

    public ScoreCard(long receiverID, long playerID) {
        super(1048609, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreCard{" +
        "}";
    }
}
