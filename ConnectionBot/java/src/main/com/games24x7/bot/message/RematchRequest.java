package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RematchRequest extends AbstractMessage { 
 
    private boolean rematchAccepted;

    public RematchRequest() {
        super(1048754, 0, 0);
    }

    public RematchRequest(long receiverID, long playerID, boolean rematchAccepted) {
        super(1048754, receiverID, playerID);
        this.rematchAccepted = rematchAccepted;
    }




    public boolean isRematchAccepted() {
        return rematchAccepted;
    }

    public void setRematchAccepted(boolean rematchAccepted) {
        this.rematchAccepted = rematchAccepted;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RematchRequest{" +
            "rematchAccepted=" + rematchAccepted +
        "}";
    }
}
