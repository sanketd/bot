/**
 * 
 */
package com.games24x7.bot.message.handler;

import java.util.Timer;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.message.PlrDisconnect;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.ChannelInfo;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class DisconnectHandler implements BotMessageHandler< PlrDisconnect >
{
	private Channel channel;
	private ChannelInfo channelInfo;
	private UserSession us;
	private PlayerInfo plrInfo;
	//private ReceivedMsgQueue receivedMsgQueue = null;
	private Timer timer;
	private boolean handshakeReceived = false;
	@Override
	public void handleMessage( UserSession session, PlrDisconnect message, PlayerInfo playerInfo )
	{/*
		this.channel = BotImpl.getInstance().createCSClientChannel( "54.254.183.150", 9443 );
		this.channelInfo = ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( channel );
		// this.msgOnChannel=new SendMsgOnChannel();
		this.us = new UserSession( Long.toString( channelInfo.getChannelID() ), channelInfo.getSessionKey() );
		this.plrInfo = new PlayerInfo( playerInfo.getPlayerId(), channel );
		HandShake msg = new HandShake( message.getReceiverID(), playerInfo.getPlayerId(), "192.168.48.8" );
		us.setAccountID( Thread.currentThread().getId(), playerInfo.getPlayerId() );
		us.setAccountName( Thread.currentThread().getName() );
		System.out.println( "DisconnectHandler ---->>> " + channel );

		boolean sendSuccess = SendMsgOnChannel.send( msg, channel );
		if( sendSuccess )
		{
			try
			{
				Thread.sleep( 1000 );
				System.out.println( " DisconnectHandler Geting receiver queue" );
				receivedMsgQueue = ChannelIdVsReceiverQueue.get( channelInfo.getChannelID() );
				
				if( receivedMsgQueue != null )
				{
					// TODO receive msg
					// poll queue for msgs
					while( true )
					{
						// System.out.println("polling queue "+receivedMsgQueue);
						Message message2 = receivedMsgQueue.get();
						if( message2 != null )
						{
							System.out.println("DisconnectHandler Message2 " + message2.getClass().toString() );
							if( message2.getClassID() == BotMessageConstants.HANDSHAKERESPONSE )
							{
								handshakeReceived = true;

								plrInfo.getTimer().schedule( new HeartBeatWorkerThread( channel, message2.getReceiverID(), message2.getPlayerID() ), 0, 2000 );
								Recon recon = new Recon( message2.getReceiverID(), plrInfo.getPlayerId(), "", "","", 1, plrInfo.getMatchId(), plrInfo.getTableId(), playerInfo.getMPlayerId() );
								SendMsgOnChannel.send( recon, channel );
							}
							else
							{
								if( handshakeReceived )
								{
									
									if( message2.getClassID() == BotMessageConstants.HEARTBEATRESPONSE )
									{
										HeartBeatResponse heartBeatResponse = ( HeartBeatResponse ) message2;
										// System.out.println("HeartBeatResponse "+heartBeatResponse.toString());
										System.out.println( "DisconnectHandler HeartBeatResponse " + heartBeatResponse.toString() );
									}
									BotMessageHandler messageHandler = BotImpl.getInstance().getMsgHandler( message2.getClassID() );
									try
									{
										if(message2.getClassID() == 1048636)
										{
											System.out.println("INSIDE PLRMOVE AFTER RECON");
										}
										else
										{
											messageHandler.handleMessage( us, message2, plrInfo );
										}
										
									}
									catch( NullPointerException e )
									{
										System.out.println( " DisconnectHandler INsdide null pointer exception -- " + message2 );
										System.out.println(" DisconnectHandler us" +us);
										System.out.println(" DisconnectHandler plrInfo" +plrInfo);
										System.out.println(" DisconnectHandler messageHandler" + messageHandler);
									}
								}
							}
						}
					}
				}
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
	*/}

}
