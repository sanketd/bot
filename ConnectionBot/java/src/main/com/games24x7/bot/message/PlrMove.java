package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlrMove extends AbstractMessage { 
 
    private long totalgracetime;
    private long gracetime;
    private long movetime;
    private boolean isfirstmove;
    private int moveno;
    private long matchid;
    private long gameid;
    private long mplayerid;

    public PlrMove() {
        super(1048636, 0, 0);
    }

    public PlrMove(long receiverID, long playerID, long totalgracetime,long gracetime,long movetime,boolean isfirstmove,int moveno,long matchid,long gameid,long mplayerid) {
        super(1048636, receiverID, playerID);
        this.totalgracetime = totalgracetime;
        this.gracetime = gracetime;
        this.movetime = movetime;
        this.isfirstmove = isfirstmove;
        this.moveno = moveno;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mplayerid = mplayerid;
    }




    public long getTotalgracetime() {
        return totalgracetime;
    }

    public void setTotalgracetime(long totalgracetime) {
        this.totalgracetime = totalgracetime;
    }

    public long getGracetime() {
        return gracetime;
    }

    public void setGracetime(long gracetime) {
        this.gracetime = gracetime;
    }

    public long getMovetime() {
        return movetime;
    }

    public void setMovetime(long movetime) {
        this.movetime = movetime;
    }

    public boolean isIsfirstmove() {
        return isfirstmove;
    }

    public void setIsfirstmove(boolean isfirstmove) {
        this.isfirstmove = isfirstmove;
    }

    public int getMoveno() {
        return moveno;
    }

    public void setMoveno(int moveno) {
        this.moveno = moveno;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlrMove{" +
            "totalgracetime=" + totalgracetime +
            "gracetime=" + gracetime +
            "movetime=" + movetime +
            "isfirstmove=" + isfirstmove +
            "moveno=" + moveno +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mplayerid=" + mplayerid +
        "}";
    }


}
