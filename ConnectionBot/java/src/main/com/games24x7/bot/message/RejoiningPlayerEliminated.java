package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RejoiningPlayerEliminated extends AbstractMessage { 
 
    private String title;
    private boolean balanceverifed;
    private boolean isactive;
    private boolean showfmgflag;
    private String msg;

    public RejoiningPlayerEliminated() {
        super(1048642, 0, 0);
    }

    public RejoiningPlayerEliminated(long receiverID, long playerID, String title,boolean balanceverifed,boolean isactive,boolean showfmgflag,String msg) {
        super(1048642, receiverID, playerID);
        this.title = title;
        this.balanceverifed = balanceverifed;
        this.isactive = isactive;
        this.showfmgflag = showfmgflag;
        this.msg = msg;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isBalanceverifed() {
        return balanceverifed;
    }

    public void setBalanceverifed(boolean balanceverifed) {
        this.balanceverifed = balanceverifed;
    }

    public boolean isIsactive() {
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public boolean isShowfmgflag() {
        return showfmgflag;
    }

    public void setShowfmgflag(boolean showfmgflag) {
        this.showfmgflag = showfmgflag;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RejoiningPlayerEliminated{" +
            "title=" + title +
            "balanceverifed=" + balanceverifed +
            "isactive=" + isactive +
            "showfmgflag=" + showfmgflag +
            "msg=" + msg +
        "}";
    }
}
