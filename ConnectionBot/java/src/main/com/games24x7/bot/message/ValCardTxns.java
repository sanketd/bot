package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ValCardTxns extends ChildAbstractMessage{
 
    private int pointsgiven;
    private int pointsrcd;
    private int netvaluecardgains;
    private ValCardStr valuecardstr;
    private long userid;
    private String loginid;
    private List<PlayerTxn> playertxns;
    private int handCardGain;
    private String status;
    private List<List<String>> cards;

    public ValCardTxns() {
    super(1048677);
    }

    public ValCardTxns( int pointsgiven,int pointsrcd,int netvaluecardgains,ValCardStr valuecardstr,long userid,String loginid,List<PlayerTxn> playertxns,int handCardGain,String status,List<List<String>> cards) {
         super(1048677);
        this.pointsgiven = pointsgiven;
        this.pointsrcd = pointsrcd;
        this.netvaluecardgains = netvaluecardgains;
        this.valuecardstr = valuecardstr;
        this.userid = userid;
        this.loginid = loginid;
        this.playertxns = playertxns;
        this.handCardGain = handCardGain;
        this.status = status;
        this.cards = cards;
    }




    public int getPointsgiven() {
        return pointsgiven;
    }

    public void setPointsgiven(int pointsgiven) {
        this.pointsgiven = pointsgiven;
    }

    public int getPointsrcd() {
        return pointsrcd;
    }

    public void setPointsrcd(int pointsrcd) {
        this.pointsrcd = pointsrcd;
    }

    public int getNetvaluecardgains() {
        return netvaluecardgains;
    }

    public void setNetvaluecardgains(int netvaluecardgains) {
        this.netvaluecardgains = netvaluecardgains;
    }

    public ValCardStr getValuecardstr() {
        return valuecardstr;
    }

    public void setValuecardstr(ValCardStr valuecardstr) {
        this.valuecardstr = valuecardstr;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getLoginid() {
        return loginid;
    }

    public void setLoginid(String loginid) {
        this.loginid = loginid;
    }

    public List<PlayerTxn> getPlayertxns() {
        return playertxns;
    }

    public void setPlayertxns(List<PlayerTxn> playertxns) {
        this.playertxns = playertxns;
    }

    public int getHandCardGain() {
        return handCardGain;
    }

    public void setHandCardGain(int handCardGain) {
        this.handCardGain = handCardGain;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<List<String>> getCards() {
        return cards;
    }

    public void setCards(List<List<String>> cards) {
        this.cards = cards;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ValCardTxns{" +
            "pointsgiven=" + pointsgiven +
            "pointsrcd=" + pointsrcd +
            "netvaluecardgains=" + netvaluecardgains +
            "valuecardstr=" + valuecardstr +
            "userid=" + userid +
            "loginid=" + loginid +
            "playertxns=" + playertxns +
            "handCardGain=" + handCardGain +
            "status=" + status +
            "cards=" + cards +
        "}";
    }
}
