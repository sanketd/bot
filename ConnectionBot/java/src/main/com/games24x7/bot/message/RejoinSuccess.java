package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RejoinSuccess extends AbstractMessage { 
 
    private String title;
    private int rejoinscore;
    private String msg;

    public RejoinSuccess() {
        super(1048643, 0, 0);
    }

    public RejoinSuccess(long receiverID, long playerID, String title,int rejoinscore,String msg) {
        super(1048643, receiverID, playerID);
        this.title = title;
        this.rejoinscore = rejoinscore;
        this.msg = msg;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getRejoinscore() {
        return rejoinscore;
    }

    public void setRejoinscore(int rejoinscore) {
        this.rejoinscore = rejoinscore;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RejoinSuccess{" +
            "title=" + title +
            "rejoinscore=" + rejoinscore +
            "msg=" + msg +
        "}";
    }
}
