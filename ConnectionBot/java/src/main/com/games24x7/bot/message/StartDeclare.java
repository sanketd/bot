package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class StartDeclare extends AbstractMessage { 
 
    private long timetodeclare;
    private long totalgracetime;
    private long gracetime;
    private long matchid;
    private long totaltimetodeclare;
    private long gameid;

    public StartDeclare() {
        super(1048659, 0, 0);
    }

    public StartDeclare(long receiverID, long playerID, long timetodeclare,long totalgracetime,long gracetime,long matchid,long totaltimetodeclare,long gameid) {
        super(1048659, receiverID, playerID);
        this.timetodeclare = timetodeclare;
        this.totalgracetime = totalgracetime;
        this.gracetime = gracetime;
        this.matchid = matchid;
        this.totaltimetodeclare = totaltimetodeclare;
        this.gameid = gameid;
    }




    public long getTimetodeclare() {
        return timetodeclare;
    }

    public void setTimetodeclare(long timetodeclare) {
        this.timetodeclare = timetodeclare;
    }

    public long getTotalgracetime() {
        return totalgracetime;
    }

    public void setTotalgracetime(long totalgracetime) {
        this.totalgracetime = totalgracetime;
    }

    public long getGracetime() {
        return gracetime;
    }

    public void setGracetime(long gracetime) {
        this.gracetime = gracetime;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getTotaltimetodeclare() {
        return totaltimetodeclare;
    }

    public void setTotaltimetodeclare(long totaltimetodeclare) {
        this.totaltimetodeclare = totaltimetodeclare;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"StartDeclare{" +
            "timetodeclare=" + timetodeclare +
            "totalgracetime=" + totalgracetime +
            "gracetime=" + gracetime +
            "matchid=" + matchid +
            "totaltimetodeclare=" + totaltimetodeclare +
            "gameid=" + gameid +
        "}";
    }
}
