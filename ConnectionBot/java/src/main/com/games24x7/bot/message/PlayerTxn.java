package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlayerTxn extends AbstractMessage { 
 
    private long toplayer;
    private long toplayeruserid;
    private String fromplayerloginid;
    private String toplayerloginid;
    private int score;
    private long fromplayeruserid;
    private long fromplayer;

    public PlayerTxn() {
        super(1048744, 0, 0);
    }

    public PlayerTxn(long receiverID, long playerID, long toplayer,long toplayeruserid,String fromplayerloginid,String toplayerloginid,int score,long fromplayeruserid,long fromplayer) {
        super(1048744, receiverID, playerID);
        this.toplayer = toplayer;
        this.toplayeruserid = toplayeruserid;
        this.fromplayerloginid = fromplayerloginid;
        this.toplayerloginid = toplayerloginid;
        this.score = score;
        this.fromplayeruserid = fromplayeruserid;
        this.fromplayer = fromplayer;
    }




    public long getToplayer() {
        return toplayer;
    }

    public void setToplayer(long toplayer) {
        this.toplayer = toplayer;
    }

    public long getToplayeruserid() {
        return toplayeruserid;
    }

    public void setToplayeruserid(long toplayeruserid) {
        this.toplayeruserid = toplayeruserid;
    }

    public String getFromplayerloginid() {
        return fromplayerloginid;
    }

    public void setFromplayerloginid(String fromplayerloginid) {
        this.fromplayerloginid = fromplayerloginid;
    }

    public String getToplayerloginid() {
        return toplayerloginid;
    }

    public void setToplayerloginid(String toplayerloginid) {
        this.toplayerloginid = toplayerloginid;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public long getFromplayeruserid() {
        return fromplayeruserid;
    }

    public void setFromplayeruserid(long fromplayeruserid) {
        this.fromplayeruserid = fromplayeruserid;
    }

    public long getFromplayer() {
        return fromplayer;
    }

    public void setFromplayer(long fromplayer) {
        this.fromplayer = fromplayer;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerTxn{" +
            "toplayer=" + toplayer +
            "toplayeruserid=" + toplayeruserid +
            "fromplayerloginid=" + fromplayerloginid +
            "toplayerloginid=" + toplayerloginid +
            "score=" + score +
            "fromplayeruserid=" + fromplayeruserid +
            "fromplayer=" + fromplayer +
        "}";
    }
}
