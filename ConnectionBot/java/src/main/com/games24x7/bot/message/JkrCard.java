package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class JkrCard extends AbstractMessage { 
 
    private String cardid;
    private long matchid;
    private long gameid;

    public JkrCard() {
        super(1048725, 0, 0);
    }

    public JkrCard(long receiverID, long playerID, String cardid,long matchid,long gameid) {
        super(1048725, receiverID, playerID);
        this.cardid = cardid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public String getCardid() {
        return cardid;
    }

    public void setCardid(String cardid) {
        this.cardid = cardid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"JkrCard{" +
            "cardid=" + cardid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
