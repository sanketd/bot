package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Theme extends AbstractMessage { 
 
    private int id;
    private String thumbimage;
    private String name;
    private String bigimage;
    private String configfile;
    private int dealtype;

    public Theme() {
        super(1048663, 0, 0);
    }

    public Theme(long receiverID, long playerID, int id,String thumbimage,String name,String bigimage,String configfile,int dealtype) {
        super(1048663, receiverID, playerID);
        this.id = id;
        this.thumbimage = thumbimage;
        this.name = name;
        this.bigimage = bigimage;
        this.configfile = configfile;
        this.dealtype = dealtype;
    }




    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getThumbimage() {
        return thumbimage;
    }

    public void setThumbimage(String thumbimage) {
        this.thumbimage = thumbimage;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBigimage() {
        return bigimage;
    }

    public void setBigimage(String bigimage) {
        this.bigimage = bigimage;
    }

    public String getConfigfile() {
        return configfile;
    }

    public void setConfigfile(String configfile) {
        this.configfile = configfile;
    }

    public int getDealtype() {
        return dealtype;
    }

    public void setDealtype(int dealtype) {
        this.dealtype = dealtype;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Theme{" +
            "id=" + id +
            "thumbimage=" + thumbimage +
            "name=" + name +
            "bigimage=" + bigimage +
            "configfile=" + configfile +
            "dealtype=" + dealtype +
        "}";
    }
}
