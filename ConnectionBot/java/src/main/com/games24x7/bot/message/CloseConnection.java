package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class CloseConnection extends AbstractMessage { 
 
    private long matchid;
    private long mid;
    private long gameid;

    public CloseConnection() {
        super(1048699, 0, 0);
    }

    public CloseConnection(long receiverID, long playerID, long matchid,long mid,long gameid) {
        super(1048699, receiverID, playerID);
        this.matchid = matchid;
        this.mid = mid;
        this.gameid = gameid;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"CloseConnection{" +
            "matchid=" + matchid +
            "mid=" + mid +
            "gameid=" + gameid +
        "}";
    }
}
