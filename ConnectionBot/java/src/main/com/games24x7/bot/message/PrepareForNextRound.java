package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PrepareForNextRound extends AbstractMessage { 
 
    private int roundnumber;
    private long nextroundstarttime;

    public PrepareForNextRound() {
        super(1048637, 0, 0);
    }

    public PrepareForNextRound(long receiverID, long playerID, int roundnumber,long nextroundstarttime) {
        super(1048637, receiverID, playerID);
        this.roundnumber = roundnumber;
        this.nextroundstarttime = nextroundstarttime;
    }




    public int getRoundnumber() {
        return roundnumber;
    }

    public void setRoundnumber(int roundnumber) {
        this.roundnumber = roundnumber;
    }

    public long getNextroundstarttime() {
        return nextroundstarttime;
    }

    public void setNextroundstarttime(long nextroundstarttime) {
        this.nextroundstarttime = nextroundstarttime;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PrepareForNextRound{" +
            "roundnumber=" + roundnumber +
            "nextroundstarttime=" + nextroundstarttime +
        "}";
    }
}
