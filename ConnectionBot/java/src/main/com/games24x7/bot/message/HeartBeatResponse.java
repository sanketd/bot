package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class HeartBeatResponse extends AbstractMessage {
    private int lastrecmsgid;

    public HeartBeatResponse() {
        super(2097154, 0, 0);
    }

    public HeartBeatResponse(long receiverID, long playerID , int lastrecmsgid) {
        super(2097154, receiverID, playerID);
        this.lastrecmsgid = lastrecmsgid;
    }

    public int getLastrecmsgid() {
        return lastrecmsgid;
    }

    public void setLastrecmsgid(int lastrecmsgid) {
        this.lastrecmsgid = lastrecmsgid;
    }

    @Override
    public String toString() {
        return "HeartBeatResponse{" +
            "lastrecmsgid=" + lastrecmsgid +
        "}";
    }
}
