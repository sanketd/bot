package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Chat extends AbstractMessage { 
 
    private String ctext;

    public Chat() {
        super(1048583, 0, 0);
    }

    public Chat(long receiverID, long playerID, String ctext) {
        super(1048583, receiverID, playerID);
        this.ctext = ctext;
    }




    public String getCtext() {
        return ctext;
    }

    public void setCtext(String ctext) {
        this.ctext = ctext;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Chat{" +
            "ctext=" + ctext +
        "}";
    }
}
