package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BONTournamentInfo extends AbstractMessage { 
 
    private List<RankObj> ranks;
    private int totaldeals;
    private double totalprize;
    private int numofprizes;

    public BONTournamentInfo() {
        super(1048695, 0, 0);
    }

    public BONTournamentInfo(long receiverID, long playerID, List<RankObj> ranks,int totaldeals,double totalprize,int numofprizes) {
        super(1048695, receiverID, playerID);
        this.ranks = ranks;
        this.totaldeals = totaldeals;
        this.totalprize = totalprize;
        this.numofprizes = numofprizes;
    }




    public List<RankObj> getRanks() {
        return ranks;
    }

    public void setRanks(List<RankObj> ranks) {
        this.ranks = ranks;
    }

    public int getTotaldeals() {
        return totaldeals;
    }

    public void setTotaldeals(int totaldeals) {
        this.totaldeals = totaldeals;
    }

    public double getTotalprize() {
        return totalprize;
    }

    public void setTotalprize(double totalprize) {
        this.totalprize = totalprize;
    }

    public int getNumofprizes() {
        return numofprizes;
    }

    public void setNumofprizes(int numofprizes) {
        this.numofprizes = numofprizes;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BONTournamentInfo{" +
            "ranks=" + ranks +
            "totaldeals=" + totaldeals +
            "totalprize=" + totalprize +
            "numofprizes=" + numofprizes +
        "}";
    }
}
