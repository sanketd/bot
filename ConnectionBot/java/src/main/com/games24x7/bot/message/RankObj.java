package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RankObj extends AbstractMessage { 
 
    private int rank;
    private double cashprize;

    public RankObj() {
        super(1048696, 0, 0);
    }

    public RankObj(long receiverID, long playerID, int rank,double cashprize) {
        super(1048696, receiverID, playerID);
        this.rank = rank;
        this.cashprize = cashprize;
    }




    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public double getCashprize() {
        return cashprize;
    }

    public void setCashprize(double cashprize) {
        this.cashprize = cashprize;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RankObj{" +
            "rank=" + rank +
            "cashprize=" + cashprize +
        "}";
    }
}
