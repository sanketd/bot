package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Reshuffle extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private int size;

    public Reshuffle() {
        super(1048645, 0, 0);
    }

    public Reshuffle(long receiverID, long playerID, long matchid,long gameid,int size) {
        super(1048645, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.size = size;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Reshuffle{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "size=" + size +
        "}";
    }
}
