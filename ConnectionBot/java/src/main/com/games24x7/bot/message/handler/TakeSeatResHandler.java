package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.BuyIn;
import com.games24x7.bot.message.TakeSeatBal;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class TakeSeatResHandler implements BotMessageHandler< TakeSeatBal >
{

	@Override
	public void handleMessage( UserSession session, TakeSeatBal message, PlayerInfo playerInfo )
	{
		System.out.println("Take seat bal msg "+message.getBal());
		BuyIn buyIn = new BuyIn( message.getReceiverID(), playerInfo.getPlayerId(), 800, 1000, 80, 0, 800, 0);
		SendMsgOnChannel.send( buyIn, playerInfo.getChannel() );
	}

}
