package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BonusValueObj extends AbstractMessage { 
 
    private boolean bonusactive;
    private long userid;
    private double bonusreleasedincurrentsettlement;
    private double remainingamttoplayforcurrentchunk;
    private int daystoexpire;
    private double chunkamount;
    private int totalchunks;
    private int chunksreleased;
    private double entryfeeperchunk;
    private String bonuscode;
    private double remainingbonusamount;
    private double totalbonusamt;
    private PendingBonusObj pendingbonus;

    public BonusValueObj() {
        super(1048720, 0, 0);
    }

    public BonusValueObj(long receiverID, long playerID, boolean bonusactive,long userid,double bonusreleasedincurrentsettlement,double remainingamttoplayforcurrentchunk,int daystoexpire,double chunkamount,int totalchunks,int chunksreleased,double entryfeeperchunk,String bonuscode,double remainingbonusamount,double totalbonusamt,PendingBonusObj pendingbonus) {
        super(1048720, receiverID, playerID);
        this.bonusactive = bonusactive;
        this.userid = userid;
        this.bonusreleasedincurrentsettlement = bonusreleasedincurrentsettlement;
        this.remainingamttoplayforcurrentchunk = remainingamttoplayforcurrentchunk;
        this.daystoexpire = daystoexpire;
        this.chunkamount = chunkamount;
        this.totalchunks = totalchunks;
        this.chunksreleased = chunksreleased;
        this.entryfeeperchunk = entryfeeperchunk;
        this.bonuscode = bonuscode;
        this.remainingbonusamount = remainingbonusamount;
        this.totalbonusamt = totalbonusamt;
        this.pendingbonus = pendingbonus;
    }




    public boolean isBonusactive() {
        return bonusactive;
    }

    public void setBonusactive(boolean bonusactive) {
        this.bonusactive = bonusactive;
    }

    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public double getBonusreleasedincurrentsettlement() {
        return bonusreleasedincurrentsettlement;
    }

    public void setBonusreleasedincurrentsettlement(double bonusreleasedincurrentsettlement) {
        this.bonusreleasedincurrentsettlement = bonusreleasedincurrentsettlement;
    }

    public double getRemainingamttoplayforcurrentchunk() {
        return remainingamttoplayforcurrentchunk;
    }

    public void setRemainingamttoplayforcurrentchunk(double remainingamttoplayforcurrentchunk) {
        this.remainingamttoplayforcurrentchunk = remainingamttoplayforcurrentchunk;
    }

    public int getDaystoexpire() {
        return daystoexpire;
    }

    public void setDaystoexpire(int daystoexpire) {
        this.daystoexpire = daystoexpire;
    }

    public double getChunkamount() {
        return chunkamount;
    }

    public void setChunkamount(double chunkamount) {
        this.chunkamount = chunkamount;
    }

    public int getTotalchunks() {
        return totalchunks;
    }

    public void setTotalchunks(int totalchunks) {
        this.totalchunks = totalchunks;
    }

    public int getChunksreleased() {
        return chunksreleased;
    }

    public void setChunksreleased(int chunksreleased) {
        this.chunksreleased = chunksreleased;
    }

    public double getEntryfeeperchunk() {
        return entryfeeperchunk;
    }

    public void setEntryfeeperchunk(double entryfeeperchunk) {
        this.entryfeeperchunk = entryfeeperchunk;
    }

    public String getBonuscode() {
        return bonuscode;
    }

    public void setBonuscode(String bonuscode) {
        this.bonuscode = bonuscode;
    }

    public double getRemainingbonusamount() {
        return remainingbonusamount;
    }

    public void setRemainingbonusamount(double remainingbonusamount) {
        this.remainingbonusamount = remainingbonusamount;
    }

    public double getTotalbonusamt() {
        return totalbonusamt;
    }

    public void setTotalbonusamt(double totalbonusamt) {
        this.totalbonusamt = totalbonusamt;
    }

    public PendingBonusObj getPendingbonus() {
        return pendingbonus;
    }

    public void setPendingbonus(PendingBonusObj pendingbonus) {
        this.pendingbonus = pendingbonus;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BonusValueObj{" +
            "bonusactive=" + bonusactive +
            "userid=" + userid +
            "bonusreleasedincurrentsettlement=" + bonusreleasedincurrentsettlement +
            "remainingamttoplayforcurrentchunk=" + remainingamttoplayforcurrentchunk +
            "daystoexpire=" + daystoexpire +
            "chunkamount=" + chunkamount +
            "totalchunks=" + totalchunks +
            "chunksreleased=" + chunksreleased +
            "entryfeeperchunk=" + entryfeeperchunk +
            "bonuscode=" + bonuscode +
            "remainingbonusamount=" + remainingbonusamount +
            "totalbonusamt=" + totalbonusamt +
            "pendingbonus=" + pendingbonus +
        "}";
    }
}
