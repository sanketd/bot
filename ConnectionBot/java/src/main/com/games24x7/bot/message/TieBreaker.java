package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TieBreaker extends AbstractMessage { 
 
    private int tienumber;
    private boolean iscurrent;
    private int tietype;
    private List<String> players;

    public TieBreaker() {
        super(1048665, 0, 0);
    }

    public TieBreaker(long receiverID, long playerID, int tienumber,boolean iscurrent,int tietype,List<String> players) {
        super(1048665, receiverID, playerID);
        this.tienumber = tienumber;
        this.iscurrent = iscurrent;
        this.tietype = tietype;
        this.players = players;
    }




    public int getTienumber() {
        return tienumber;
    }

    public void setTienumber(int tienumber) {
        this.tienumber = tienumber;
    }

    public boolean isIscurrent() {
        return iscurrent;
    }

    public void setIscurrent(boolean iscurrent) {
        this.iscurrent = iscurrent;
    }

    public int getTietype() {
        return tietype;
    }

    public void setTietype(int tietype) {
        this.tietype = tietype;
    }

    public List<String> getPlayers() {
        return players;
    }

    public void setPlayers(List<String> players) {
        this.players = players;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TieBreaker{" +
            "tienumber=" + tienumber +
            "iscurrent=" + iscurrent +
            "tietype=" + tietype +
            "players=" + players +
        "}";
    }
}
