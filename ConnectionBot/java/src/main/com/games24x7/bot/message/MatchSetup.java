package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MatchSetup extends AbstractMessage { 
 
    private long mplayerid;
    private long matchid;
    private long gameid;

    public MatchSetup() {
        super(1048751, 0, 0);
    }

    public MatchSetup(long receiverID, long playerID, long mplayerid,long matchid,long gameid) {
        super(1048751, receiverID, playerID);
        this.mplayerid = mplayerid;
        this.matchid = matchid;
        this.gameid = gameid;
    }




    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MatchSetup{" +
            "mplayerid=" + mplayerid +
            "matchid=" + matchid +
            "gameid=" + gameid +
        "}";
    }
}
