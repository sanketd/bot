package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AcceptSplitSuccess extends AbstractMessage { 
 
    private long aaid;

    public AcceptSplitSuccess() {
        super(1048684, 0, 0);
    }

    public AcceptSplitSuccess(long receiverID, long playerID, long aaid) {
        super(1048684, receiverID, playerID);
        this.aaid = aaid;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AcceptSplitSuccess{" +
            "aaid=" + aaid +
        "}";
    }
}
