package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PendingBonusObj extends AbstractMessage { 
 
    private int count;
    private List<Bonuses> bonuses;

    public PendingBonusObj() {
        super(1048737, 0, 0);
    }

    public PendingBonusObj(long receiverID, long playerID, int count,List<Bonuses> bonuses) {
        super(1048737, receiverID, playerID);
        this.count = count;
        this.bonuses = bonuses;
    }




    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Bonuses> getBonuses() {
        return bonuses;
    }

    public void setBonuses(List<Bonuses> bonuses) {
        this.bonuses = bonuses;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PendingBonusObj{" +
            "count=" + count +
            "bonuses=" + bonuses +
        "}";
    }
}
