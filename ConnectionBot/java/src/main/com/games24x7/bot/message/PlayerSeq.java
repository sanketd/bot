package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlayerSeq extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private List<Long> orderedmplayerids;

    public PlayerSeq() {
        super(1048630, 0, 0);
    }

    public PlayerSeq(long receiverID, long playerID, long matchid,long gameid,List<Long> orderedmplayerids) {
        super(1048630, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.orderedmplayerids = orderedmplayerids;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public List<Long> getOrderedmplayerids() {
        return orderedmplayerids;
    }

    public void setOrderedmplayerids(List<Long> orderedmplayerids) {
        this.orderedmplayerids = orderedmplayerids;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlayerSeq{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "orderedmplayerids=" + orderedmplayerids +
        "}";
    }
}
