package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class GraceMove extends AbstractMessage { 
 
    private long totalgracetime;
    private long gracetime;
    private boolean firstmove;
    private long gameid;
    private long matchid;
    private long mplayerid;
    private long movetime;
    private int moveno;

    public GraceMove() {
        super(1048717, 0, 0);
    }

    public GraceMove(long receiverID, long playerID, long totalgracetime,long gracetime,boolean firstmove,long gameid,long matchid,long mplayerid,long movetime,int moveno) {
        super(1048717, receiverID, playerID);
        this.totalgracetime = totalgracetime;
        this.gracetime = gracetime;
        this.firstmove = firstmove;
        this.gameid = gameid;
        this.matchid = matchid;
        this.mplayerid = mplayerid;
        this.movetime = movetime;
        this.moveno = moveno;
    }




    public long getTotalgracetime() {
        return totalgracetime;
    }

    public void setTotalgracetime(long totalgracetime) {
        this.totalgracetime = totalgracetime;
    }

    public long getGracetime() {
        return gracetime;
    }

    public void setGracetime(long gracetime) {
        this.gracetime = gracetime;
    }

    public boolean isFirstmove() {
        return firstmove;
    }

    public void setFirstmove(boolean firstmove) {
        this.firstmove = firstmove;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getMplayerid() {
        return mplayerid;
    }

    public void setMplayerid(long mplayerid) {
        this.mplayerid = mplayerid;
    }

    public long getMovetime() {
        return movetime;
    }

    public void setMovetime(long movetime) {
        this.movetime = movetime;
    }

    public int getMoveno() {
        return moveno;
    }

    public void setMoveno(int moveno) {
        this.moveno = moveno;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"GraceMove{" +
            "totalgracetime=" + totalgracetime +
            "gracetime=" + gracetime +
            "firstmove=" + firstmove +
            "gameid=" + gameid +
            "matchid=" + matchid +
            "mplayerid=" + mplayerid +
            "movetime=" + movetime +
            "moveno=" + moveno +
        "}";
    }
}
