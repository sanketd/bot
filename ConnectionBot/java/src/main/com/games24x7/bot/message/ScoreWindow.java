package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ScoreWindow extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private int dealnumber;
    private String joker;
    private boolean lastdeal;
    private List<ScoreWindowValue> scorewindow;

    public ScoreWindow() {
        super(1048648, 0, 0);
    }

    public ScoreWindow(long receiverID, long playerID, long matchid,long gameid,int dealnumber,String joker,boolean lastdeal,List<ScoreWindowValue> scorewindow) {
        super(1048648, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.dealnumber = dealnumber;
        this.joker = joker;
        this.lastdeal = lastdeal;
        this.scorewindow = scorewindow;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getDealnumber() {
        return dealnumber;
    }

    public void setDealnumber(int dealnumber) {
        this.dealnumber = dealnumber;
    }

    public String getJoker() {
        return joker;
    }

    public void setJoker(String joker) {
        this.joker = joker;
    }

    public boolean isLastdeal() {
        return lastdeal;
    }

    public void setLastdeal(boolean lastdeal) {
        this.lastdeal = lastdeal;
    }

    public List<ScoreWindowValue> getScorewindow() {
        return scorewindow;
    }

    public void setScorewindow(List<ScoreWindowValue> scorewindow) {
        this.scorewindow = scorewindow;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreWindow{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "dealnumber=" + dealnumber +
            "joker=" + joker +
            "lastdeal=" + lastdeal +
            "scorewindow=" + scorewindow +
        "}";
    }
}
