package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BonusIndicator extends AbstractMessage { 
 
    private long userid;
    private String bonuscode;
    private double entryfeeperchunk;
    private int chunksreleased;
    private int totalchunks;
    private double chunkamount;
    private int daystoexpire;
    private double totalbonusamt;
    private double remainingbonusamount;
    private double remainingamttoplayforcurrentchunk;
    private double bonusreleasedincurrentsettlement;
    private boolean bonusactive;

    public BonusIndicator() {
        super(1048736, 0, 0);
    }

    public BonusIndicator(long receiverID, long playerID, long userid,String bonuscode,double entryfeeperchunk,int chunksreleased,int totalchunks,double chunkamount,int daystoexpire,double totalbonusamt,double remainingbonusamount,double remainingamttoplayforcurrentchunk,double bonusreleasedincurrentsettlement,boolean bonusactive) {
        super(1048736, receiverID, playerID);
        this.userid = userid;
        this.bonuscode = bonuscode;
        this.entryfeeperchunk = entryfeeperchunk;
        this.chunksreleased = chunksreleased;
        this.totalchunks = totalchunks;
        this.chunkamount = chunkamount;
        this.daystoexpire = daystoexpire;
        this.totalbonusamt = totalbonusamt;
        this.remainingbonusamount = remainingbonusamount;
        this.remainingamttoplayforcurrentchunk = remainingamttoplayforcurrentchunk;
        this.bonusreleasedincurrentsettlement = bonusreleasedincurrentsettlement;
        this.bonusactive = bonusactive;
    }




    public long getUserid() {
        return userid;
    }

    public void setUserid(long userid) {
        this.userid = userid;
    }

    public String getBonuscode() {
        return bonuscode;
    }

    public void setBonuscode(String bonuscode) {
        this.bonuscode = bonuscode;
    }

    public double getEntryfeeperchunk() {
        return entryfeeperchunk;
    }

    public void setEntryfeeperchunk(double entryfeeperchunk) {
        this.entryfeeperchunk = entryfeeperchunk;
    }

    public int getChunksreleased() {
        return chunksreleased;
    }

    public void setChunksreleased(int chunksreleased) {
        this.chunksreleased = chunksreleased;
    }

    public int getTotalchunks() {
        return totalchunks;
    }

    public void setTotalchunks(int totalchunks) {
        this.totalchunks = totalchunks;
    }

    public double getChunkamount() {
        return chunkamount;
    }

    public void setChunkamount(double chunkamount) {
        this.chunkamount = chunkamount;
    }

    public int getDaystoexpire() {
        return daystoexpire;
    }

    public void setDaystoexpire(int daystoexpire) {
        this.daystoexpire = daystoexpire;
    }

    public double getTotalbonusamt() {
        return totalbonusamt;
    }

    public void setTotalbonusamt(double totalbonusamt) {
        this.totalbonusamt = totalbonusamt;
    }

    public double getRemainingbonusamount() {
        return remainingbonusamount;
    }

    public void setRemainingbonusamount(double remainingbonusamount) {
        this.remainingbonusamount = remainingbonusamount;
    }

    public double getRemainingamttoplayforcurrentchunk() {
        return remainingamttoplayforcurrentchunk;
    }

    public void setRemainingamttoplayforcurrentchunk(double remainingamttoplayforcurrentchunk) {
        this.remainingamttoplayforcurrentchunk = remainingamttoplayforcurrentchunk;
    }

    public double getBonusreleasedincurrentsettlement() {
        return bonusreleasedincurrentsettlement;
    }

    public void setBonusreleasedincurrentsettlement(double bonusreleasedincurrentsettlement) {
        this.bonusreleasedincurrentsettlement = bonusreleasedincurrentsettlement;
    }

    public boolean isBonusactive() {
        return bonusactive;
    }

    public void setBonusactive(boolean bonusactive) {
        this.bonusactive = bonusactive;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BonusIndicator{" +
            "userid=" + userid +
            "bonuscode=" + bonuscode +
            "entryfeeperchunk=" + entryfeeperchunk +
            "chunksreleased=" + chunksreleased +
            "totalchunks=" + totalchunks +
            "chunkamount=" + chunkamount +
            "daystoexpire=" + daystoexpire +
            "totalbonusamt=" + totalbonusamt +
            "remainingbonusamount=" + remainingbonusamount +
            "remainingamttoplayforcurrentchunk=" + remainingamttoplayforcurrentchunk +
            "bonusreleasedincurrentsettlement=" + bonusreleasedincurrentsettlement +
            "bonusactive=" + bonusactive +
        "}";
    }
}
