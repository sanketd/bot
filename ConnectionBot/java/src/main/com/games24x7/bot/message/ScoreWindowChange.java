package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ScoreWindowChange extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private ScoreWindowValue playerresult;

    public ScoreWindowChange() {
        super(1048650, 0, 0);
    }

    public ScoreWindowChange(long receiverID, long playerID, long matchid,long gameid,ScoreWindowValue playerresult) {
        super(1048650, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.playerresult = playerresult;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public ScoreWindowValue getPlayerresult() {
        return playerresult;
    }

    public void setPlayerresult(ScoreWindowValue playerresult) {
        this.playerresult = playerresult;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreWindowChange{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "playerresult=" + playerresult +
        "}";
    }
}
