package com.games24x7.bot.message.handler;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.AmountWon;
import com.games24x7.bot.message.FMGRequest;
import com.games24x7.bot.message.RematchRequest;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class AmountWonHandler implements BotMessageHandler< AmountWon >

{
	private long playerId = -1;
	private int noOfPlayer = 2;
	private double FMGAmount = 0;
	Channel channel = null;

	@Override
	public void handleMessage( UserSession session, AmountWon message, PlayerInfo plrInfo )

	{
		// TODO Auto-generated method stub

		boolean rematch = BotImpl.getInstance().getConfiguration().getBooleanValue( "REMATCH" );
		int players = BotImpl.getInstance().getConfiguration().getIntValue( "PLAYERS_ON_A_TABLE" );
		if( players == 2 && rematch )
		{
			RematchRequest rematchRequest = new RematchRequest( message.getReceiverID(), plrInfo.getPlayerId(), true );
			rematchRequest.setSource( String.valueOf( plrInfo.getPlayerId() ) );
			SendMsgOnChannel.send( rematchRequest, plrInfo.getChannel() );
		}
		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "REMATCH_ACCEPT_REJECT" ) == true )
		{
			int seatId = plrInfo.getSeatId();
			if( seatId == 0 )
			{
				System.out.println( plrInfo.getSeatId() );
				RematchRequest rematchRequest = new RematchRequest( message.getReceiverID(), plrInfo.getPlayerId(), true );
				rematchRequest.setSource( String.valueOf( plrInfo.getPlayerId() ) );
				SendMsgOnChannel.send( rematchRequest, plrInfo.getChannel() );
			}
			else
			{
				System.out.println( "Player on seat id ONE REJECTED" );
				RematchRequest rematchRequest = new RematchRequest( message.getReceiverID(), plrInfo.getPlayerId(), false );
				rematchRequest.setSource( String.valueOf( plrInfo.getPlayerId() ) );
				SendMsgOnChannel.send( rematchRequest, plrInfo.getChannel() );

			}

		}

	}

}
