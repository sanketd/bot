package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class GProperty extends AbstractMessage { 
 
    private int movetime;
    private int noofdeck;
    private int fullcountcfp;
    private int maxbuyinmultiplier21cardcfp;
    private int noofdubleestobesafe21cardcfp;
    private int maxplayer;
    private int blockedwords;
    private long extratime;
    private int nooftunnelastodeclare21cardcfp;
    private int rptplmobserver;
    private long mvtimefirstplayer;
    private int noofcardtodeal;
    private long declaretime;
    private int dealbetweentime;
    private int prizetype;
    private long tosstimeout;
    private int cardperpureseq;
    private int missedmovesdrop;
    private int firstdropcfp;
    private int printedjokersperdeck;
    private int observerchat;
    private int settlementtype;
    private int minpointvalue21cardcfp;
    private int maxpointvalue21cardcfp;
    private long autoplaytimeout;
    private int timebtwround;
    private int noofdubleestodeclare21cardcfp;
    private long sitouttimeout;
    private int safescoreonplayerdeclaration21cardcfp;
    private boolean autoemail;
    private int jokertype;
    private long extratimechunk;
    private int middledropcfp;
    private int noofjokerstodeclare21cardcfp;
    private int noofjokerstobesafe21cardcfp;

    public GProperty() {
        super(1048681, 0, 0);
    }

    public GProperty(long receiverID, long playerID, int movetime,int noofdeck,int fullcountcfp,int maxbuyinmultiplier21cardcfp,int noofdubleestobesafe21cardcfp,int maxplayer,int blockedwords,long extratime,int nooftunnelastodeclare21cardcfp,int rptplmobserver,long mvtimefirstplayer,int noofcardtodeal,long declaretime,int dealbetweentime,int prizetype,long tosstimeout,int cardperpureseq,int missedmovesdrop,int firstdropcfp,int printedjokersperdeck,int observerchat,int settlementtype,int minpointvalue21cardcfp,int maxpointvalue21cardcfp,long autoplaytimeout,int timebtwround,int noofdubleestodeclare21cardcfp,long sitouttimeout,int safescoreonplayerdeclaration21cardcfp,boolean autoemail,int jokertype,long extratimechunk,int middledropcfp,int noofjokerstodeclare21cardcfp,int noofjokerstobesafe21cardcfp) {
        super(1048681, receiverID, playerID);
        this.movetime = movetime;
        this.noofdeck = noofdeck;
        this.fullcountcfp = fullcountcfp;
        this.maxbuyinmultiplier21cardcfp = maxbuyinmultiplier21cardcfp;
        this.noofdubleestobesafe21cardcfp = noofdubleestobesafe21cardcfp;
        this.maxplayer = maxplayer;
        this.blockedwords = blockedwords;
        this.extratime = extratime;
        this.nooftunnelastodeclare21cardcfp = nooftunnelastodeclare21cardcfp;
        this.rptplmobserver = rptplmobserver;
        this.mvtimefirstplayer = mvtimefirstplayer;
        this.noofcardtodeal = noofcardtodeal;
        this.declaretime = declaretime;
        this.dealbetweentime = dealbetweentime;
        this.prizetype = prizetype;
        this.tosstimeout = tosstimeout;
        this.cardperpureseq = cardperpureseq;
        this.missedmovesdrop = missedmovesdrop;
        this.firstdropcfp = firstdropcfp;
        this.printedjokersperdeck = printedjokersperdeck;
        this.observerchat = observerchat;
        this.settlementtype = settlementtype;
        this.minpointvalue21cardcfp = minpointvalue21cardcfp;
        this.maxpointvalue21cardcfp = maxpointvalue21cardcfp;
        this.autoplaytimeout = autoplaytimeout;
        this.timebtwround = timebtwround;
        this.noofdubleestodeclare21cardcfp = noofdubleestodeclare21cardcfp;
        this.sitouttimeout = sitouttimeout;
        this.safescoreonplayerdeclaration21cardcfp = safescoreonplayerdeclaration21cardcfp;
        this.autoemail = autoemail;
        this.jokertype = jokertype;
        this.extratimechunk = extratimechunk;
        this.middledropcfp = middledropcfp;
        this.noofjokerstodeclare21cardcfp = noofjokerstodeclare21cardcfp;
        this.noofjokerstobesafe21cardcfp = noofjokerstobesafe21cardcfp;
    }




    public int getMovetime() {
        return movetime;
    }

    public void setMovetime(int movetime) {
        this.movetime = movetime;
    }

    public int getNoofdeck() {
        return noofdeck;
    }

    public void setNoofdeck(int noofdeck) {
        this.noofdeck = noofdeck;
    }

    public int getFullcountcfp() {
        return fullcountcfp;
    }

    public void setFullcountcfp(int fullcountcfp) {
        this.fullcountcfp = fullcountcfp;
    }

    public int getMaxbuyinmultiplier21cardcfp() {
        return maxbuyinmultiplier21cardcfp;
    }

    public void setMaxbuyinmultiplier21cardcfp(int maxbuyinmultiplier21cardcfp) {
        this.maxbuyinmultiplier21cardcfp = maxbuyinmultiplier21cardcfp;
    }

    public int getNoofdubleestobesafe21cardcfp() {
        return noofdubleestobesafe21cardcfp;
    }

    public void setNoofdubleestobesafe21cardcfp(int noofdubleestobesafe21cardcfp) {
        this.noofdubleestobesafe21cardcfp = noofdubleestobesafe21cardcfp;
    }

    public int getMaxplayer() {
        return maxplayer;
    }

    public void setMaxplayer(int maxplayer) {
        this.maxplayer = maxplayer;
    }

    public int getBlockedwords() {
        return blockedwords;
    }

    public void setBlockedwords(int blockedwords) {
        this.blockedwords = blockedwords;
    }

    public long getExtratime() {
        return extratime;
    }

    public void setExtratime(long extratime) {
        this.extratime = extratime;
    }

    public int getNooftunnelastodeclare21cardcfp() {
        return nooftunnelastodeclare21cardcfp;
    }

    public void setNooftunnelastodeclare21cardcfp(int nooftunnelastodeclare21cardcfp) {
        this.nooftunnelastodeclare21cardcfp = nooftunnelastodeclare21cardcfp;
    }

    public int getRptplmobserver() {
        return rptplmobserver;
    }

    public void setRptplmobserver(int rptplmobserver) {
        this.rptplmobserver = rptplmobserver;
    }

    public long getMvtimefirstplayer() {
        return mvtimefirstplayer;
    }

    public void setMvtimefirstplayer(long mvtimefirstplayer) {
        this.mvtimefirstplayer = mvtimefirstplayer;
    }

    public int getNoofcardtodeal() {
        return noofcardtodeal;
    }

    public void setNoofcardtodeal(int noofcardtodeal) {
        this.noofcardtodeal = noofcardtodeal;
    }

    public long getDeclaretime() {
        return declaretime;
    }

    public void setDeclaretime(long declaretime) {
        this.declaretime = declaretime;
    }

    public int getDealbetweentime() {
        return dealbetweentime;
    }

    public void setDealbetweentime(int dealbetweentime) {
        this.dealbetweentime = dealbetweentime;
    }

    public int getPrizetype() {
        return prizetype;
    }

    public void setPrizetype(int prizetype) {
        this.prizetype = prizetype;
    }

    public long getTosstimeout() {
        return tosstimeout;
    }

    public void setTosstimeout(long tosstimeout) {
        this.tosstimeout = tosstimeout;
    }

    public int getCardperpureseq() {
        return cardperpureseq;
    }

    public void setCardperpureseq(int cardperpureseq) {
        this.cardperpureseq = cardperpureseq;
    }

    public int getMissedmovesdrop() {
        return missedmovesdrop;
    }

    public void setMissedmovesdrop(int missedmovesdrop) {
        this.missedmovesdrop = missedmovesdrop;
    }

    public int getFirstdropcfp() {
        return firstdropcfp;
    }

    public void setFirstdropcfp(int firstdropcfp) {
        this.firstdropcfp = firstdropcfp;
    }

    public int getPrintedjokersperdeck() {
        return printedjokersperdeck;
    }

    public void setPrintedjokersperdeck(int printedjokersperdeck) {
        this.printedjokersperdeck = printedjokersperdeck;
    }

    public int getObserverchat() {
        return observerchat;
    }

    public void setObserverchat(int observerchat) {
        this.observerchat = observerchat;
    }

    public int getSettlementtype() {
        return settlementtype;
    }

    public void setSettlementtype(int settlementtype) {
        this.settlementtype = settlementtype;
    }

    public int getMinpointvalue21cardcfp() {
        return minpointvalue21cardcfp;
    }

    public void setMinpointvalue21cardcfp(int minpointvalue21cardcfp) {
        this.minpointvalue21cardcfp = minpointvalue21cardcfp;
    }

    public int getMaxpointvalue21cardcfp() {
        return maxpointvalue21cardcfp;
    }

    public void setMaxpointvalue21cardcfp(int maxpointvalue21cardcfp) {
        this.maxpointvalue21cardcfp = maxpointvalue21cardcfp;
    }

    public long getAutoplaytimeout() {
        return autoplaytimeout;
    }

    public void setAutoplaytimeout(long autoplaytimeout) {
        this.autoplaytimeout = autoplaytimeout;
    }

    public int getTimebtwround() {
        return timebtwround;
    }

    public void setTimebtwround(int timebtwround) {
        this.timebtwround = timebtwround;
    }

    public int getNoofdubleestodeclare21cardcfp() {
        return noofdubleestodeclare21cardcfp;
    }

    public void setNoofdubleestodeclare21cardcfp(int noofdubleestodeclare21cardcfp) {
        this.noofdubleestodeclare21cardcfp = noofdubleestodeclare21cardcfp;
    }

    public long getSitouttimeout() {
        return sitouttimeout;
    }

    public void setSitouttimeout(long sitouttimeout) {
        this.sitouttimeout = sitouttimeout;
    }

    public int getSafescoreonplayerdeclaration21cardcfp() {
        return safescoreonplayerdeclaration21cardcfp;
    }

    public void setSafescoreonplayerdeclaration21cardcfp(int safescoreonplayerdeclaration21cardcfp) {
        this.safescoreonplayerdeclaration21cardcfp = safescoreonplayerdeclaration21cardcfp;
    }

    public boolean isAutoemail() {
        return autoemail;
    }

    public void setAutoemail(boolean autoemail) {
        this.autoemail = autoemail;
    }

    public int getJokertype() {
        return jokertype;
    }

    public void setJokertype(int jokertype) {
        this.jokertype = jokertype;
    }

    public long getExtratimechunk() {
        return extratimechunk;
    }

    public void setExtratimechunk(long extratimechunk) {
        this.extratimechunk = extratimechunk;
    }

    public int getMiddledropcfp() {
        return middledropcfp;
    }

    public void setMiddledropcfp(int middledropcfp) {
        this.middledropcfp = middledropcfp;
    }

    public int getNoofjokerstodeclare21cardcfp() {
        return noofjokerstodeclare21cardcfp;
    }

    public void setNoofjokerstodeclare21cardcfp(int noofjokerstodeclare21cardcfp) {
        this.noofjokerstodeclare21cardcfp = noofjokerstodeclare21cardcfp;
    }

    public int getNoofjokerstobesafe21cardcfp() {
        return noofjokerstobesafe21cardcfp;
    }

    public void setNoofjokerstobesafe21cardcfp(int noofjokerstobesafe21cardcfp) {
        this.noofjokerstobesafe21cardcfp = noofjokerstobesafe21cardcfp;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"GProperty{" +
            "movetime=" + movetime +
            "noofdeck=" + noofdeck +
            "fullcountcfp=" + fullcountcfp +
            "maxbuyinmultiplier21cardcfp=" + maxbuyinmultiplier21cardcfp +
            "noofdubleestobesafe21cardcfp=" + noofdubleestobesafe21cardcfp +
            "maxplayer=" + maxplayer +
            "blockedwords=" + blockedwords +
            "extratime=" + extratime +
            "nooftunnelastodeclare21cardcfp=" + nooftunnelastodeclare21cardcfp +
            "rptplmobserver=" + rptplmobserver +
            "mvtimefirstplayer=" + mvtimefirstplayer +
            "noofcardtodeal=" + noofcardtodeal +
            "declaretime=" + declaretime +
            "dealbetweentime=" + dealbetweentime +
            "prizetype=" + prizetype +
            "tosstimeout=" + tosstimeout +
            "cardperpureseq=" + cardperpureseq +
            "missedmovesdrop=" + missedmovesdrop +
            "firstdropcfp=" + firstdropcfp +
            "printedjokersperdeck=" + printedjokersperdeck +
            "observerchat=" + observerchat +
            "settlementtype=" + settlementtype +
            "minpointvalue21cardcfp=" + minpointvalue21cardcfp +
            "maxpointvalue21cardcfp=" + maxpointvalue21cardcfp +
            "autoplaytimeout=" + autoplaytimeout +
            "timebtwround=" + timebtwround +
            "noofdubleestodeclare21cardcfp=" + noofdubleestodeclare21cardcfp +
            "sitouttimeout=" + sitouttimeout +
            "safescoreonplayerdeclaration21cardcfp=" + safescoreonplayerdeclaration21cardcfp +
            "autoemail=" + autoemail +
            "jokertype=" + jokertype +
            "extratimechunk=" + extratimechunk +
            "middledropcfp=" + middledropcfp +
            "noofjokerstodeclare21cardcfp=" + noofjokerstodeclare21cardcfp +
            "noofjokerstobesafe21cardcfp=" + noofjokerstobesafe21cardcfp +
        "}";
    }
}
