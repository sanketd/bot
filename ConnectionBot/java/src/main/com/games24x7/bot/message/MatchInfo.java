package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MatchInfo extends AbstractMessage { 
 
    private List<MatchSeatArrayObj> matchseataray;
    private int dealno;
    private int meport;
    private long matchid;
    private String meip;
    private long mpid;

    public MatchInfo() {
        super(1048726, 0, 0);
    }

    public MatchInfo(long receiverID, long playerID, List<MatchSeatArrayObj> matchseataray,int dealno,int meport,long matchid,String meip,long mpid) {
        super(1048726, receiverID, playerID);
        this.matchseataray = matchseataray;
        this.dealno = dealno;
        this.meport = meport;
        this.matchid = matchid;
        this.meip = meip;
        this.mpid = mpid;
    }




    public List<MatchSeatArrayObj> getMatchseataray() {
        return matchseataray;
    }

    public void setMatchseataray(List<MatchSeatArrayObj> matchseataray) {
        this.matchseataray = matchseataray;
    }

    public int getDealno() {
        return dealno;
    }

    public void setDealno(int dealno) {
        this.dealno = dealno;
    }

    public int getMeport() {
        return meport;
    }

    public void setMeport(int meport) {
        this.meport = meport;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public String getMeip() {
        return meip;
    }

    public void setMeip(String meip) {
        this.meip = meip;
    }

    public long getMpid() {
        return mpid;
    }

    public void setMpid(long mpid) {
        this.mpid = mpid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MatchInfo{" +
            "matchseataray=" + matchseataray +
            "dealno=" + dealno +
            "meport=" + meport +
            "matchid=" + matchid +
            "meip=" + meip +
            "mpid=" + mpid +
        "}";
    }
}
