package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class HandProgressBarState extends AbstractMessage { 
 
    private boolean progressbarmax;

    public HandProgressBarState() {
        super(1048594, 0, 0);
    }

    public HandProgressBarState(long receiverID, long playerID, boolean progressbarmax) {
        super(1048594, receiverID, playerID);
        this.progressbarmax = progressbarmax;
    }




    public boolean isProgressbarmax() {
        return progressbarmax;
    }

    public void setProgressbarmax(boolean progressbarmax) {
        this.progressbarmax = progressbarmax;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"HandProgressBarState{" +
            "progressbarmax=" + progressbarmax +
        "}";
    }
}
