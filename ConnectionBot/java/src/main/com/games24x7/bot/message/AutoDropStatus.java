package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoDropStatus extends AbstractMessage { 
 
    private long matchid;
    private long mid;
    private long gameid;
    private boolean autodrop;

    public AutoDropStatus() {
        super(1048689, 0, 0);
    }

    public AutoDropStatus(long receiverID, long playerID, long matchid,long mid,long gameid,boolean autodrop) {
        super(1048689, receiverID, playerID);
        this.matchid = matchid;
        this.mid = mid;
        this.gameid = gameid;
        this.autodrop = autodrop;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getMid() {
        return mid;
    }

    public void setMid(long mid) {
        this.mid = mid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public boolean isAutodrop() {
        return autodrop;
    }

    public void setAutodrop(boolean autodrop) {
        this.autodrop = autodrop;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoDropStatus{" +
            "matchid=" + matchid +
            "mid=" + mid +
            "gameid=" + gameid +
            "autodrop=" + autodrop +
        "}";
    }
}
