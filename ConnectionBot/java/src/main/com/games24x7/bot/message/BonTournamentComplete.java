package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BonTournamentComplete extends AbstractMessage { 
 
    private long aaid;
    private List<String> winners;
    private List<Long> matchidlist;
    private long tournamentId;

    public BonTournamentComplete() {
        super(1048745, 0, 0);
    }

    public BonTournamentComplete(long receiverID, long playerID, long aaid,List<String> winners,List<Long> matchidlist,long tournamentId) {
        super(1048745, receiverID, playerID);
        this.aaid = aaid;
        this.winners = winners;
        this.matchidlist = matchidlist;
        this.tournamentId = tournamentId;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<String> getWinners() {
        return winners;
    }

    public void setWinners(List<String> winners) {
        this.winners = winners;
    }

    public List<Long> getMatchidlist() {
        return matchidlist;
    }

    public void setMatchidlist(List<Long> matchidlist) {
        this.matchidlist = matchidlist;
    }

    public long getTournamentId() {
        return tournamentId;
    }

    public void setTournamentId(long tournamentId) {
        this.tournamentId = tournamentId;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BonTournamentComplete{" +
            "aaid=" + aaid +
            "winners=" + winners +
            "matchidlist=" + matchidlist +
            "tournamentId=" + tournamentId +
        "}";
    }
}
