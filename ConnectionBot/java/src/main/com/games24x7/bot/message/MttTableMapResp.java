package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class MttTableMapResp extends AbstractMessage {
    private List<Long> mtttableid;
    private List<Long> mttplayerid;

    public MttTableMapResp() {
        super(2097158, 0, 0);
    }

    public MttTableMapResp(long receiverID, long playerID , List<Long> mtttableid, List<Long> mttplayerid) {
        super(2097158, receiverID, playerID);
        this.mtttableid = mtttableid;
        this.mttplayerid = mttplayerid;
    }

    public List<Long> getMtttableid() {
        return mtttableid;
    }

    public void setMtttableid(List<Long> mtttableid) {
        this.mtttableid = mtttableid;
    }

    public List<Long> getMttplayerid() {
        return mttplayerid;
    }

    public void setMttplayerid(List<Long> mttplayerid) {
        this.mttplayerid = mttplayerid;
    }

    @Override
    public String toString() {
        return "MttTableMapResp{" +
            "mtttableid=" + mtttableid +
            "mttplayerid=" + mttplayerid +
        "}";
    }
}
