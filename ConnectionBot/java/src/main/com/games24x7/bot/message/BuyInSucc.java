package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BuyInSucc extends AbstractMessage { 
 
    private long aaid;
    private String sessionid;
    private boolean rebuy;
    private boolean first21cardpracticegame;

    public BuyInSucc() {
        super(1048697, 0, 0);
    }

    public BuyInSucc(long receiverID, long playerID, long aaid,String sessionid,boolean rebuy,boolean first21cardpracticegame) {
        super(1048697, receiverID, playerID);
        this.aaid = aaid;
        this.sessionid = sessionid;
        this.rebuy = rebuy;
        this.first21cardpracticegame = first21cardpracticegame;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public String getSessionid() {
        return sessionid;
    }

    public void setSessionid(String sessionid) {
        this.sessionid = sessionid;
    }

    public boolean isRebuy() {
        return rebuy;
    }

    public void setRebuy(boolean rebuy) {
        this.rebuy = rebuy;
    }

    public boolean isFirst21cardpracticegame() {
        return first21cardpracticegame;
    }

    public void setFirst21cardpracticegame(boolean first21cardpracticegame) {
        this.first21cardpracticegame = first21cardpracticegame;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BuyInSucc{" +
            "aaid=" + aaid +
            "sessionid=" + sessionid +
            "rebuy=" + rebuy +
            "first21cardpracticegame=" + first21cardpracticegame +
        "}";
    }
}
