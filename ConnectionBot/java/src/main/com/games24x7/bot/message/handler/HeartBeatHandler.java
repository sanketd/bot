/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.HeartBeatResponse;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class HeartBeatHandler implements BotMessageHandler< HeartBeatResponse >
{
	@Override
	public void handleMessage( UserSession session, HeartBeatResponse message, PlayerInfo playerInfo )
	{
		//System.out.println("HeartBeat Response");
	}

}
