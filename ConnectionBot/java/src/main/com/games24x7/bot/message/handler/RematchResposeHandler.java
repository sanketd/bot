package com.games24x7.bot.message.handler;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.RematchResponse;
import com.games24x7.bot.message.Setup;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class RematchResposeHandler implements BotMessageHandler< RematchResponse >
{

	@Override
	public void handleMessage( UserSession session, RematchResponse message, PlayerInfo plrInfo )
	{
		if( message.getRematchMessageType() == 3 )
		{
			int channel = BotImpl.getInstance().getConfiguration().getIntValue( "CHANNEL_ID" );
			System.out.println( "Rematch Response received" );
			System.out.println( "Channel Id from setup request--->>> " + channel );
			plrInfo.setTimer( null );
			Setup setup = new Setup( message.getReceiverID(), plrInfo.getPlayerId(), session.getSessionKey(), null, channel );
			System.out.println( "Player INFO Channel" + plrInfo.getChannel() );
			System.out.println( "Player Channel" + channel );
			setup.setSource( String.valueOf( plrInfo.getPlayerId() ) );
			SendMsgOnChannel.send( setup, plrInfo.getChannel() );

		}
		
			
	}
}
