package com.games24x7.bot.message.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.FMGRequest;
import com.games24x7.bot.message.PlayerEliminated;
import com.games24x7.init.BotMain;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class PlayerEliminatedHandler implements BotMessageHandler<PlayerEliminated> {

	private Logger logger = LoggerFactory.getLogger(PlayerEliminatedHandler.class);
	
	@Override
	public void handleMessage(UserSession session, PlayerEliminated message,PlayerInfo plrInfo) 
	{
		
		if (BotImpl.getInstance().getConfiguration().getBooleanValue("PLAYNOW")) 
		{
			
						
					long playerId = plrInfo.getPlayerId();
					System.out.println( "Completed creating new game from Player Eliminated " + playerId );
					BotMain.executeInfiniteGame( playerId );
				
			
			
			/*
			logger.debug("Player Eliminated Handler " + message);
			try {
				
				//TableIDVSTimer.get( message.getPlayerID() ).cancel();
				int players = BotImpl.getInstance().getConfiguration().getIntValue("PLAYERS_ON_A_TABLE");
				int tableid = BotImpl.getInstance().getConfiguration().getIntValue("STARTING_TABLE_ID");
				int nooftables = BotImpl.getInstance().getConfiguration().getIntValue("NO_OF_TOURNAMENT");
				boolean playnow = BotImpl.getInstance().getConfiguration().getBooleanValue("PLAYNOW");
				int gameType = BotImpl.getInstance().getConfiguration().getIntValue("SETTLEMENT_TYPE");
				int poolVariant = BotImpl.getInstance().getConfiguration().getIntValue("GAME_VARIANT");
				int amt = BotImpl.getInstance().getConfiguration().getIntValue("FMG_AMOUNT");
				int deck = BotImpl.getInstance().getConfiguration().getIntValue("NO_OF_DECK");
				logger.debug("Amount won message received");
				logger.debug("Player id 1====" + message.getPlayerID());
				int gameFormat = 0;
				if (players == 6 && playnow) {
					if (gameType == 3)
						gameFormat = 306;

					else if (gameType == 2 && poolVariant == 101)
						gameFormat = 201;

					else if (gameType == 2 && poolVariant == 201)
						gameFormat = 202;

				} else if (players == 2 && playnow) {
					if (gameType == 2 && poolVariant == 101)
						gameFormat = 201;

					else if (gameType == 2 && poolVariant == 201)
						gameFormat = 201;
				}
				logger.debug("Game format is in PE " + gameFormat);
				// FMGRequest fmgRequest = new FMGRequest( 0,
				// plrInfo.getPlayerId(), 0l, 0l, 2, gameFormat, players, 0d,
				// amt, 0, "", plrInfo.getTableId(), false, false ,false );
				FMGRequest fmgRequest = new FMGRequest(0,plrInfo.getPlayerId(), 0l, 0l, 2, gameFormat, players,	0d, amt, 1, "", 0, false, true, false);
				fmgRequest.setDest("TE");
				fmgRequest.setSessionId(session.getSessionKey());
				logger.debug("Reciver id----->" + message.getReceiverID());
				logger.debug("Playerid==============="+ plrInfo.getPlayerId());
				logger.debug("Game Format++++++++++++++++++++++++++++++"+ gameFormat);
				logger.debug("Players++-++++++-++++++++--------"+ players);
				logger.debug("Message for Playnow On game table POOL/BON---------> "+ fmgRequest);
				SendMsgOnChannel.send(fmgRequest, plrInfo.getChannel());
				logger.debug("Playnow Sent message ");

			} 
			catch (Exception e) 
			{
				e.printStackTrace();
			}

		*/
		/*else
		{
			logger.debug("************************Player eliminate Handler***************************** player getting disconnected");
			//TableIDVSTimer.get(plrInfo.getChannel()).cancel();
			plrInfo.getChannel().close();
			
		}*/
		
		}
	}
}
