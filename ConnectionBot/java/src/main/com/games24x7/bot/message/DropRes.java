package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class DropRes extends AbstractMessage { 
 
    private String message;
    private int point;
    private int result;
    private long matchid;
    private long gameid;
    private int mdrop;

    public DropRes() {
        super(1048710, 0, 0);
    }

    public DropRes(long receiverID, long playerID, String message,int point,int result,long matchid,long gameid,int mdrop) {
        super(1048710, receiverID, playerID);
        this.message = message;
        this.point = point;
        this.result = result;
        this.matchid = matchid;
        this.gameid = gameid;
        this.mdrop = mdrop;
    }




    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getMdrop() {
        return mdrop;
    }

    public void setMdrop(int mdrop) {
        this.mdrop = mdrop;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"DropRes{" +
            "message=" + message +
            "point=" + point +
            "result=" + result +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "mdrop=" + mdrop +
        "}";
    }
}
