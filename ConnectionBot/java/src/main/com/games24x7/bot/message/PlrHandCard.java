package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class PlrHandCard extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private List<List<String>> cards;
    private int topgroupscount;

    public PlrHandCard() {
        super(1048635, 0, 0);
    }

    public PlrHandCard(long receiverID, long playerID, long matchid,long gameid,List<List<String>> cards,int topgroupscount) {
        super(1048635, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.cards = cards;
        this.topgroupscount = topgroupscount;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public List<List<String>> getCards() {
        return cards;
    }

    public void setCards(List<List<String>> cards) {
        this.cards = cards;
    }

    public int getTopgroupscount() {
        return topgroupscount;
    }

    public void setTopgroupscount(int topgroupscount) {
        this.topgroupscount = topgroupscount;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"PlrHandCard{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "cards=" + cards +
            "topgroupscount=" + topgroupscount +
        "}";
    }
}
