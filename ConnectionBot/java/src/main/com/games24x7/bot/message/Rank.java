package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Rank extends AbstractMessage { 
 
    private int rank;
    private int status;
    private double cashprize;
    private boolean hasticket;
    private int ticketid;
    private String ticketname;

    public Rank() {
        super(1048673, 0, 0);
    }

    public Rank(long receiverID, long playerID, int rank,int status,double cashprize,boolean hasticket,int ticketid,String ticketname) {
        super(1048673, receiverID, playerID);
        this.rank = rank;
        this.status = status;
        this.cashprize = cashprize;
        this.hasticket = hasticket;
        this.ticketid = ticketid;
        this.ticketname = ticketname;
    }




    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public double getCashprize() {
        return cashprize;
    }

    public void setCashprize(double cashprize) {
        this.cashprize = cashprize;
    }

    public boolean isHasticket() {
        return hasticket;
    }

    public void setHasticket(boolean hasticket) {
        this.hasticket = hasticket;
    }

    public int getTicketid() {
        return ticketid;
    }

    public void setTicketid(int ticketid) {
        this.ticketid = ticketid;
    }

    public String getTicketname() {
        return ticketname;
    }

    public void setTicketname(String ticketname) {
        this.ticketname = ticketname;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Rank{" +
            "rank=" + rank +
            "status=" + status +
            "cashprize=" + cashprize +
            "hasticket=" + hasticket +
            "ticketid=" + ticketid +
            "ticketname=" + ticketname +
        "}";
    }
}
