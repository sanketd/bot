package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ValCardStr extends ChildAbstractMessage{
 
    private List<String> ujokers;
    private List<String> ljokers;
    private List<List<String>> marraige;
    private List<String> jokers;

    public ValCardStr() {
    super(1048743);
    }

    public ValCardStr( List<String> ujokers,List<String> ljokers,List<List<String>> marraige,List<String> jokers) {
         super(1048743);
        this.ujokers = ujokers;
        this.ljokers = ljokers;
        this.marraige = marraige;
        this.jokers = jokers;
    }




    public List<String> getUjokers() {
        return ujokers;
    }

    public void setUjokers(List<String> ujokers) {
        this.ujokers = ujokers;
    }

    public List<String> getLjokers() {
        return ljokers;
    }

    public void setLjokers(List<String> ljokers) {
        this.ljokers = ljokers;
    }

    public List<List<String>> getMarraige() {
        return marraige;
    }

    public void setMarraige(List<List<String>> marraige) {
        this.marraige = marraige;
    }

    public List<String> getJokers() {
        return jokers;
    }

    public void setJokers(List<String> jokers) {
        this.jokers = jokers;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ValCardStr{" +
            "ujokers=" + ujokers +
            "ljokers=" + ljokers +
            "marraige=" + marraige +
            "jokers=" + jokers +
        "}";
    }
}
