package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class FMGSearch extends AbstractMessage { 
 
    private String fmgparams;
    private String fmg;
    private int lastte;
    private String pptip;
    private int clientvariant;
    private long oldtid;

    public FMGSearch() {
        super(1048593, 0, 0);
    }

    public FMGSearch(long receiverID, long playerID, String fmgparams,String fmg,int lastte,String pptip,int clientvariant,long oldtid) {
        super(1048593, receiverID, playerID);
        this.fmgparams = fmgparams;
        this.fmg = fmg;
        this.lastte = lastte;
        this.pptip = pptip;
        this.clientvariant = clientvariant;
        this.oldtid = oldtid;
    }




    public String getFmgparams() {
        return fmgparams;
    }

    public void setFmgparams(String fmgparams) {
        this.fmgparams = fmgparams;
    }

    public String getFmg() {
        return fmg;
    }

    public void setFmg(String fmg) {
        this.fmg = fmg;
    }

    public int getLastte() {
        return lastte;
    }

    public void setLastte(int lastte) {
        this.lastte = lastte;
    }

    public String getPptip() {
        return pptip;
    }

    public void setPptip(String pptip) {
        this.pptip = pptip;
    }

    public int getClientvariant() {
        return clientvariant;
    }

    public void setClientvariant(int clientvariant) {
        this.clientvariant = clientvariant;
    }

    public long getOldtid() {
        return oldtid;
    }

    public void setOldtid(long oldtid) {
        this.oldtid = oldtid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"FMGSearch{" +
            "fmgparams=" + fmgparams +
            "fmg=" + fmg +
            "lastte=" + lastte +
            "pptip=" + pptip +
            "clientvariant=" + clientvariant +
            "oldtid=" + oldtid +
        "}";
    }
}
