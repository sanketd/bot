package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ShowRejoin extends AbstractMessage { 
 
    private String title;
    private double bal;
    private long remainingtimeforrejoin;
    private int rejoinscore;
    private long aaid;

    public ShowRejoin() {
        super(1048654, 0, 0);
    }

    public ShowRejoin(long receiverID, long playerID, String title,double bal,long remainingtimeforrejoin,int rejoinscore,long aaid) {
        super(1048654, receiverID, playerID);
        this.title = title;
        this.bal = bal;
        this.remainingtimeforrejoin = remainingtimeforrejoin;
        this.rejoinscore = rejoinscore;
        this.aaid = aaid;
    }




    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    public long getRemainingtimeforrejoin() {
        return remainingtimeforrejoin;
    }

    public void setRemainingtimeforrejoin(long remainingtimeforrejoin) {
        this.remainingtimeforrejoin = remainingtimeforrejoin;
    }

    public int getRejoinscore() {
        return rejoinscore;
    }

    public void setRejoinscore(int rejoinscore) {
        this.rejoinscore = rejoinscore;
    }

    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ShowRejoin{" +
            "title=" + title +
            "bal=" + bal +
            "remainingtimeforrejoin=" + remainingtimeforrejoin +
            "rejoinscore=" + rejoinscore +
            "aaid=" + aaid +
        "}";
    }
}
