package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class MatchCreated extends AbstractMessage { 
 
    private long aaid;

    public MatchCreated() {
        super(1048748, 0, 0);
    }

    public MatchCreated(long receiverID, long playerID, long aaid) {
        super(1048748, receiverID, playerID);
        this.aaid = aaid;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"MatchCreated{" +
            "aaid=" + aaid +
        "}";
    }
}
