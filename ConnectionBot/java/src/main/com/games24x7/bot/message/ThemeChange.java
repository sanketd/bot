package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ThemeChange extends AbstractMessage { 
 
    private List<Theme> theme;

    public ThemeChange() {
        super(1048662, 0, 0);
    }

    public ThemeChange(long receiverID, long playerID, List<Theme> theme) {
        super(1048662, receiverID, playerID);
        this.theme = theme;
    }




    public List<Theme> getTheme() {
        return theme;
    }

    public void setTheme(List<Theme> theme) {
        this.theme = theme;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ThemeChange{" +
            "theme=" + theme +
        "}";
    }
}
