package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ScoreCardResp extends AbstractMessage { 
 
    private long aaid;
    private long matchid;
    private List<ScoreListResp> scorelist;

    public ScoreCardResp() {
        super(1048646, 0, 0);
    }

    public ScoreCardResp(long receiverID, long playerID, long aaid,long matchid,List<ScoreListResp> scorelist) {
        super(1048646, receiverID, playerID);
        this.aaid = aaid;
        this.matchid = matchid;
        this.scorelist = scorelist;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public List<ScoreListResp> getScorelist() {
        return scorelist;
    }

    public void setScorelist(List<ScoreListResp> scorelist) {
        this.scorelist = scorelist;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreCardResp{" +
            "aaid=" + aaid +
            "matchid=" + matchid +
            "scorelist=" + scorelist +
        "}";
    }
}
