package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.SeatSetup;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

public class SeatSetupHandler implements BotMessageHandler< SeatSetup >
{

	@Override
	public void handleMessage( UserSession session, SeatSetup message, PlayerInfo playerInfo )
	{
		long aaid = message.getAaid();
		long playerId = playerInfo.getPlayerId();
		String sessionId = session.getSessionKey();
		System.out.println("Seat Setup Msg "+aaid+" "+playerId+" "+sessionId+" ");

	}

}
