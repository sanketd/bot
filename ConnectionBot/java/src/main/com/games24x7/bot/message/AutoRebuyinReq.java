package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoRebuyinReq extends AbstractMessage { 
 
    private boolean autorebuyinenabled;
    private long tournamentid;
    private boolean autorebuyindonotshow;

    public AutoRebuyinReq() {
        super(1048757, 0, 0);
    }

    public AutoRebuyinReq(long receiverID, long playerID, boolean autorebuyinenabled,long tournamentid,boolean autorebuyindonotshow) {
        super(1048757, receiverID, playerID);
        this.autorebuyinenabled = autorebuyinenabled;
        this.tournamentid = tournamentid;
        this.autorebuyindonotshow = autorebuyindonotshow;
    }




    public boolean isAutorebuyinenabled() {
        return autorebuyinenabled;
    }

    public void setAutorebuyinenabled(boolean autorebuyinenabled) {
        this.autorebuyinenabled = autorebuyinenabled;
    }

    public long getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(long tournamentid) {
        this.tournamentid = tournamentid;
    }

    public boolean isAutorebuyindonotshow() {
        return autorebuyindonotshow;
    }

    public void setAutorebuyindonotshow(boolean autorebuyindonotshow) {
        this.autorebuyindonotshow = autorebuyindonotshow;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoRebuyinReq{" +
            "autorebuyinenabled=" + autorebuyinenabled +
            "tournamentid=" + tournamentid +
            "autorebuyindonotshow=" + autorebuyindonotshow +
        "}";
    }
}
