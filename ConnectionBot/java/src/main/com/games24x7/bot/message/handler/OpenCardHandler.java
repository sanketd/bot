/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.OpnCard;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class OpenCardHandler implements BotMessageHandler< OpnCard >
{
	@Override
	public void handleMessage( UserSession session, OpnCard message, PlayerInfo playerInfo )
	{
		playerInfo.setOpenCard( message.getCardid() );
		System.out.println("Open Card "+message.getCardid() );
	}

}
