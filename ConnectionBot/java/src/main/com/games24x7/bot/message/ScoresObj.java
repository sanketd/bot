package com.games24x7.bot.message;
import com.games24x7.service.message.ChildAbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

 	public class ScoresObj extends ChildAbstractMessage{
 
    private int rank;
    private String username;
    private double chips;

    public ScoresObj() {
    super(1048753);
    }

    public ScoresObj( int rank,String username,double chips) {
         super(1048753);
        this.rank = rank;
        this.username = username;
        this.chips = chips;
    }




    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getChips() {
        return chips;
    }

    public void setChips(double chips) {
        this.chips = chips;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoresObj{" +
            "rank=" + rank +
            "username=" + username +
            "chips=" + chips +
        "}";
    }
}
