package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TakeSeatFail extends AbstractMessage { 
 
    private String msg;

    public TakeSeatFail() {
        super(1048738, 0, 0);
    }

    public TakeSeatFail(long receiverID, long playerID, String msg) {
        super(1048738, receiverID, playerID);
        this.msg = msg;
    }




    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TakeSeatFail{" +
            "msg=" + msg +
        "}";
    }
}
