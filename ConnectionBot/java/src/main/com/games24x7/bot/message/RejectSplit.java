package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RejectSplit extends AbstractMessage { 
 

    public RejectSplit() {
        super(1048606, 0, 0);
    }

    public RejectSplit(long receiverID, long playerID) {
        super(1048606, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RejectSplit{" +
        "}";
    }
}
