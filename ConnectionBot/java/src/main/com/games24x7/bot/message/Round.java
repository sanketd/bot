package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Round extends AbstractMessage { 
 
    private int qualifiers;
    private int roundnum;
    private String name;
    private int prizes;
    private List<Rank> ranks;

    public Round() {
        super(1048674, 0, 0);
    }

    public Round(long receiverID, long playerID, int qualifiers,int roundnum,String name,int prizes,List<Rank> ranks) {
        super(1048674, receiverID, playerID);
        this.qualifiers = qualifiers;
        this.roundnum = roundnum;
        this.name = name;
        this.prizes = prizes;
        this.ranks = ranks;
    }




    public int getQualifiers() {
        return qualifiers;
    }

    public void setQualifiers(int qualifiers) {
        this.qualifiers = qualifiers;
    }

    public int getRoundnum() {
        return roundnum;
    }

    public void setRoundnum(int roundnum) {
        this.roundnum = roundnum;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrizes() {
        return prizes;
    }

    public void setPrizes(int prizes) {
        this.prizes = prizes;
    }

    public List<Rank> getRanks() {
        return ranks;
    }

    public void setRanks(List<Rank> ranks) {
        this.ranks = ranks;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Round{" +
            "qualifiers=" + qualifiers +
            "roundnum=" + roundnum +
            "name=" + name +
            "prizes=" + prizes +
            "ranks=" + ranks +
        "}";
    }
}
