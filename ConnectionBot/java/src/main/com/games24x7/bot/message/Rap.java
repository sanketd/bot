package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Rap extends AbstractMessage { 
 
    private String category;
    private long matchid;
    private String ptext;
    private long gameid;
    private long tournamentid;
    private int settlementtype;

    public Rap() {
        super(1048602, 0, 0);
    }

    public Rap(long receiverID, long playerID, String category,long matchid,String ptext,long gameid,long tournamentid,int settlementtype) {
        super(1048602, receiverID, playerID);
        this.category = category;
        this.matchid = matchid;
        this.ptext = ptext;
        this.gameid = gameid;
        this.tournamentid = tournamentid;
        this.settlementtype = settlementtype;
    }




    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public String getPtext() {
        return ptext;
    }

    public void setPtext(String ptext) {
        this.ptext = ptext;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public long getTournamentid() {
        return tournamentid;
    }

    public void setTournamentid(long tournamentid) {
        this.tournamentid = tournamentid;
    }

    public int getSettlementtype() {
        return settlementtype;
    }

    public void setSettlementtype(int settlementtype) {
        this.settlementtype = settlementtype;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Rap{" +
            "category=" + category +
            "matchid=" + matchid +
            "ptext=" + ptext +
            "gameid=" + gameid +
            "tournamentid=" + tournamentid +
            "settlementtype=" + settlementtype +
        "}";
    }
}
