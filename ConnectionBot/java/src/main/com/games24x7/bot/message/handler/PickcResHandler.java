package com.games24x7.bot.message.handler;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.impl.BotImpl;
import com.games24x7.bot.message.BotMessageConstants;
import com.games24x7.bot.message.Discard;
import com.games24x7.bot.message.Finish;
import com.games24x7.bot.message.HandShake;
import com.games24x7.bot.message.HeartBeatResponse;
import com.games24x7.bot.message.PickcRes;
import com.games24x7.bot.message.Recon;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.HeartBeatWorkerThread;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.ChannelIDChannelInfoMap;
import com.games24x7.util.ChannelInfo;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public class PickcResHandler implements BotMessageHandler< PickcRes >
{
	String cardid = null;
	boolean alreadyDicon = false;
	private Channel channel;
	private ChannelInfo channelInfo;
	private UserSession us;
	private PlayerInfo plrInfo;
	//private ReceivedMsgQueue receivedMsgQueue = null;
	private boolean handshakeReceived = false;

	@Override
	public void handleMessage( UserSession session, PickcRes message, PlayerInfo playerInfo )
	{
		System.out.println( "Pick close deck card " + message.getCardid() );
		// System.out.println("DATA:-----------------"+playerInfo.getPlayerMoveCount()+":::::::"
		// + BotImpl.getInstance().getConfiguration().getIntValue(
		// "LIMITED_MOVE_COUNT" ));
		cardid = message.getCardid();
		if( BotImpl.getInstance().getConfiguration().getBooleanValue( "QUICK_FINISH" ) && BotImpl.getInstance().getConfiguration().getBooleanValue( "DISCON_QUICK_FINISH" ) )
		{
			if( !alreadyDicon )
			{
				System.out.println( "with discon" );

				/*
				 * try { //Thread.sleep( 15000 ); } catch(
				 * InterruptedException e ) { // TODO
				 * Auto-generated // catch block
				 * e.printStackTrace(); }
				 */
				processDisconnection( session, message, playerInfo );
			}

		}
		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "QUICK_FINISH" ) )
		{
			Message finish = new Finish( message.getReceiverID(), playerInfo.getPlayerId(), message.getCardid(), playerInfo.getMPlayerId(), message.getMatchid() );

			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "ONLYPICK" ) )
			{
				SendMsgOnChannel.send( finish, playerInfo.getChannel() );
			}

		}
		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "VALID_FINISH" ) )
		{
			// TODO add logic for valid finish
		}
		
		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "INFINITE_MOVE" ) )
		{
			Message discard = new Discard( message.getReceiverID(), playerInfo.getPlayerId(), message.getCardid(), playerInfo.getMPlayerId(), message.getMatchid() );
			System.out.println( message.getPlayerID() + " Discarding card " + message.getCardid() );
			System.out.println( "ONLY PICK" + BotImpl.getInstance().getConfiguration().getBooleanValue( "ONLYPICK" ) );
			if( BotImpl.getInstance().getConfiguration().getBooleanValue( "ONLYPICK" ) )
			{
				SendMsgOnChannel.send( discard, playerInfo.getChannel() );
			}
			else if( playerInfo.getPlayerMoveCount() <= BotImpl.getInstance().getConfiguration().getIntValue( "LIMITED_MOVE_COUNT" ) )
			{
				SendMsgOnChannel.send( discard, playerInfo.getChannel() );
			}
			else
			{
				playerInfo.setPlayerMoveCount( 0 );
				Message finish = new Finish( message.getReceiverID(), playerInfo.getPlayerId(), message.getCardid(), playerInfo.getMPlayerId(), message.getMatchid() );
				SendMsgOnChannel.send( finish, playerInfo.getChannel() );
			}
		}

		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MAKE_SPLIT_2_PLAYERS" ) )
		{

			System.out.println( "AUTO SPLIT IS SUCCESSSSSSSSooooooo------------------------" );
			Message finish = new Finish( message.getReceiverID(), playerInfo.getPlayerId(), message.getCardid(), playerInfo.getMPlayerId(), message.getMatchid() );
			SendMsgOnChannel.send( finish, playerInfo.getChannel() );

		}

		else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "MAKE_SPLIT_3_PLAYERS" ) )
		{

			//System.out.println( "AUTO Split for 3 players---#######----MAKE_SPLIT_3_PLAYERS" );
			Message finish = new Finish( message.getReceiverID(), playerInfo.getPlayerId(), message.getCardid(), playerInfo.getMPlayerId(), message.getMatchid() );
			SendMsgOnChannel.send( finish, playerInfo.getChannel() );

		}

	}

	public void processDisconnection( UserSession session, PickcRes message, PlayerInfo playerInfo )
	{/*
		this.channel = BotImpl.getInstance().createCSClientChannel( "54.254.167.219", 9443 );
		this.channelInfo = ChannelIDChannelInfoMap.createChannelIDAndAddInfoToMap( channel );
		// this.msgOnChannel=new SendMsgOnChannel();
		this.us = new UserSession( Long.toString( channelInfo.getChannelID() ), channelInfo.getSessionKey() );
		this.plrInfo = new PlayerInfo( playerInfo.getPlayerId(), channel );
		HandShake msg = new HandShake( message.getReceiverID(), playerInfo.getPlayerId(), "192.168.48.8" );
		us.setAccountID( Thread.currentThread().getId(), playerInfo.getPlayerId() );
		us.setAccountName( Thread.currentThread().getName() );
		System.out.println( "DisconnectHandler ---->>> " + channel );

		boolean sendSuccess = SendMsgOnChannel.send( msg, channel );
		if( sendSuccess )
		{
			try
			{
				Thread.sleep( 15000 );
				System.out.println( " DisconnectHandler Geting receiver queue" );
				receivedMsgQueue = ChannelIdVsReceiverQueue.get( channelInfo.getChannelID() );

				if( receivedMsgQueue != null )
				{
					// TODO receive msg
					// poll queue for msgs
					while( true )
					{
						// System.out.println("polling
						// queue "+receivedMsgQueue);
						Message message2 = receivedMsgQueue.get();
						if( message2 != null )
						{
							System.out.println( "DisconnectHandler Message2 " + message2.getClass().toString() );
							if( message2.getClassID() == BotMessageConstants.HANDSHAKERESPONSE )
							{
								handshakeReceived = true;
								//
								plrInfo.getTimer().schedule( new HeartBeatWorkerThread( channel, message2.getReceiverID(), message2.getPlayerID() ), 0, 2000 );
								// reconnection
								Recon recon = new Recon( message2.getReceiverID(), plrInfo.getPlayerId(), "", "", "", 1, plrInfo.getMatchId(), plrInfo.getTableId(),
										playerInfo.getMPlayerId() );
								SendMsgOnChannel.send( recon, channel );
							}
							else
							{
								if( handshakeReceived )
								{

									if( message2.getClassID() == BotMessageConstants.HEARTBEATRESPONSE )
									{
										HeartBeatResponse heartBeatResponse = ( HeartBeatResponse ) message2;
										// System.out.println("HeartBeatResponse
										// "+heartBeatResponse.toString());
										System.out.println( "DisconnectHandler HeartBeatResponse " + heartBeatResponse.toString() );
									}
									// Player
									// getting
									// disconnected
									// after
									// pick
									// card
									BotMessageHandler messageHandler = BotImpl.getInstance().getMsgHandler( message2.getClassID() );
									try
									{
										if( message2.getClassID() == 1048636 )
										{
											if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DISCON_INFINITE_MOVE" ) )
											{
												System.out.println( "DISCON_INFINITE_MOVE AFTER RECON" );
												// Discard
												// Message

											}
											else if( BotImpl.getInstance().getConfiguration().getBooleanValue( "DISCON_QUICK_FINISH" ) )
											{
												System.out.println(
														"<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<DISCON_QUICK_FINISH AFTER RECON>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" );
												Message finish = new Finish( message2.getReceiverID(), message2.getPlayerID(), cardid,
														playerInfo.getMPlayerId(), plrInfo.getMatchId() );
												SendMsgOnChannel.send( finish, playerInfo.getChannel() );

											}

										}
										else
										{
											messageHandler.handleMessage( us, message2, plrInfo );
										}

									}
									catch( NullPointerException e )
									{
										System.out.println( " DisconnectHandler INsdide null pointer exception -- " + message2 );
										System.out.println( " DisconnectHandler us" + us );
										System.out.println( " DisconnectHandler plrInfo" + plrInfo );
										System.out.println( " DisconnectHandler messageHandler" + messageHandler );
									}
								}
							}
						}
					}
				}
			}
			catch( Exception e )
			{
				e.printStackTrace();
			}
		}
	*/}

}
