package com.games24x7.bot.message.handler;

import java.util.TimerTask;

import org.jboss.netty.channel.Channel;

import com.games24x7.bot.message.Recon;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.message.Message;
import com.games24x7.util.PlayerInfo;
import com.games24x7.util.SendMsgOnChannel;

public  class ReconHandler extends TimerTask
{
	Channel channel=null;
	long receiverID;
	long playerID;
	String fmgparams;
	String sessionid;
	String pptip;
	int clientvariant;
	long matchid;
	long gameid;
	long mplayerid;
	
	public ReconHandler(Channel ch,long receiverID, long playerID, String fmgparams, String sessionid, String pptip, int clientvariant, long matchid, long gameid, long mplayerid)
	{
		this.channel=ch;
		this.receiverID=receiverID;
		this.playerID=playerID;
		this.fmgparams=fmgparams;
		this.sessionid=sessionid;
		this.pptip=pptip;
		this.clientvariant=clientvariant;
		this.matchid=matchid;
		this.gameid=gameid;
		this.mplayerid=mplayerid;
		
	}
	@Override
	public void run()
	{
		Message recon= new Recon( receiverID, playerID, fmgparams, sessionid, pptip, clientvariant, matchid, gameid, mplayerid );
		System.out.printf("Recon"+recon.toString(),""+receiverID,""+playerID,""+fmgparams,""+sessionid,""+pptip,""+clientvariant,""+matchid,""+ gameid, ""+mplayerid);
		SendMsgOnChannel.send( recon, channel );
		
		
	}
	
}
