package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BONBonusUpdateReq extends AbstractMessage { 
 

    public BONBonusUpdateReq() {
        super(1048581, 0, 0);
    }

    public BONBonusUpdateReq(long receiverID, long playerID) {
        super(1048581, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BONBonusUpdateReq{" +
        "}";
    }
}
