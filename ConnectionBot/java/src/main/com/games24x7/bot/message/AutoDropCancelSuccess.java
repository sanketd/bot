package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class AutoDropCancelSuccess extends AbstractMessage { 
 
    private long aaid;

    public AutoDropCancelSuccess() {
        super(1048688, 0, 0);
    }

    public AutoDropCancelSuccess(long receiverID, long playerID, long aaid) {
        super(1048688, receiverID, playerID);
        this.aaid = aaid;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"AutoDropCancelSuccess{" +
            "aaid=" + aaid +
        "}";
    }
}
