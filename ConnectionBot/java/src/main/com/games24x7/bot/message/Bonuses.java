package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class Bonuses extends AbstractMessage { 
 
    private String promocode;
    private String expirydate;
    private double maxamount;

    public Bonuses() {
        super(1048739, 0, 0);
    }

    public Bonuses(long receiverID, long playerID, String promocode,String expirydate,double maxamount) {
        super(1048739, receiverID, playerID);
        this.promocode = promocode;
        this.expirydate = expirydate;
        this.maxamount = maxamount;
    }




    public String getPromocode() {
        return promocode;
    }

    public void setPromocode(String promocode) {
        this.promocode = promocode;
    }

    public String getExpirydate() {
        return expirydate;
    }

    public void setExpirydate(String expirydate) {
        this.expirydate = expirydate;
    }

    public double getMaxamount() {
        return maxamount;
    }

    public void setMaxamount(double maxamount) {
        this.maxamount = maxamount;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"Bonuses{" +
            "promocode=" + promocode +
            "expirydate=" + expirydate +
            "maxamount=" + maxamount +
        "}";
    }
}
