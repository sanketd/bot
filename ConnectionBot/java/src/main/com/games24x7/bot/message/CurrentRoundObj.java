package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class CurrentRoundObj extends AbstractMessage { 
 
    private int maxdeals;
    private long inprogresstime;
    private int dealmultiplier;
    private int number;

    public CurrentRoundObj() {
        super(1048672, 0, 0);
    }

    public CurrentRoundObj(long receiverID, long playerID, int maxdeals,long inprogresstime,int dealmultiplier,int number) {
        super(1048672, receiverID, playerID);
        this.maxdeals = maxdeals;
        this.inprogresstime = inprogresstime;
        this.dealmultiplier = dealmultiplier;
        this.number = number;
    }




    public int getMaxdeals() {
        return maxdeals;
    }

    public void setMaxdeals(int maxdeals) {
        this.maxdeals = maxdeals;
    }

    public long getInprogresstime() {
        return inprogresstime;
    }

    public void setInprogresstime(long inprogresstime) {
        this.inprogresstime = inprogresstime;
    }

    public int getDealmultiplier() {
        return dealmultiplier;
    }

    public void setDealmultiplier(int dealmultiplier) {
        this.dealmultiplier = dealmultiplier;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"CurrentRoundObj{" +
            "maxdeals=" + maxdeals +
            "inprogresstime=" + inprogresstime +
            "dealmultiplier=" + dealmultiplier +
            "number=" + number +
        "}";
    }
}
