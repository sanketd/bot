package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class RejoinStatus extends AbstractMessage { 
 
    private boolean status;

    public RejoinStatus() {
        super(1048607, 0, 0);
    }

    public RejoinStatus(long receiverID, long playerID, boolean status) {
        super(1048607, receiverID, playerID);
        this.status = status;
    }




    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"RejoinStatus{" +
            "status=" + status +
        "}";
    }
}
