/**
 * 
 */
package com.games24x7.bot.message.handler;

import com.games24x7.bot.message.OppDiscard;
import com.games24x7.service.UserSession;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.util.PlayerInfo;

/**
 * @author rupesh
 * 
 */
public class OppDiscardHandler implements BotMessageHandler< OppDiscard >
{
	@Override
	public void handleMessage( UserSession session, OppDiscard message, PlayerInfo playerInfo )
	{
		playerInfo.setOpenCard( message.getCardid() );
		System.out.println("Open Card "+message.getCardid() );
	}

}
