package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TournamentComplete extends AbstractMessage { 
 

    public TournamentComplete() {
        super(1048669, 0, 0);
    }

    public TournamentComplete(long receiverID, long playerID) {
        super(1048669, receiverID, playerID);
    }




    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TournamentComplete{" +
        "}";
    }
}
