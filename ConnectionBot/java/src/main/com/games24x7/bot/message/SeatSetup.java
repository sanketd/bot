package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class SeatSetup extends AbstractMessage { 
 
    private long aaid;
    private List<Seats> seats;

    public SeatSetup() {
        super(1048651, 0, 0);
    }

    public SeatSetup(long receiverID, long playerID, long aaid,List<Seats> seats) {
        super(1048651, receiverID, playerID);
        this.aaid = aaid;
        this.seats = seats;
    }




    public long getAaid() {
        return aaid;
    }

    public void setAaid(long aaid) {
        this.aaid = aaid;
    }

    public List<Seats> getSeats() {
        return seats;
    }

    public void setSeats(List<Seats> seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"SeatSetup{" +
            "aaid=" + aaid +
            "seats=" + seats +
        "}";
    }
}
