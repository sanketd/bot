package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class ScoreWindow21Card extends AbstractMessage { 
 
    private long matchid;
    private long gameid;
    private int dealnumber;
    private List<ValCardTxns> valcardtxns;

    public ScoreWindow21Card() {
        super(1048756, 0, 0);
    }

    public ScoreWindow21Card(long receiverID, long playerID, long matchid,long gameid,int dealnumber,List<ValCardTxns> valcardtxns) {
        super(1048756, receiverID, playerID);
        this.matchid = matchid;
        this.gameid = gameid;
        this.dealnumber = dealnumber;
        this.valcardtxns = valcardtxns;
    }




    public long getMatchid() {
        return matchid;
    }

    public void setMatchid(long matchid) {
        this.matchid = matchid;
    }

    public long getGameid() {
        return gameid;
    }

    public void setGameid(long gameid) {
        this.gameid = gameid;
    }

    public int getDealnumber() {
        return dealnumber;
    }

    public void setDealnumber(int dealnumber) {
        this.dealnumber = dealnumber;
    }

    public List<ValCardTxns> getValcardtxns() {
        return valcardtxns;
    }

    public void setValcardtxns(List<ValCardTxns> valcardtxns) {
        this.valcardtxns = valcardtxns;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"ScoreWindow21Card{" +
            "matchid=" + matchid +
            "gameid=" + gameid +
            "dealnumber=" + dealnumber +
            "valcardtxns=" + valcardtxns +
        "}";
    }
}
