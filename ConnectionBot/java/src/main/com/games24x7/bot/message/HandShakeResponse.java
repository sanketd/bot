package com.games24x7.bot.message;

import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */
public class HandShakeResponse extends AbstractMessage {

    public HandShakeResponse() {
        super(2097156, 0, 0);
    }

    public HandShakeResponse(long receiverID, long playerID) {
        super(2097156, receiverID, playerID);
    }

    @Override
    public String toString() {
        return "HandShakeResponse{" +
        "}";
    }
}
