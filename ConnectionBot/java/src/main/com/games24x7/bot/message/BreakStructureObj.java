package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class BreakStructureObj extends AbstractMessage { 
 
    private String id;
    private long time;

    public BreakStructureObj() {
        super(1048747, 0, 0);
    }

    public BreakStructureObj(long receiverID, long playerID, String id,long time) {
        super(1048747, receiverID, playerID);
        this.id = id;
        this.time = time;
    }




    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"BreakStructureObj{" +
            "id=" + id +
            "time=" + time +
        "}";
    }
}
