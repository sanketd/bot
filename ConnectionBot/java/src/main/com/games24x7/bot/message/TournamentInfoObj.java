package com.games24x7.bot.message;
import com.games24x7.service.message.AbstractMessage;
import java.util.List;

/**
 * Auto Generated Message Class.
 * THIS FILE SHOULD NOT BE EDITED MANUALLY.
 */

	public class TournamentInfoObj extends AbstractMessage { 
 
    private int roundtotaltime;
    private String tournamentname;
    private List<BreakStructureObj> breakstructure;
    private CurrentRoundObj currentround;
    private List<Round> rounds;

    public TournamentInfoObj() {
        super(1048671, 0, 0);
    }

    public TournamentInfoObj(long receiverID, long playerID, int roundtotaltime,String tournamentname,List<BreakStructureObj> breakstructure,CurrentRoundObj currentround,List<Round> rounds) {
        super(1048671, receiverID, playerID);
        this.roundtotaltime = roundtotaltime;
        this.tournamentname = tournamentname;
        this.breakstructure = breakstructure;
        this.currentround = currentround;
        this.rounds = rounds;
    }




    public int getRoundtotaltime() {
        return roundtotaltime;
    }

    public void setRoundtotaltime(int roundtotaltime) {
        this.roundtotaltime = roundtotaltime;
    }

    public String getTournamentname() {
        return tournamentname;
    }

    public void setTournamentname(String tournamentname) {
        this.tournamentname = tournamentname;
    }

    public List<BreakStructureObj> getBreakstructure() {
        return breakstructure;
    }

    public void setBreakstructure(List<BreakStructureObj> breakstructure) {
        this.breakstructure = breakstructure;
    }

    public CurrentRoundObj getCurrentround() {
        return currentround;
    }

    public void setCurrentround(CurrentRoundObj currentround) {
        this.currentround = currentround;
    }

    public List<Round> getRounds() {
        return rounds;
    }

    public void setRounds(List<Round> rounds) {
        this.rounds = rounds;
    }

    @Override
    public String toString() {
        return "Super :"+super.toString()+" Child "+"TournamentInfoObj{" +
            "roundtotaltime=" + roundtotaltime +
            "tournamentname=" + tournamentname +
            "breakstructure=" + breakstructure +
            "currentround=" + currentround +
            "rounds=" + rounds +
        "}";
    }
}
