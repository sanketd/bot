package com.games24x7.bot.impl;

import java.net.InetSocketAddress;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import org.apache.commons.lang3.concurrent.BasicThreadFactory;
import org.jboss.netty.bootstrap.ClientBootstrap;
import org.jboss.netty.channel.Channel;
import org.jboss.netty.channel.ChannelFactory;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.games24x7.bot.message.BotMessageConstants;
import com.games24x7.bot.message.handler.AAInfoHandler;
import com.games24x7.bot.message.handler.AcceptSplitHandler;
import com.games24x7.bot.message.handler.AmountWonHandler;
import com.games24x7.bot.message.handler.BuyinSuccHandler;
import com.games24x7.bot.message.handler.ChangeTableIdHandler;
import com.games24x7.bot.message.handler.CloseAllConnectionsHandler;
import com.games24x7.bot.message.handler.CloseConnectionsHandler;
import com.games24x7.bot.message.handler.DealerHandler;
import com.games24x7.bot.message.handler.EnableSplitHandler;
import com.games24x7.bot.message.handler.FMGResponseHandler;
import com.games24x7.bot.message.handler.HandShakeResponseHandler;
import com.games24x7.bot.message.handler.HeartBeatHandler;
import com.games24x7.bot.message.handler.InitiateSplitHandler;
import com.games24x7.bot.message.handler.MTTTableMapRespHandler;
import com.games24x7.bot.message.handler.MatchInfoHandler;
import com.games24x7.bot.message.handler.MoveToTableHandler;
import com.games24x7.bot.message.handler.PickcResHandler;
import com.games24x7.bot.message.handler.PlayerEliminatedHandler;
import com.games24x7.bot.message.handler.PlrHandCardHandler;
import com.games24x7.bot.message.handler.PlrMoveHandler;
import com.games24x7.bot.message.handler.RematchResposeHandler;
import com.games24x7.bot.message.handler.SeatSetupHandler;
import com.games24x7.bot.message.handler.SetupResHandler;
import com.games24x7.bot.message.handler.SplitDetailsHandler;
import com.games24x7.bot.message.handler.StartDeclareHandler;
import com.games24x7.bot.message.handler.TakeSeatBalHandler;
import com.games24x7.bot.message.serializer.BotSerializer;
import com.games24x7.framework.configuration.Configuration;
import com.games24x7.service.bot.AbstractBot;
import com.games24x7.service.bot.ConnectionBot;
import com.games24x7.service.bot.StartConnectionBots;
import com.games24x7.service.bot.WorkerBot;
import com.games24x7.service.bot.connection.BotMessageHandler;
import com.games24x7.service.bot.connection.BotPipelineFactory;
import com.games24x7.util.BotMsgDigester;

public class BotImpl extends AbstractBot implements ConnectionBot
{
	private Logger logger = LoggerFactory.getLogger( BotImpl.class );
	private static BotImpl instance = null;
	private static String hostname;
	private static int port;
	private static ClientBootstrap clientBootstrap = null;
	private BotSerializer botSerializer = null;
	protected Map< Integer, BotMessageHandler > messageHandlerMap;
	Configuration config = null;
	StartConnectionBots connBot = null;
	BotMsgDigester botMsgDigester = null;
	ScheduledExecutorService scheduledThreadPool = null;
	ScheduledExecutorService retryScheduledThreadPool = null;

	public BotImpl( Configuration mainConfig )
	{
		super( mainConfig );
		BasicThreadFactory bossFactory = new BasicThreadFactory.Builder().namingPattern( "bossthread-%d" ).build();
		BasicThreadFactory workerFactory = new BasicThreadFactory.Builder().namingPattern( "workerthread-%d" ).build();
		ChannelFactory factory = new NioClientSocketChannelFactory( Executors.newCachedThreadPool( bossFactory ), Executors.newCachedThreadPool( workerFactory ), 1, 1 );

		clientBootstrap = new ClientBootstrap( factory );
		this.config = mainConfig;
	}

	@Override
	public void startup()
	{
		clientBootstrap.setPipelineFactory( new BotPipelineFactory() );
		clientBootstrap.setOption( "tcpNoDelay", true );
		clientBootstrap.setOption( "keepAlive", true );
		clientBootstrap.setOption( "reuseAddress", true );
		botSerializer = new BotSerializer();
		botMsgDigester = new BotMsgDigester( config );
		scheduledThreadPool = Executors.newScheduledThreadPool( config.getIntValue( "NO_OF_HEARTBEAT_THREADS", 100 ) );
		retryScheduledThreadPool = Executors.newScheduledThreadPool( config.getIntValue( "NO_OF_RETRY_THREADS", 100 ) );
		connBot = new StartConnectionBots( config.getIntValue( "NO_OF_THREADS" ) );
		registerHandler();
	}

	@Override
	public Channel createCSClientChannel( String teIp, int portFromClient )
	{
		System.out.println( "in createClientBotChannel" );
		hostname = teIp;
		port = portFromClient;
		Channel chl = clientBootstrap.connect( new InetSocketAddress( hostname, port ) ).awaitUninterruptibly().getChannel();
		return chl;
	}

	public static BotImpl init( Configuration mainConfig )
	{
		if( instance == null )
		{

			if( mainConfig != null )
			{
				instance = new BotImpl( mainConfig );
			}
			else
			{
				throw new IllegalArgumentException( "configuration and message bundle cannot be null" );
			}
		}
		return instance;
	}

	public static ConnectionBot getInstance()
	{
		return instance;
	}

	@Override
	public BotSerializer getBotSerializer()
	{
		return botSerializer;
	}

	public void registerHandler()
	{
		this.messageHandlerMap = new HashMap< Integer, BotMessageHandler >();
		messageHandlerMap.put( BotMessageConstants.HEARTBEATRESPONSE, new HeartBeatHandler() );
		messageHandlerMap.put( BotMessageConstants.SETUPRES, new SetupResHandler() );
		messageHandlerMap.put( BotMessageConstants.SEATSETUP, new SeatSetupHandler() );
		messageHandlerMap.put( BotMessageConstants.BUYINSUCC, new BuyinSuccHandler() );
		messageHandlerMap.put( BotMessageConstants.PLRMOVE, new PlrMoveHandler() );
		messageHandlerMap.put( BotMessageConstants.AAINFO, new AAInfoHandler() );
		messageHandlerMap.put( BotMessageConstants.MATCHINFO, new MatchInfoHandler() );
		messageHandlerMap.put( BotMessageConstants.PICKCRES, new PickcResHandler() );
		messageHandlerMap.put( BotMessageConstants.STARTDECLARE, new StartDeclareHandler() );
		messageHandlerMap.put( BotMessageConstants.PLRHANDCARD, new PlrHandCardHandler() );
		messageHandlerMap.put( BotMessageConstants.MTTTABLEMAPRESP, new MTTTableMapRespHandler() );
		messageHandlerMap.put( BotMessageConstants.CLOSEALLCONNECTIONS, new CloseAllConnectionsHandler() );
		messageHandlerMap.put( BotMessageConstants.CLOSECONNECTION, new CloseConnectionsHandler() );
		messageHandlerMap.put( BotMessageConstants.CHANGETABLEID, new ChangeTableIdHandler() );
		messageHandlerMap.put( BotMessageConstants.PLAYERELIMINATED, new PlayerEliminatedHandler() );
		messageHandlerMap.put( BotMessageConstants.FMGRESPONSE, new FMGResponseHandler() );
		messageHandlerMap.put( BotMessageConstants.AMOUNTWON, new AmountWonHandler() );
		messageHandlerMap.put( BotMessageConstants.REMATCHRESPONSE, new RematchResposeHandler() );
		messageHandlerMap.put( BotMessageConstants.TAKESEATBAL, new TakeSeatBalHandler() );
		messageHandlerMap.put( BotMessageConstants.MOVETOTABLE, new MoveToTableHandler() );
		messageHandlerMap.put( BotMessageConstants.SPLITDETAILS, new SplitDetailsHandler() );
		messageHandlerMap.put( BotMessageConstants.INITIATESPLIT, new InitiateSplitHandler() );
		messageHandlerMap.put( BotMessageConstants.ACCEPTSPLIT, new AcceptSplitHandler() );
		messageHandlerMap.put( BotMessageConstants.ENABLESPLIT, new EnableSplitHandler() );
		messageHandlerMap.put( BotMessageConstants.DEALER, new DealerHandler() );
		messageHandlerMap.put( BotMessageConstants.HANDSHAKERESPONSE, new HandShakeResponseHandler() );
	}

	@Override
	public BotMessageHandler getMsgHandler( int classId )
	{
		return messageHandlerMap.get( classId );
	}

	public Configuration getConfiguration()
	{
		return config;
	}

	public StartConnectionBots getBots()
	{
		return connBot;
	}

	public BotMsgDigester getBotMsgDigester()
	{
		return botMsgDigester;
	}

	public ScheduledExecutorService getScheduledThreadPool()
	{
		return scheduledThreadPool;
	}

	public void setScheduledThreadPool( ScheduledExecutorService scheduledThreadPool )
	{
		this.scheduledThreadPool = scheduledThreadPool;
	}

	@Override
	public void handleDisconnection( long playerId, long tableId, ScheduledFuture heartBeatTask )
	{
		heartBeatTask.cancel( true );
		System.out.println( "In disconnection Handler for player" + playerId + " " + new Date( System.currentTimeMillis() ) );
		if( config.getBooleanValue( "RECON_AFTER_DISCONNNECTION", false ) )
		{
			System.out.println( "starting new bot" );
			getBots().getExecutor()
					.execute( new WorkerBot( tableId, playerId, config.getStringValue( "CS_IP" ), config.getIntValue( "CS_PORT" ), config.getIntValue( "PLAYERS_ON_A_TABLE" ),
							config.getIntValue( "SETTLEMENT_TYPE" ), false, config.getLongValue( "MTT_TOURNAMENT_ID" ), config.getIntValue( "CHANNEL_ID" ),
							config.getBooleanValue( "FMG_FLAG" ), config.getIntValue( "FMG_AMOUNT" ) ) );
		}
	}

}
